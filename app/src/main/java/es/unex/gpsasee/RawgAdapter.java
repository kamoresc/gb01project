package es.unex.gpsasee;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import es.unex.gpsasee.model.Result;
import es.unex.gpsasee.ui.details.DetailsActivity;


public class RawgAdapter extends RecyclerView.Adapter<RawgAdapter.MyViewHolder> {
    private List<Result> mDataset;
    private int count;
    private Context context;

    public interface OnListInteractionListener{
        public void onListInteraction(String url);
    }

    public OnListInteractionListener mListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView thumbnail;
        // each data item is just a string in this case
        public TextView mTextView;
        public TextView mTextViewPlatform;
        public View mView;

        public Result mItem;

        public MyViewHolder(View v) {
            super(v);
            mView=v;
            //TODO
            mTextView = v.findViewById(R.id.textView);
            mTextViewPlatform=v.findViewById(R.id.textViewPlatforms);
            thumbnail = v.findViewById(R.id.thumbnail);
            //mDateView = v.findViewById(R.id.dateView);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RawgAdapter(Context context, List<Result> myDataset, OnListInteractionListener listener) {
        mDataset = myDataset;
        mListener = listener;
        this.context=context;
    }

    @Override
    public RawgAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        // Create new views (invoked by the layout manager)

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_instance, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.mItem = mDataset.get(position);
        holder.mTextView.setText(mDataset.get(position).getName());
        holder.mTextViewPlatform.setText(choosePlatforms(mDataset.get(position)));

        String url = holder.mItem.getBackgroundImage();
        Glide.with(holder.mView)  //2 Contexto
                .load(url) //3 Foto buena
                .centerCrop() //4
                //.placeholder(R.drawable.ic_image_place_holder) //5
                //.error(R.drawable.ic_broken_image) //6
                //.fallback(R.drawable.ic_no_image) //7
                .into(holder.thumbnail); //8 Donde se pone

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    // todo mListener.onListInteraction(holder.mItem.getSvnUrl());
                    Result r = mDataset.get(position);

                    Intent intent = new Intent(context, DetailsActivity.class);
                    intent.putExtra("id", r.getId());
                    context.startActivity(intent);
                }
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public int getTotalCount(){
        return count;
    }

    public void swap(List<Result> dataset, int count){
        this.count=count;
        mDataset = dataset;
        notifyDataSetChanged();
    }

    private String choosePlatforms(Result result) {
        String text="Sin plataforma";
        if (result.getRPlatforms()!=null) {
            text="";
            for (int i = 0; i < result.getRPlatforms().size() && i < 3; i++) {
                text = text + result.getRPlatforms().get(i).getPlatform().getName() + " ";
            }
        }
        return text;
    }
}
