package es.unex.gpsasee.repositories;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.network.SuggestedNetworkDataSource;
import es.unex.gpsasee.roomdb.dao.SuggestedGameDao;
import es.unex.gpsasee.roomdb.entity.SuggestedGames;


/**
 * Handles data operations in Sunshine. Acts as a mediator between {@link SuggestedNetworkDataSource}
 * and {@link SuggestedGameDao}
 */
public class SuggestedRepository {
    private static final String LOG_TAG = SuggestedRepository.class.getSimpleName();

    // For Singleton instantiation
    private static SuggestedRepository sInstance;
    private final SuggestedGameDao mSuggestedGameDao;
    private final SuggestedNetworkDataSource mSuggestedNetworkDataSource;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private final MutableLiveData<Integer> gameFilterLiveData = new MutableLiveData<>();
    private final Map<Integer, Long> lastUpdateTimeMillisMap = new HashMap<>();
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 30000;

    private SuggestedRepository(SuggestedGameDao suggestedDao, SuggestedNetworkDataSource suggestedNetworkDataSource) {
        mSuggestedGameDao = suggestedDao;
        mSuggestedNetworkDataSource = suggestedNetworkDataSource;
        // LiveData that fetches repos from network
        LiveData<SuggestedGames> networkData = mSuggestedNetworkDataSource.getCurrentGames();
        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkData.observeForever(suggestedGames -> {
            mExecutors.diskIO().execute(() -> {
                // Deleting cached repos of user
                if (suggestedGames.getRagw().getCount()>0){
                    mSuggestedGameDao.deleteGamesById(suggestedGames.getId());
                }
                // Insert our new repos into local database
                mSuggestedGameDao.bulkInsert(suggestedGames);
                Log.d(LOG_TAG, "New values inserted in Room");
                Log.d("Ver Params",""+suggestedGames.getId());
            });
        });
    }

    public synchronized static SuggestedRepository getInstance(SuggestedGameDao dao, SuggestedNetworkDataSource nds) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new SuggestedRepository(dao, nds);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public void setId(final int id){
        gameFilterLiveData.setValue(id);
        AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeeded(id)) {
                doFetchRepos(id);
            }
        });
    }

    public void doFetchRepos(int id){
        Log.d(LOG_TAG, "Fetching Suggested Games");
        AppExecutors.getInstance().diskIO().execute(() -> {
            mSuggestedGameDao.deleteGamesById(id);
            mSuggestedNetworkDataSource.fetchGames(id);
            lastUpdateTimeMillisMap.put(id, System.currentTimeMillis());
        });
    }

    /**
     * Database related operations
     **/

    public LiveData<SuggestedGames> getCurrentRepos() {
        return Transformations.switchMap(gameFilterLiveData, mSuggestedGameDao::getGamesById);
    }

    /**
     * Checks if we have to update the repos data.
     * @return Whether a fetch is needed
     */
    private boolean isFetchNeeded(int id) {
        Long lastFetchTimeMillis = lastUpdateTimeMillisMap.get(id);
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;
        return timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS;
    }

}