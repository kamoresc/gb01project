package es.unex.gpsasee.repositories;

import android.util.Log;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.HashMap;
import java.util.Map;

import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.gameModel.Game;
import es.unex.gpsasee.network.DetailsNetworkDataSource;
import es.unex.gpsasee.roomdb.dao.DetailsGameDAO;


/**
 * Handles data operations in Sunshine. Acts as a mediator between {@link es.unex.gpsasee.network.DetailsNetworkDataSource}
 * and {@link es.unex.gpsasee.roomdb.dao.DetailsGameDAO}
 */
public class DetailsRepository {
    private static final String LOG_TAG = DetailsRepository.class.getSimpleName();

    // For Singleton instantiation
    private static DetailsRepository sInstance;
    private final DetailsGameDAO mDetailsDao;
    private final DetailsNetworkDataSource mDetailsNetworkDataSource;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private final MutableLiveData<Integer> searchFilterLiveData = new MutableLiveData<>();
    private final Map<Integer, Long> lastUpdateTimeMillisMap = new HashMap<>();
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 30000;

    private DetailsRepository(DetailsGameDAO detailsDao, DetailsNetworkDataSource detailsNetworkDataSource) {
        mDetailsDao = detailsDao;
        mDetailsNetworkDataSource = detailsNetworkDataSource;
        // LiveData that fetches repos from network
        LiveData<Game> networkData = mDetailsNetworkDataSource.getCurrentDetails();
        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkData.observeForever(Game -> mExecutors.diskIO().execute(() -> {

            if(Game!=null){
                mDetailsDao.deleteGamesBySearch(Game.getId());
            }

            // Insert our new repos into local database

            mDetailsDao.bulkInsert(Game);
            Log.d(LOG_TAG, "New values inserted in Room (details)");
        }));
    }

    public synchronized static DetailsRepository getInstance(DetailsGameDAO dao, DetailsNetworkDataSource gds) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new DetailsRepository(dao, gds);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public void setParams(final int id){
        searchFilterLiveData.setValue(id);
        AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeeded(id)) {
                doFetchGames(id);
            }
        });
    }

    public void doFetchGames(int id){
        Log.d(LOG_TAG, "Doing fetch of details");

        AppExecutors.getInstance().diskIO().execute(() -> {
            mDetailsDao.deleteGamesBySearch(id);
            mDetailsNetworkDataSource.fetchGames(id);
            lastUpdateTimeMillisMap.put(id, System.currentTimeMillis());
        });
    }

    /**
     * Database related operations
     **/

    public LiveData<Game> getCurrentGames() {
        return Transformations.switchMap(searchFilterLiveData, new Function<Integer, LiveData<Game>>() {

        @Override
            public LiveData<Game> apply (Integer input) {
                return mDetailsDao.getGamesBySearch(input);

            }
        });
    }

    /**
     * Checks if we have to update the repos data.
     * @return Whether a fetch is needed
     */
    private boolean isFetchNeeded(int id) {
        Long lastFetchTimeMillis = lastUpdateTimeMillisMap.get(id);
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;
        return timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS;
    }

}