package es.unex.gpsasee.repositories;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.SessionStore;
import es.unex.gpsasee.roomdb.dao.CommentDAO;
import es.unex.gpsasee.roomdb.entity.Comment;
import es.unex.gpsasee.roomdb.entity.Favourite;

public class CommentsListRepository {

    // For Singleton instantiation
    private static CommentsListRepository sInstance;
    private CommentDAO mComDao;
    private Context mContext;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private LiveData<List<Comment>> mCommentsResults;
    private static final String LOG_TAG = GameRepository.class.getSimpleName();

    private CommentsListRepository(CommentDAO ComDao) {
        //Pasamos la dependencia por parametro de constructor
        mComDao = ComDao;
        mExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    //Cojo todos los comentarios
                    mCommentsResults = mComDao.getAllComments();
                }catch (Exception e){
                    Log.e("Err", "Objeto Nulo");
                }
            }
        });
    }

    //Patron Singleton
    public synchronized static CommentsListRepository getInstance(CommentDAO ComDao) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new CommentsListRepository(ComDao);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public LiveData<List<Comment>> getCurrentComments() {
        return mCommentsResults;
    }
}
