package es.unex.gpsasee.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.List;

import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.SessionStore;
import es.unex.gpsasee.roomdb.dao.UserDAO;
import es.unex.gpsasee.roomdb.entity.User;

public class UserRepository {
    private static UserRepository INSTANCE = null;
    private final UserDAO mUserDao;
    private final SessionStore mSessionStore;
    private final AppExecutors mExecutors = AppExecutors.getInstance();

    private LiveData<List<User>> mListLiveData = new MutableLiveData<>();
    private LiveData<User> mOneLiveData = new MutableLiveData<>();

    private UserRepository(UserDAO mUserDao, SessionStore mSessionStore) {
        this.mUserDao = mUserDao;
        this.mSessionStore = mSessionStore;
    }

    public static synchronized UserRepository getInstance(UserDAO userDAO, SessionStore sessionStore){
        if(INSTANCE == null){
            INSTANCE = new UserRepository(userDAO, sessionStore);
        }

        return INSTANCE;
    }

    public LiveData<User> getCurrentUser(){
        return mSessionStore.currentUser;
    }

    public void setCurrentUser(User user){
        mSessionStore.setUser(user);
    }

    public LiveData<List<User>> getAllUsers(){
        return Transformations.switchMap(mListLiveData, input -> mUserDao.getAllUsers());
    }

    public LiveData<User> getUserById(long id){
        /*return Transformations.switchMap(mOneLiveData, new Function<User, LiveData<User>>() {
            @Override
            public LiveData<User> apply(User input) {*/
                return mUserDao.getUser(id);/*
            }
        });*/
    }

    public LiveData<User> getUserByName(String username){
        /*return Transformations.switchMap(mOneLiveData, new Function<User, LiveData<User>>() {
            @Override
            public LiveData<User> apply(User input) {*/
                return mUserDao.getUserByName(username);/*
            }
        });*/
    }

    public LiveData<User> checkLogin(String username, String password){
        return mUserDao.login(username, password);
    }

    public long insert(User user){
        return mUserDao.insert(user);
    }

    public int update(User user){
        return mUserDao.update(user);
    }

    public int delete(User user){
        return mUserDao.delete(user);
    }
}
