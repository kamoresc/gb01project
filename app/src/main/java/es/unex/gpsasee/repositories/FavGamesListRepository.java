package es.unex.gpsasee.repositories;

import android.content.Context;
import android.util.Log;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.SessionStore;
import es.unex.gpsasee.data.SearchData;
import es.unex.gpsasee.network.GameNetworkDataSource;
import es.unex.gpsasee.roomdb.dao.FavouriteDAO;
import es.unex.gpsasee.roomdb.dao.GameDao;
import es.unex.gpsasee.roomdb.entity.Favourite;
import es.unex.gpsasee.roomdb.entity.GameParams;
import es.unex.gpsasee.roomdb.entity.User;

public class FavGamesListRepository {

    // For Singleton instantiation
    private static FavGamesListRepository sInstance;
    private FavouriteDAO mFavDao;
    private static final String LOG_TAG = GameRepository.class.getSimpleName();

    private FavGamesListRepository(FavouriteDAO FavDao) {
        //Pasamos la dependencia por parametro de constructor
        mFavDao = FavDao;
    }

    //Patron Singleton
    public synchronized static FavGamesListRepository getInstance(FavouriteDAO FavDao) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new FavGamesListRepository(FavDao);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public LiveData<List<Favourite>> getFavGames(long user){
        return mFavDao.getFavouritesByUser(user);
    }

    public LiveData<List<Favourite>> getFavGamesByUserAndGame(long user, long game){
        return mFavDao.getFavouritesByUserAndGame(user, game);
    }

    public void insert(Favourite newFav) {
        mFavDao.insert(newFav);
    }

    public void deleteFavByUserAndGame(long userid, long gameid) {
        mFavDao.deleteFavouriteByUserAndGame(userid, gameid);
    }
}
