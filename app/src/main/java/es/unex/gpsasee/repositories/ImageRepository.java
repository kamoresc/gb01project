package es.unex.gpsasee.repositories;

import androidx.lifecycle.LiveData;

import java.util.List;

import es.unex.gpsasee.roomdb.dao.ImageDAO;
import es.unex.gpsasee.roomdb.entity.Image;

public class ImageRepository {
    private static ImageRepository INSTANCE;

    private ImageDAO mImageDao;

    private ImageRepository(ImageDAO imageDAO){
        mImageDao = imageDAO;
    }

    public synchronized static ImageRepository getInstance(ImageDAO imageDAO){
        if(INSTANCE == null){
            INSTANCE = new ImageRepository(imageDAO);
        }

        return INSTANCE;
    }

    public LiveData<List<Image>> getImagesByUser(long user){
        return mImageDao.getImagesByUser(user);
    }

    public LiveData<List<Image>> getImagesByUserAndGame(long user, long game){
        return mImageDao.getImagesByUserAndGame(user, game);
    }

    public LiveData<Image> getImage(long id){
        return mImageDao.getImage(id);
    }
}
