package es.unex.gpsasee.repositories;


import android.content.Context;
import android.util.Log;
import androidx.lifecycle.LiveData;
import java.util.List;
import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.SessionStore;
import es.unex.gpsasee.roomdb.dao.PlayedGameDAO;
import es.unex.gpsasee.roomdb.entity.PlayedGame;

public class PlayedGamesListRepository {

    // For Singleton instantiation
    private static PlayedGamesListRepository sInstance;
    private PlayedGameDAO mPlayedDao;
    private static final String LOG_TAG = GameRepository.class.getSimpleName();

    private PlayedGamesListRepository(PlayedGameDAO PlayedDao) {
        //Pasamos la dependencia por parametro de constructor
        mPlayedDao = PlayedDao;
    }

    //Patron Singleton
    public synchronized static PlayedGamesListRepository getInstance(PlayedGameDAO PlayedDao) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new PlayedGamesListRepository(PlayedDao);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public LiveData<List<PlayedGame>> getPlayedGames(long user){
        return mPlayedDao.getPlayedGamesByUser(user);
    }

    public LiveData<List<PlayedGame>> getPlayedGamesByUserAndGame(long userid, long gameid) {
        return mPlayedDao.getPlayedGamesByUserAndGame(userid, gameid);
    }

    public void insertPlayedGame(PlayedGame newPG) {
        mPlayedDao.insert(newPG);
    }

    public void deletePlayedGamesByUserAndGame(long userid, long gameid) {
        mPlayedDao.deletePlayedGamesByUserAndGame(userid, gameid);
    }
}