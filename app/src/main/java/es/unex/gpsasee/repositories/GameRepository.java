package es.unex.gpsasee.repositories;

import android.util.Log;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;

import java.util.HashMap;
import java.util.Map;

import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.data.SearchData;
import es.unex.gpsasee.model.Rawg;
import es.unex.gpsasee.network.GameNetworkDataSource;
import es.unex.gpsasee.roomdb.dao.GameDao;
import es.unex.gpsasee.roomdb.entity.GameParams;


/**
 * Handles data operations in Sunshine. Acts as a mediator between {@link GameNetworkDataSource}
 * and {@link GameDao}
 */
public class GameRepository {
    private static final String LOG_TAG = GameRepository.class.getSimpleName();

    // For Singleton instantiation
    private static GameRepository sInstance;
    private final GameDao mGameDao;
    private final GameNetworkDataSource mGameNetworkDataSource;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private final MutableLiveData<SearchData> searchFilterLiveData = new MutableLiveData<>();
    private final Map<SearchData, Long> lastUpdateTimeMillisMap = new HashMap<>();
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 30000;

    private GameRepository(GameDao gameDao, GameNetworkDataSource gameNetworkDataSource) {
        mGameDao = gameDao;
        mGameNetworkDataSource = gameNetworkDataSource;
        // LiveData that fetches repos from network
        LiveData<GameParams> networkData = mGameNetworkDataSource.getCurrentGames();
        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkData.observeForever(gameParams -> mExecutors.diskIO().execute(() -> {
            //Some searchData attributes can be null, must be changed to ""
            gameParams.getSearchData().toEmpty();
            // Deleting cached repos of user
            if (gameParams.getRagw().getCount()>0){
                gameDao.deleteGamesBySearch(gameParams.getSearchData().getName(),gameParams.getSearchData().getPage(),gameParams.getSearchData().getPlatform(),gameParams.getSearchData().getCompany(),gameParams.getSearchData().getDate(),gameParams.getSearchData().getOrdering());
            }
            // Insert our new repos into local database
            mGameDao.bulkInsert(gameParams);
            Log.d(LOG_TAG, "New values inserted in Room");
            Log.d("Ver Params",gameParams.toString());
        }));
    }

    public synchronized static GameRepository getInstance(GameDao dao, GameNetworkDataSource gds) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new GameRepository(dao, gds);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public void setParams(final String name, final int page, final String platform, final String company, final String date, final String ordering){
        SearchData searchData=new SearchData(name,page,platform,company,date,ordering);
        searchData.toEmpty();
        searchFilterLiveData.setValue(searchData);
        AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeeded(searchData)) {
                doFetchGames(name,page,platform,company,date,ordering);
            }
        });
    }

    public void doFetchGames(String name, int page, String platform, String company, String date, String ordering){
        Log.d(LOG_TAG, "Fetching Repos from Github");
        SearchData searchData=new SearchData(name,page,platform,company,date,ordering);
        searchData.toEmpty();
        AppExecutors.getInstance().diskIO().execute(() -> {
            mGameDao.deleteGamesBySearch(searchData.getName(),searchData.getPage(),searchData.getPlatform(),searchData.getCompany(),searchData.getDate(),searchData.getOrdering());
            mGameNetworkDataSource.fetchGames(name,page,platform,company,date,ordering);
            lastUpdateTimeMillisMap.put(searchData, System.currentTimeMillis());
        });
    }

    /**
     * Database related operations
     **/

    public LiveData<GameParams> getCurrentGames() {
        return Transformations.switchMap(searchFilterLiveData, new Function<SearchData, LiveData<GameParams>>() {
            @Override
            public LiveData<GameParams> apply(SearchData input) {
                input.toEmpty();
                return mGameDao.getGamesBySearch(input.getName(),input.getPage(),input.getPlatform(),input.getCompany(),input.getDate(),input.getOrdering());
            }
        });
    }

    /**
     * Checks if we have to update the repos data.
     * @return Whether a fetch is needed
     */
    private boolean isFetchNeeded(SearchData searchData) {
        Long lastFetchTimeMillis = lastUpdateTimeMillisMap.get(searchData);
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;
        return timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS;
    }

}