package es.unex.gpsasee;

import android.content.Context;

import es.unex.gpsasee.network.GameNetworkDataSource;
import es.unex.gpsasee.network.SuggestedNetworkDataSource;
import es.unex.gpsasee.repositories.CommentsListRepository;
import es.unex.gpsasee.repositories.FavGamesListRepository;
import es.unex.gpsasee.repositories.GameRepository;
import es.unex.gpsasee.repositories.ImageRepository;
import es.unex.gpsasee.repositories.PlayedGamesListRepository;
import es.unex.gpsasee.repositories.SuggestedRepository;
import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.ui.CommentsListFactory;
import es.unex.gpsasee.ui.CommentsListViewModel;
import es.unex.gpsasee.ui.details.DetailsViewModelFactory;
import es.unex.gpsasee.ui.fav_games.FavGamesListFactory;
import es.unex.gpsasee.ui.ListViewModelFactory;
import es.unex.gpsasee.ui.NavigationDrawerViewModelFactory;
import es.unex.gpsasee.ui.played_games.PlayedGamesListFactory;
import es.unex.gpsasee.ui.SuggestedViewModelFactory;
import es.unex.gpsasee.ui.gallery.GalleryViewModelFactory;
import es.unex.gpsasee.ui.login.LoginViewModelFactory;
import es.unex.gpsasee.ui.logout.LogoutViewModelFactory;
import es.unex.gpsasee.ui.register.RegisterViewModelFactory;
import es.unex.gpsasee.ui.user.UserViewModelFactory;

public class AppContainer {

    private RAWGDatabase database;

    private GameNetworkDataSource networkDataSource;
    public GameRepository repository;
    public ListViewModelFactory factory;
    public FavGamesListFactory favGamesListFactory;
    public FavGamesListRepository favGamesListRepository;
    public PlayedGamesListFactory playedGamesListFactory;
    public PlayedGamesListRepository playedGamesListRepository;


    private SuggestedNetworkDataSource suggestedNetworkDataSource;
    public SuggestedRepository suggestedRepository;
    public SuggestedViewModelFactory suggestedViewModelFactory;

    public UserRepository userRepository;
    public UserViewModelFactory userViewModelFactory;
    public LoginViewModelFactory loginViewModelFactory;
    public RegisterViewModelFactory registerViewModelFactory;
    public LogoutViewModelFactory logoutViewModelFactory;
    public NavigationDrawerViewModelFactory navigationDrawerViewModelFactory;

    public ImageRepository imageRepository;
    public GalleryViewModelFactory galleryViewModelFactory;

    public CommentsListRepository commentsListRepository;
    public CommentsListFactory commentsListFactory;

    public DetailsViewModelFactory detailsViewModelFactory;

    public AppContainer(Context context){
        //Referencia a la Base de Datos
        database = RAWGDatabase.getDatabase(context);

        // Para los users
        userRepository = UserRepository.getInstance(database.userDao(), SessionStore.getInstance(context));
        userViewModelFactory = new UserViewModelFactory(userRepository);
        loginViewModelFactory = new LoginViewModelFactory(userRepository);
        registerViewModelFactory = new RegisterViewModelFactory(userRepository);
        logoutViewModelFactory = new LogoutViewModelFactory(userRepository);
        navigationDrawerViewModelFactory = new NavigationDrawerViewModelFactory(userRepository);

        //Para la lista de juegos
        networkDataSource = GameNetworkDataSource.getInstance();
        repository = GameRepository.getInstance(database.gameDao(),networkDataSource);
        factory = new ListViewModelFactory(repository);

        //Para los FavGames
        favGamesListRepository = FavGamesListRepository.getInstance(database.favouriteDAO());
        favGamesListFactory = new FavGamesListFactory(favGamesListRepository, userRepository);

        //Para los PlayedGames
        playedGamesListRepository = PlayedGamesListRepository.getInstance(database.playedGameDAO());
        playedGamesListFactory = new PlayedGamesListFactory(playedGamesListRepository, userRepository);

        //Para los juegos recomendados
        suggestedNetworkDataSource = SuggestedNetworkDataSource.getInstance();
        suggestedRepository = SuggestedRepository.getInstance(database.suggestedGameDao(),suggestedNetworkDataSource);
        suggestedViewModelFactory = new SuggestedViewModelFactory(suggestedRepository);

        //Para comentarios
        commentsListRepository= CommentsListRepository.getInstance(database.commentDAO());
        commentsListFactory = new CommentsListFactory(commentsListRepository);

        // Para la galería
        imageRepository = ImageRepository.getInstance(database.imageDAO());
        galleryViewModelFactory = new GalleryViewModelFactory(imageRepository, userRepository);

        detailsViewModelFactory = new DetailsViewModelFactory(userRepository, favGamesListRepository, playedGamesListRepository);
    }
}
