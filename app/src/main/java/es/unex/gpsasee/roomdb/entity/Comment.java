package es.unex.gpsasee.roomdb.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "comments")
public class Comment {

    @PrimaryKey(autoGenerate = true)
    private long id;
    @ColumnInfo(name = "userid")
    private long userid;
    @ColumnInfo(name = "gameid")
    private long gameid;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "texto")
    private String texto;
    @ColumnInfo(name = "username")
    private String username;

    public Comment(long id, long userid, long gameid, String title, String texto,String username){
        this.id = id;
        this.userid = userid;
        this.gameid = gameid;
        this.title = title;
        this.texto = texto;
        this.username=username;
    }

    public long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public long getUserid() {return userid;}

    public void setUserid(Long userid) {this.userid = userid;}

    public long getGameid() {return gameid;}

    public void setGameid(Long gameid) {this.gameid = gameid;}

    public String getTitle() {return title;}

    public void setTitle(String title) {this.title = title;}

    public String getTexto() {return texto;}

    public void setTexto(String Texto) {this.texto = Texto;}

    public String getUsername() {return username;}

    public void setUsername(String Username) {this.username = username;}
}
