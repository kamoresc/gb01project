package es.unex.gpsasee.roomdb.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.gpsasee.roomdb.entity.User;

@Dao
public interface UserDAO {
    @Query("SELECT * FROM users")
    LiveData<List<User>> getAllUsers();

    @Query("SELECT * FROM users WHERE id = :id")
    LiveData<User> getUser(Long id);

    @Query("SELECT * FROM users WHERE username like :username")
    LiveData<User> getUserByName(String username);

    @Query("SELECT * FROM users WHERE username = :username AND password = :password")
    LiveData<User> login(String username, String password);

    @Insert
    Long insert(User user);

    @Update
    int update(User user);

    @Delete
    int delete(User user);
}
