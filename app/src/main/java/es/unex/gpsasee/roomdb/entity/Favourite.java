package es.unex.gpsasee.roomdb.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "favourites")
public class Favourite {

    @PrimaryKey(autoGenerate = true)
    private long id;
    @ColumnInfo(name = "userid")
    private long userid;
    @ColumnInfo(name = "gameid")
    private long gameid;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "platform")
    private String platform;
    @ColumnInfo(name = "image")
    private String image;

    public Favourite(long id, long userid, long gameid, String title, String platform, String image){
        this.id = id;
        this.userid = userid;
        this.gameid = gameid;
        this.title = title;
        this.platform = platform;
        this.image = image;
    }

    public long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public long getUserid() {return userid;}

    public void setUserid(Long userid) {this.userid = userid;}

    public long getGameid() {return gameid;}

    public void setGameid(Long gameid) {this.gameid = gameid;}

    public String getTitle() {return title;}

    public void setTitle(String title) {this.title = title;}

    public String getPlatform() {return platform;}

    public void setPlatform(String platform) {this.platform = platform;}

    public String getImage() {return image;}

    public void setImage(String image) {this.image = image;}
}
