package es.unex.gpsasee.roomdb.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "images")
public class Image {
    @PrimaryKey(autoGenerate = true)
    private long id;
    @ColumnInfo(name = "gameid")
    private long gameid;
    @ColumnInfo(name = "userid")
    private long userid;
    @ColumnInfo(name = "uri")
    private String uri;

    public Image(long id, long gameid, long userid, String uri){
        this.id = id;
        this.gameid = gameid;
        this.userid = userid;
        this.uri = uri;
    }

    public long getId() {return id;}

    public void setId(long id) {this.id = id;}

    public long getGameid() {return gameid;}

    public void setGameid(long gameid) {this.gameid = gameid;}

    public long getUserid() {return userid;}

    public void setUserid(long userid) {this.userid = userid;}

    public String getUri() {return uri;}

    public void setUri(String uri) {this.uri = uri;}
}
