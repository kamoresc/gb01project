package es.unex.gpsasee.roomdb.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.gpsasee.roomdb.entity.Image;
import es.unex.gpsasee.roomdb.entity.User;

@Dao
public interface ImageDAO {
    @Query("SELECT * FROM images")
    public LiveData<List<Image>> getAllImages();

    @Query("SELECT * FROM images WHERE id = :id")
    public LiveData<Image> getImage(long id);

    @Query("SELECT * FROM images WHERE userid = :user")
    public LiveData<List<Image>> getImagesByUser(long user);

    @Query("SELECT * FROM images WHERE gameid = :game")
    public LiveData<List<Image>> getImagesByGame(long game);

    @Query("SELECT * FROM images WHERE userid = :user AND gameid = :game")
    public LiveData<List<Image>> getImagesByUserAndGame(long user, long game);

    @Insert
    public long insert(Image image);

    @Update
    public int update(Image image);

    @Delete
    public int delete(Image image);

    @Query("DELETE FROM images WHERE userid = :user")
    public void deleteAllByUser(long user);

    @Query("DELETE FROM images WHERE gameid = :game")
    public void deleteAllByGame(long game);

    @Query("DELETE FROM images WHERE id = :id")
    public void deleteByID(long id);

    @Query("DELETE FROM images")
    public void deleteAll();
}
