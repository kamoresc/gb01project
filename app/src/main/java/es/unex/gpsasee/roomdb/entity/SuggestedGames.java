package es.unex.gpsasee.roomdb.entity;


import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import es.unex.gpsasee.data.SearchData;
import es.unex.gpsasee.model.Rawg;

@Entity(tableName = "suggestedGames")
public class SuggestedGames {
    @PrimaryKey
    private int id;
    @Embedded
    private Rawg ragw;

    public SuggestedGames() {
    }

    @Ignore
    public SuggestedGames(int id, Rawg ragw) {
        this.id=id;
        this.ragw = ragw;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Rawg getRagw() {
        return ragw;
    }

    public void setRagw(Rawg ragw) {
        this.ragw = ragw;
    }

    @Override
    public String toString() {
        return "GameParams{" +
                "id=" + id +
                ", ragw=" + ragw +
                '}';
    }
}
