package es.unex.gpsasee.roomdb.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.gpsasee.roomdb.entity.Favourite;
import es.unex.gpsasee.roomdb.entity.User;

@Dao
public interface FavouriteDAO {

    @Query("SELECT * FROM favourites")
    List<Favourite> getAllFavourites();

    @Query("SELECT * FROM favourites WHERE userid = :userid")
    LiveData<List<Favourite>> getFavouritesByUser(long userid);

    @Query("SELECT * FROM favourites WHERE userid = :userid AND gameid = :gameid")
    LiveData<List<Favourite>> getFavouritesByUserAndGame(long userid, long gameid);

    @Query("SELECT * FROM favourites WHERE id = :id")
    Favourite getFavourite(Long id);

    @Insert
    long insert(Favourite favourite);

    @Update
    int update(Favourite favourite);

    @Delete
    int delete(Favourite favourite);

    @Query("DELETE FROM favourites")
    void deleteAllFavourites();

    @Query("DELETE FROM favourites WHERE userid = :userid AND gameid = :gameid")
    void deleteFavouriteByUserAndGame(long userid, long gameid);

    @Query("DELETE FROM favourites WHERE userid = :userid")
    void deleteAllFavouritesByUser(long userid);

}
