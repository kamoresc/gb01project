package es.unex.gpsasee.roomdb.dao;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;


import es.unex.gpsasee.data.SearchData;
import es.unex.gpsasee.roomdb.entity.GameParams;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface GameDao {

    @Insert(onConflict = REPLACE)
    void bulkInsert(GameParams gameParams);

    // TODO - Migrate List<Repo> to LiveData<List<Repo>>
    @Query("SELECT * FROM gameParams WHERE name=:name and page=:page and platform=:platform and company=:company and date=:date and ordering=:ordering")
    LiveData<GameParams> getGamesBySearch(String name, int page, String platform, String company, String date, String ordering);


//    @Query("SELECT count(*) FROM repo WHERE owner = :owner")
//    int getNumberReposByUser(String owner);

    @Query("SELECT * FROM gameParams WHERE name=:name and page=:page and platform=:platform and company=:company and date=:date and ordering=:ordering")
    int deleteGamesBySearch(String name, int page, String platform, String company, String date, String ordering);
}
