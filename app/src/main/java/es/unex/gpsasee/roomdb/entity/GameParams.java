package es.unex.gpsasee.roomdb.entity;


import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import es.unex.gpsasee.data.SearchData;
import es.unex.gpsasee.model.Rawg;

@Entity(tableName = "gameParams")
public class GameParams {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @Embedded
    private SearchData searchData;
    @Embedded
    private Rawg ragw;

    public GameParams() {
    }

    @Ignore
    public GameParams(SearchData searchData, Rawg ragw) {
        this.searchData = searchData;
        this.ragw = ragw;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SearchData getSearchData() {
        return searchData;
    }

    public void setSearchData(SearchData searchData) {
        this.searchData = searchData;
    }

    public Rawg getRagw() {
        return ragw;
    }

    public void setRagw(Rawg ragw) {
        this.ragw = ragw;
    }

    @Override
    public String toString() {
        return "GameParams{" +
                "id=" + id +
                ", searchData=" + searchData +
                ", ragw=" + ragw +
                '}';
    }
}
