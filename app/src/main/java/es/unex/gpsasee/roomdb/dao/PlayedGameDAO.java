package es.unex.gpsasee.roomdb.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
import es.unex.gpsasee.roomdb.entity.PlayedGame;

@Dao
public interface PlayedGameDAO {
    @Query("SELECT * FROM playedgames")
    List<PlayedGame> getAllPlayedGames();

    @Query("SELECT * FROM playedgames WHERE userid = :userid")
    LiveData<List<PlayedGame>> getPlayedGamesByUser(long userid);

    @Query("SELECT * FROM playedgames WHERE userid = :userid AND gameid = :gameid")
    LiveData<List<PlayedGame>> getPlayedGamesByUserAndGame(long userid, long gameid);

    @Query("SELECT * FROM playedgames WHERE id = :id")
    PlayedGame getPlayedGame(Long id);

    @Insert
    long insert(PlayedGame playedGame);

    @Update
    int update(PlayedGame playedGame);

    @Delete
    int delete(PlayedGame playedGame);

    @Query("DELETE FROM playedgames")
    void deleteAllPlayedGames();

    @Query("DELETE FROM playedgames WHERE userid = :userid AND gameid = :gameid")
    void deletePlayedGamesByUserAndGame(long userid, long gameid);

    @Query("DELETE FROM playedgames WHERE userid = :userid")
    void deleteAllPlayedGamesByUser(long userid);
}
