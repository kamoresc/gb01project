package es.unex.gpsasee.roomdb.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import es.unex.gpsasee.roomdb.entity.GameParams;
import es.unex.gpsasee.roomdb.entity.SuggestedGames;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SuggestedGameDao {

    @Insert(onConflict = REPLACE)
    void bulkInsert(SuggestedGames suggestedGames);

    @Query("SELECT * FROM suggestedGames WHERE id=:id")
    LiveData<SuggestedGames> getGamesById(int id);


//    @Query("SELECT count(*) FROM repo WHERE owner = :owner")
//    int getNumberReposByUser(String owner);

    @Query("SELECT * FROM suggestedGames WHERE id=:id")
    int deleteGamesById(int id);
}
