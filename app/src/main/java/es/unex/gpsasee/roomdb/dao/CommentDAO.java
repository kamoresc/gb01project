package es.unex.gpsasee.roomdb.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.gpsasee.roomdb.entity.Comment;
import es.unex.gpsasee.roomdb.entity.Favourite;

@Dao
public interface CommentDAO {

    @Query("SELECT * FROM comments")
    public LiveData<List<Comment>> getAllComments();

    @Query("SELECT * FROM comments WHERE userid = :userid")
    public List<Comment> getCommentsByUser(long userid);

    @Query("SELECT * FROM comments WHERE userid = :userid AND gameid = :gameid")
    public List<Comment> getCommentsByUserAndGame(long userid, long gameid);

    @Query("SELECT * FROM comments WHERE id = :id")
    public Comment getComment(Long id);

    @Insert
    public long insert(Comment comment);

    @Update
    public int update(Comment comment);

    @Delete
    public int delete(Comment comment);

    @Query("DELETE FROM comments")
    public void deleteAllComments();

    @Query("DELETE FROM comments WHERE userid = :userid AND gameid = :gameid")
    public void deleteCommentByUserAndGame(long userid, long gameid);

    @Query("DELETE FROM comments WHERE userid = :userid")
    public void deleteAllCommentsByUser(long userid);

}
