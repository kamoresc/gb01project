package es.unex.gpsasee.roomdb.converter;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import es.unex.gpsasee.gameModel.GDeveloper;
import es.unex.gpsasee.gameModel.GPlatform__;
import es.unex.gpsasee.model.Result;

public class ResultConverter {

    @TypeConverter
    public String fromList(List<Result> result) {
        if (result == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Result>>() {}.getType();
        String json = gson.toJson(result, type);
        return json;
    }

    @TypeConverter
    public String fromGP(List<GPlatform__> result) {
        if (result == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<GPlatform__>>() {}.getType();
        String json = gson.toJson(result, type);
        return json;
    }

    @TypeConverter
    public List<GPlatform__> toGP(String result) {
        if (result == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Result>>() {}.getType();
        List<GPlatform__> resultList = gson.fromJson(result, type);
        return resultList;
    }

    @TypeConverter
    public String fromGD(List<GDeveloper> result) {
        if (result == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<GDeveloper>>() {}.getType();
        String json = gson.toJson(result, type);
        return json;
    }

    @TypeConverter
    public List<GDeveloper> toGD(String result) {
        if (result == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<GDeveloper>>() {}.getType();
        List<GDeveloper> resultList = gson.fromJson(result, type);
        return resultList;
    }

    @TypeConverter
    public List<Result> toList(String result) {
        if (result == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Result>>() {}.getType();
        List<Result> resultList = gson.fromJson(result, type);
        return resultList;
    }
}
