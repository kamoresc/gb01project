package es.unex.gpsasee.roomdb.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import es.unex.gpsasee.gameModel.Game;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface DetailsGameDAO {

    @Insert(onConflict = REPLACE)
    void bulkInsert(Game game);

    @Query("SELECT * FROM game WHERE id=:id")
    LiveData<Game> getGamesBySearch(int id);

//    @Query("SELECT count(*) FROM repo WHERE owner = :owner")
//    int getNumberReposByUser(String owner);
    @Query("SELECT * FROM game WHERE id=:id")
    int deleteGamesBySearch(int id);
}
