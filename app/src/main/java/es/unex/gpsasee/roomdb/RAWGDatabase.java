package es.unex.gpsasee.roomdb;

import android.content.Context;

import es.unex.gpsasee.gameModel.Game;
import es.unex.gpsasee.roomdb.entity.*;
import es.unex.gpsasee.roomdb.dao.*;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {User.class, Favourite.class, PlayedGame.class, Image.class,Comment.class,GameParams.class,SuggestedGames.class, Game.class}, version = 5, exportSchema = false)
public abstract class RAWGDatabase extends RoomDatabase {
    private static RAWGDatabase instance;

    public static RAWGDatabase getDatabase(Context context) {

        /*final Migration MIGRATION_1_2 = new Migration(1, 2) {
            @Override
            public void migrate(@NonNull SupportSQLiteDatabase database) {
                @Database(entities = {PlayedGame.class}, version = 2, exportSchema = false)
            }
        };*/

        if (instance == null)
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    RAWGDatabase.class,
                    "rawg8.db").build();
        return instance;
    }

    public abstract UserDAO userDao();

    public abstract FavouriteDAO favouriteDAO();

    public abstract PlayedGameDAO playedGameDAO();

    public abstract ImageDAO imageDAO();

    public abstract CommentDAO commentDAO();

    public abstract GameDao gameDao();

    public abstract SuggestedGameDao suggestedGameDao();

    public abstract DetailsGameDAO detailsGameDAO();
}
