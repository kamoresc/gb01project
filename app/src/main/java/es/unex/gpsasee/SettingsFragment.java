package es.unex.gpsasee;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
    ;
    public static final String KEY_LIST_SIZE = "listTextSize";
    public static final String KEY_RESULTS = "listResults";

    static final String TAG = "SettingsFragmentTag";
    private ListPreference mListLetterSize;
    private ListPreference mListResults;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

        addPreferencesFromResource(R.xml.preferences);
        //Now I get a reference of the preferences
        mListLetterSize = (ListPreference) getPreferenceScreen().findPreference(KEY_LIST_SIZE);
        mListResults = (ListPreference) getPreferenceScreen().findPreference(KEY_RESULTS);

        ListPreference themePreference = (ListPreference) findPreference("themePref");
        if (themePreference != null) {
            themePreference.setOnPreferenceChangeListener(
                    new Preference.OnPreferenceChangeListener() {
                        @Override
                        public boolean onPreferenceChange(Preference preference, Object newValue) {
                            String themeOption = (String) newValue;
                            //ThemeHelper.applyTheme(themeOption);
                            return true;
                        }
                    });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Setup the initial values
        mListLetterSize.setSummary("Current value is " + mListLetterSize.getEntry().toString());
        mListResults.setSummary("Current value is " + mListResults.getEntry().toString());

        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister the listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        // Set new summary, when a preference value changes
        if (key.equals(KEY_LIST_SIZE)) {
            mListLetterSize.setSummary("Current value is " + mListLetterSize.getEntry().toString());
        }
        if (key.equals(KEY_RESULTS)) {
            mListResults.setSummary("Current value is " + mListResults.getEntry().toString());
        }
    }

     /*   @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        return view;
    }*/


}
