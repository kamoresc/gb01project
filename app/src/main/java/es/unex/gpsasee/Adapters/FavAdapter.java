package es.unex.gpsasee.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import es.unex.gpsasee.R;
import es.unex.gpsasee.roomdb.entity.Favourite;

//RecyclerView
public class FavAdapter extends RecyclerView.Adapter<FavAdapter.ViewHolder> implements View.OnClickListener{

    List<Favourite> listaFavoritos;
    Context mContext;
    private View.OnClickListener listener;

    public FavAdapter(List<Favourite> listaFavoritos) {
        this.listaFavoritos = listaFavoritos;
    }



    @NonNull
    @Override
    //Enlaza el adaptador con game_item (objeto item)
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Inflamos la vista y la devolvemos
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.game_item, null, false);

        view.setOnClickListener(this);

        return new ViewHolder(view);
    }

    @Override
    //Se encarga de establecer la comunicacion entre adaptatador y ViewHolder
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.asignarDatos(listaFavoritos.get(position));
    }

    @Override
    public int getItemCount() {
        return listaFavoritos.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imagen;
        TextView titulo;
        TextView platform;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imagen = itemView.findViewById(R.id.idImagen);
            titulo = itemView.findViewById(R.id.idNombre);
            platform = itemView.findViewById(R.id.idInfo);
        }

        public void asignarDatos(Favourite favourite) {
            titulo.setText(favourite.getTitle());
            platform.setText(favourite.getPlatform());
            Glide.with(mContext)
                    .load(favourite.getImage())
                    .centerCrop() //4
                    .error(R.drawable.error_image)
                    .into(imagen);
        }
    }
}
