package es.unex.gpsasee.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;
import es.unex.gpsasee.R;
import es.unex.gpsasee.roomdb.entity.Image;

public class GalleryAdapter extends BaseAdapter {

    private Context mContext;

    private List<Image> GalleryImages;

    public GalleryAdapter(Context mContext, List<Image> GalleryImages){
        this.mContext = mContext;
        this.GalleryImages = GalleryImages;
    }

    @Override
    public int getCount() {
        return GalleryImages == null ? 0: GalleryImages.size();
    }

    @Override
    public Object getItem(int i) {
        return GalleryImages.get(i);
    }

    @Override
    public long getItemId(int position) {
        return GalleryImages.get(position).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(
                new GridView.LayoutParams(
                  340,
                  350
                )
        );
        Picasso.with(mContext)
                .load(GalleryImages.get(i).getUri())
                .error(R.drawable.error_image)
                .into(imageView);
        Log.e("URL", "URL imagen: " + GalleryImages.get(i).getUri());
        return imageView;
    }
}
