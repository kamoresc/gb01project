package es.unex.gpsasee.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import es.unex.gpsasee.R;
import es.unex.gpsasee.roomdb.entity.PlayedGame;


public class PlayedAdapter extends RecyclerView.Adapter<PlayedAdapter.ViewHolder> implements View.OnClickListener{

    List<PlayedGame> listaJugados;
    Context mContext;
    private View.OnClickListener listener;

    public PlayedAdapter(List<PlayedGame> listaJugados) {
        this.listaJugados = listaJugados;
    }



    @NonNull
    @Override
    //Enlaza el adaptador con game_item (objeto item)
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Inflamos la vista y la devolvemos
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.game_item, null, false);

        view.setOnClickListener(this);

        return new ViewHolder(view);
    }

    @Override
    //Se encarga de establecer la comunicacion entre adaptatador y ViewHolder
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.asignarDatos(listaJugados.get(position));
    }

    @Override
    public int getItemCount() {
        return listaJugados.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imagen;
        TextView titulo;
        TextView platform;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imagen = itemView.findViewById(R.id.idImagen);
            titulo = itemView.findViewById(R.id.idNombre);
            platform = itemView.findViewById(R.id.idInfo);
        }

        public void asignarDatos(PlayedGame PlayedGame) {
            titulo.setText(PlayedGame.getTitle());
            platform.setText(PlayedGame.getPlatform());
            Glide.with(mContext)
                    .load(PlayedGame.getImage())
                    .centerCrop() //4
                    .error(R.drawable.error_image)
                    .into(imagen);
        }
    }
}
