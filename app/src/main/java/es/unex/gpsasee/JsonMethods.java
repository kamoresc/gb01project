package es.unex.gpsasee;

import android.content.Context;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.entity.Favourite;
import es.unex.gpsasee.roomdb.entity.Image;

public class JsonMethods  {

    private List<Favourite> stdList;

    public boolean res;

    public String devolverTablaFavs(Context o) {
        final String[] res = new String[1];
        AppExecutors.getInstance().diskIO().execute(new Runnable() { //Accesos a la base de datos con un hilo secundario
            @Override
            public void run() {
                stdList = RAWGDatabase.getDatabase(o).favouriteDAO().getAllFavourites();

                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {

                    }
                });

            }

        });
        int i=0;
        while(stdList==null || i<1000000){
            i++;
        }
       /* try {
            AppExecutors.getInstance().mainThread().wait(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        Gson gson = new Gson();
        Type type = new TypeToken<List<Favourite>>(){}.getType();
        res[0] = gson.toJson(stdList, type);
        if(res[0].equals("")){
            res[0]="No hay nada en la tabla Favoritos de la base de datos";
        }
        return res[0];
    }

    public boolean importarJson(Context o,String json){
        res=true;
        try {
            String saas="{\"Favourites\": [ {\"gameid\":12,\"id\":12,\"image\":\"4\"," +
                    "\"platform\":\"PC\",\"title\":\"Grand Theft Auto V\",\"userid\":12}]}";
            JSONObject obj = new JSONObject(json);
            JSONArray m_jArry = obj.getJSONArray("Favourites");
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String id = jo_inside.getString("id");
                String userid = jo_inside.getString("userid");
                String gameid = jo_inside.getString("gameid");
                String title = jo_inside.getString("title");
                String platform = jo_inside.getString("platform");
                String image = jo_inside.getString("image");

                //Add your values in your `ArrayList` as below:
                Favourite fav=new Favourite(Long.parseLong(id),Long.parseLong(userid),Long.parseLong(gameid),title,platform,image);
                AppExecutors.getInstance().diskIO().execute(new Runnable() { //Accesos a la base de datos con un hilo secundario
                    @Override
                    public void run() {
                        if(RAWGDatabase.getDatabase(o).favouriteDAO().getFavourite(fav.getId()) == null) {
                            RAWGDatabase.getDatabase(o).favouriteDAO().insert(fav);
                            AppExecutors.getInstance().mainThread().execute(new Runnable() {
                                @Override
                                public void run() {
                                    //para que se espere al hilo principal
                                }
                            });
                        }
                        else{
                            res=false;
                        }
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
            res=false;
        }
        return res;
    }
}
