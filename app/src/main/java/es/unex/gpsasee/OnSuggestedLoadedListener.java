package es.unex.gpsasee;

import es.unex.gpsasee.model.Rawg;


public interface OnSuggestedLoadedListener {

    public void onResultLoaded(Rawg rawg);

}
