package es.unex.gpsasee;

import android.app.MediaRouteButton;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import es.unex.gpsasee.ui.ListViewModel;
import es.unex.gpsasee.ui.LoadingDialog;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListFragment extends Fragment
        implements RawgAdapter.OnListInteractionListener
        , SearchDialog.SearchDialogListener,
        AdapterView.OnItemSelectedListener{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private EditText textSearch;
    private String mParam1;
    private String mParam2;
    private Context mContext;

    private RecyclerView recyclerView;
    private RawgAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private TextView tv;
    private ImageButton prev;
    private ImageButton next;
    private ProgressBar mProgressBar;
    private SwipeRefreshLayout swipeRefreshLayout;

    ListViewModel mViewModel;


    public ListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListFragment newInstance(String param1, String param2) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list, container, false);
        //restartValues();
        recyclerView = (RecyclerView) v.findViewById(R.id.resultlist);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new RawgAdapter(getContext(), new ArrayList<>(), this);
        // Parse json file into JsonReader
        tv = v.findViewById(R.id.page_number);
        prev = v.findViewById(R.id.previous_button);
        prev.setVisibility(View.INVISIBLE);
        next = v.findViewById(R.id.next_button);
        next.setVisibility(View.INVISIBLE);
        mProgressBar=v.findViewById(R.id.progressBar);
        swipeRefreshLayout=v.findViewById(R.id.swipeRefreshLayout);
        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factory).get(ListViewModel.class);

        ImageButton b = v.findViewById(R.id.search_button);
        textSearch = v.findViewById(R.id.editTextSearch);

        mViewModel.getRawg().observe(getViewLifecycleOwner(), gameParams -> {
            if(gameParams!=null){
                stopLoading();
                mAdapter.swap(gameParams.getRagw().getResults(),gameParams.getRagw().getCount());
                //loadingDialog.dismissDialog();
                tv.setText(String.valueOf(mViewModel.getPage()));
                //TODO El 20 ese sale de las preferencias, es el numero de juegos por pagina
                if (mViewModel.getPage()==(Math.ceil(mAdapter.getTotalCount()/20.))||mAdapter.getTotalCount()<20){
                    next.setVisibility(View.INVISIBLE);
                }
                else{
                    next.setVisibility(View.VISIBLE);
                }
                if(mViewModel.getPage()==1){
                    prev.setVisibility(View.INVISIBLE);
                }
                else{
                    prev.setVisibility(View.VISIBLE);
                }
            }
            else{
                showLoading();
            }
            // Show the repo list or the loading screen based on whether the repos data exists and is loaded
        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.setName(textSearch.getText().toString());
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.decreasePage();
            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.increasePage();
            }
        });

        Button more = v.findViewById(R.id.more_button);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        Spinner spinner = v.findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mContext, R.array.RSort,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        swipeRefreshLayout.setOnRefreshListener(mViewModel::onRefresh);

        recyclerView.setAdapter(mAdapter);
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
    }

    @Override
    public void onListInteraction(String url) {
        Uri webpage = Uri.parse(url);
        Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
        startActivity(webIntent);
    }

    private void showDialog() {
        SearchDialog searchDialog = new SearchDialog();
        searchDialog.setTargetFragment(this,1);
        searchDialog.show(getFragmentManager(),"searchDialog");
    }


    @Override
    public void applyTexts(String name, String company, int platform, String date1, String date2) {
        String mPlatform=platform==0? null : ""+platform;
        String mDate= date1==null||date2==null?null:date1+","+date2;

        mViewModel.setParams(name,mPlatform,company,mDate);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String mOrdering = interpretateOrdering(parent.getItemAtPosition(position).toString());
        mViewModel.setOrdering(mOrdering);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private String interpretateOrdering(String in){
        String out=null;
        switch (in){
            case "Nombre A-Z":
                out="name";
                break;
            case "Nombre Z-A":
                out="-name";
                break;
            case "Más recientes":
                out="-released";
                break;
            case "Más antiguos":
                out="released";
                break;
            case "Mejor valorados":
                out="-rating";
                break;
            case "Peor valorados":
                out="rating";
                break;
            default:
                out=null;
        }
        return out;
    }

    private void showLoading(){
        mProgressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
    }

    private void stopLoading(){
        mProgressBar.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        recyclerView.setVisibility(View.VISIBLE);
    }
}