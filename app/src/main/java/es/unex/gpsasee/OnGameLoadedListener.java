package es.unex.gpsasee;


import es.unex.gpsasee.gameModel.Game;

public interface OnGameLoadedListener {

    public void onGameLoaded(Game game);

}
