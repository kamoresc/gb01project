package es.unex.gpsasee;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import es.unex.gpsasee.roomdb.entity.User;

public class SessionStore {
    private static final String PREFS_NAME = "user";
    private static final String USERNAME = "username";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String USER_ID = "user_id";
    private static final String DESCRIPTION = "description";

    private static SessionStore SINGLETON = null;

    private SharedPreferences sharedPreferences;

    private MutableLiveData<User> liveData;
    public LiveData<User> currentUser;

    private SessionStore(Context context){
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        liveData = new MutableLiveData<>(getUser());
        currentUser = Transformations.switchMap(liveData, (user) -> new MutableLiveData<>(user));
    }

    public static SessionStore getInstance(Context context) {
        if(SINGLETON == null){
            SINGLETON = new SessionStore(context);
        }

        return SINGLETON;
    }

    public void setUser(final User user){
        SharedPreferences.Editor editor = sharedPreferences.edit();

        if(user == null) {
            editor.clear();
        } else {
            editor.putString(USERNAME, user.getUsername());
            editor.putString(EMAIL, user.getEmail());
            editor.putString(PASSWORD, user.getPassword());
            editor.putLong(USER_ID, user.getId());
            editor.putString(DESCRIPTION, user.getDescription());
        }

        AppExecutors.getInstance().mainThread().execute(() -> liveData.setValue(user));

        editor.commit();
    }

    public User getUser(){
        Long id = sharedPreferences.getLong(USER_ID, -1);

        return id <= 0 ? null : new User(id,
                sharedPreferences.getString(EMAIL, EMAIL),
                sharedPreferences.getString(USERNAME, USERNAME),
                sharedPreferences.getString(PASSWORD, PASSWORD),
                sharedPreferences.getString(DESCRIPTION, DESCRIPTION)
        );
    }
}
