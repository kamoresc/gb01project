package es.unex.gpsasee;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.Snackbar;

import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.entity.User;

public class JsonFragment extends Fragment {

    Button export;
    Button importar;
    TextView tv;
    EditText et;
    JsonMethods jsM;

    public JsonFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.import_export, container, false);
        jsM=new JsonMethods();
        export=view.findViewById(R.id.exportar);
        importar=view.findViewById(R.id.importar);
        tv=view.findViewById(R.id.textView3);
        et=view.findViewById(R.id.editTextImport);

        export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv.setText(jsM.devolverTablaFavs (getActivity().getApplicationContext()));
            }
        });

        importar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = SessionStore.getInstance(getActivity()).getUser();
                if(user==null){
                    Toast.makeText(getActivity().getApplicationContext(), "Necesitas estar logueado para realizar esta función", Toast.LENGTH_LONG).show();
                }
                else {
                    if (jsM.importarJson(getActivity().getApplicationContext(), et.getText().toString())) {
                        Toast.makeText(getActivity().getApplicationContext(), "Base de datos actualizada correctamente", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), "Ha ocurrido un problema; intente revisar el JSon que has proporcionado", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


        return view;
    }
}
