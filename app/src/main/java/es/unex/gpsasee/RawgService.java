package es.unex.gpsasee;

import es.unex.gpsasee.gameModel.Game;
import es.unex.gpsasee.model.Rawg;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RawgService {

    // Ejemplo de query completa: https://api.rawg.io/api/games?page_size=20&page=1&search=zelda&platforms=8&developers=nintendo&dates=2015-10-22,2015-10-25
    // TODO ORDENAR JUEGOS: una query nueva llamada ordering


    @GET("games")
    Call<Rawg> getRawg(@Query("search") String search,
                       @Query("page_size") int pageSize,
                       @Query("page") int page,
                       @Query("platforms") String platforms,
                       @Query("developers") String developers,
                       @Query("dates") String dates,
                       @Query("ordering") String ordering);

    @GET("games/{id}")
    Call<Game> getGame(@Path ("id") int id);

    @GET("games/{id}/suggested")
    Call<Rawg> getSuggested(@Path("id") int id);
}