
package es.unex.gpsasee.model;

import androidx.room.Embedded;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RFilters implements Serializable {

    @SerializedName("years")
    @Expose
    @Embedded
    private List<RYear> RYears = null;

    public List<RYear> getRYears() {
        return RYears;
    }

    public void setRYears(List<RYear> RYears) {
        this.RYears = RYears;
    }

}
