
package es.unex.gpsasee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Result implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("released")
    @Expose
    private String released;
    @SerializedName("tba")
    @Expose
    private Boolean tba;
    @SerializedName("background_image")
    @Expose
    private String backgroundImage;
    @SerializedName("rating")
    @Expose
    private Double rating;
    @SerializedName("rating_top")
    @Expose
    private Integer ratingTop;
    @SerializedName("ratings")
    @Expose
    private List<RRating> RRatings = null;
    @SerializedName("ratings_count")
    @Expose
    private Integer ratingsCount;
    @SerializedName("reviews_text_count")
    @Expose
    private Integer reviewsTextCount;
    @SerializedName("added")
    @Expose
    private Integer added;
    @SerializedName("added_by_status")
    @Expose
    private es.unex.gpsasee.model.RAddedByStatus rAddedByStatus;
    @SerializedName("metacritic")
    @Expose
    private Integer metacritic;
    @SerializedName("playtime")
    @Expose
    private Integer playtime;
    @SerializedName("suggestions_count")
    @Expose
    private Integer suggestionsCount;
    @SerializedName("user_game")
    @Expose
    private Object userGame;
    @SerializedName("reviews_count")
    @Expose
    private Integer reviewsCount;
    @SerializedName("saturated_color")
    @Expose
    private String saturatedColor;
    @SerializedName("dominant_color")
    @Expose
    private String dominantColor;
    @SerializedName("platforms")
    @Expose
    private List<RPlatform> RPlatforms = null;
    @SerializedName("parent_platforms")
    @Expose
    private List<RParentPlatform> RParentPlatforms = null;
    @SerializedName("genres")
    @Expose
    private List<RGenre> RGenres = null;
    @SerializedName("stores")
    @Expose
    private List<RStore> RStores = null;
    @SerializedName("clip")
    @Expose
    private RClip RClip;
    @SerializedName("tags")
    @Expose
    private List<RTag> RTags = null;
    @SerializedName("short_screenshots")
    @Expose
    private List<RShortScreenshot> RShortScreenshots = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public Boolean getTba() {
        return tba;
    }

    public void setTba(Boolean tba) {
        this.tba = tba;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getRatingTop() {
        return ratingTop;
    }

    public void setRatingTop(Integer ratingTop) {
        this.ratingTop = ratingTop;
    }

    public List<RRating> getRRatings() {
        return RRatings;
    }

    public void setRRatings(List<RRating> RRatings) {
        this.RRatings = RRatings;
    }

    public Integer getRatingsCount() {
        return ratingsCount;
    }

    public void setRatingsCount(Integer ratingsCount) {
        this.ratingsCount = ratingsCount;
    }

    public Integer getReviewsTextCount() {
        return reviewsTextCount;
    }

    public void setReviewsTextCount(Integer reviewsTextCount) {
        this.reviewsTextCount = reviewsTextCount;
    }

    public Integer getAdded() {
        return added;
    }

    public void setAdded(Integer added) {
        this.added = added;
    }

    public es.unex.gpsasee.model.RAddedByStatus getAddedByStatus() {
        return rAddedByStatus;
    }

    public void setAddedByStatus(es.unex.gpsasee.model.RAddedByStatus rAddedByStatus) {
        this.rAddedByStatus = rAddedByStatus;
    }

    public Integer getMetacritic() {
        return metacritic;
    }

    public void setMetacritic(Integer metacritic) {
        this.metacritic = metacritic;
    }

    public Integer getPlaytime() {
        return playtime;
    }

    public void setPlaytime(Integer playtime) {
        this.playtime = playtime;
    }

    public Integer getSuggestionsCount() {
        return suggestionsCount;
    }

    public void setSuggestionsCount(Integer suggestionsCount) {
        this.suggestionsCount = suggestionsCount;
    }

    public Object getUserGame() {
        return userGame;
    }

    public void setUserGame(Object userGame) {
        this.userGame = userGame;
    }

    public Integer getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(Integer reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

    public String getSaturatedColor() {
        return saturatedColor;
    }

    public void setSaturatedColor(String saturatedColor) {
        this.saturatedColor = saturatedColor;
    }

    public String getDominantColor() {
        return dominantColor;
    }

    public void setDominantColor(String dominantColor) {
        this.dominantColor = dominantColor;
    }

    public List<RPlatform> getRPlatforms() {
        return RPlatforms;
    }

    public void setRPlatforms(List<RPlatform> RPlatforms) {
        this.RPlatforms = RPlatforms;
    }

    public List<RParentPlatform> getRParentPlatforms() {
        return RParentPlatforms;
    }

    public void setRParentPlatforms(List<RParentPlatform> RParentPlatforms) {
        this.RParentPlatforms = RParentPlatforms;
    }

    public List<RGenre> getRGenres() {
        return RGenres;
    }

    public void setRGenres(List<RGenre> RGenres) {
        this.RGenres = RGenres;
    }

    public List<RStore> getRStores() {
        return RStores;
    }

    public void setRStores(List<RStore> RStores) {
        this.RStores = RStores;
    }

    public RClip getRClip() {
        return RClip;
    }

    public void setRClip(RClip RClip) {
        this.RClip = RClip;
    }

    public List<RTag> getRTags() {
        return RTags;
    }

    public void setRTags(List<RTag> RTags) {
        this.RTags = RTags;
    }

    public List<RShortScreenshot> getRShortScreenshots() {
        return RShortScreenshots;
    }

    public void setRShortScreenshots(List<RShortScreenshot> RShortScreenshots) {
        this.RShortScreenshots = RShortScreenshots;
    }

}
