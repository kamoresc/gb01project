
package es.unex.gpsasee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RClip implements Serializable {

    @SerializedName("clip")
    @Expose
    private String clip;
    @SerializedName("clips")
    @Expose
    private RClips RClips;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("preview")
    @Expose
    private String preview;

    public String getClip() {
        return clip;
    }

    public void setClip(String clip) {
        this.clip = clip;
    }

    public RClips getRClips() {
        return RClips;
    }

    public void setRClips(RClips RClips) {
        this.RClips = RClips;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

}
