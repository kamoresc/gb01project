
package es.unex.gpsasee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RYear_ implements Serializable {

    @SerializedName("year")
    @Expose
    private Integer year;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("nofollow")
    @Expose
    private Boolean nofollow;

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Boolean getNofollow() {
        return nofollow;
    }

    public void setNofollow(Boolean nofollow) {
        this.nofollow = nofollow;
    }

}
