
package es.unex.gpsasee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RParentPlatform implements Serializable {

    @SerializedName("platform")
    @Expose
    private RPlatform__ platform;

    public RPlatform__ getPlatform() {
        return platform;
    }

    public void setPlatform(RPlatform__ platform) {
        this.platform = platform;
    }

}
