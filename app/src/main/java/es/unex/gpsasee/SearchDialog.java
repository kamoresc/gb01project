package es.unex.gpsasee;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import java.util.Calendar;
import java.util.Date;

public class SearchDialog extends AppCompatDialogFragment implements AdapterView.OnItemSelectedListener {
    private EditText editTextName;
    private EditText editTextCompany;
    private Spinner spinnerPlatform;
    private static String dateString;
    private static TextView dateView1;
    private static TextView dateView2;

    private Context mContext;
    private SearchDialogListener listener;

    private Date mDate;
    private static final int SEVEN_DAYS = 604800000;

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_search, null);

        editTextName = view.findViewById(R.id.game_name);
        editTextCompany = view.findViewById(R.id.game_company);
        spinnerPlatform = view.findViewById(R.id.game_platform);

        builder.setView(view)
                .setTitle("Busqueda avanzada")
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = editTextName.getText().toString().equals("") ? null : editTextName.getText().toString();
                        String company = editTextCompany.getText().toString().equals("") ? null : editTextCompany.getText().toString();
                        int platform = platformToId(spinnerPlatform.getSelectedItem().toString());

                        String date1 = dateView1.getText().toString();
                        String date2 = dateView2.getText().toString();
                        if (date1==date2){
                            date1=null;
                            date2=null;
                        }

                        sendBackResult(name, company, platform, date1, date2);
                    }
                });

        editTextName = view.findViewById(R.id.game_name);

        dateView1 =  view.findViewById(R.id.game_date1);
        dateView2 =  view.findViewById(R.id.game_date2);
        setDefaultDateTime();
        final Button datePickerButton1 =  view.findViewById(R.id.game_date_button1);
        datePickerButton1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment1();
                newFragment.show(getActivity().getFragmentManager(), "datePicker");
            }
        });

        final Button datePickerButton2 =  view.findViewById(R.id.game_date_button2);
        datePickerButton2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new SearchDialog.DatePickerFragment2();
                newFragment.show(getActivity().getFragmentManager(), "datePicker");
            }
        });

        Spinner spinner = view.findViewById(R.id.game_platform);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mContext, R.array.RPlatforms,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext=context;
        try {
            listener = (SearchDialogListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement SearchDialogListener");
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public interface SearchDialogListener{
        void applyTexts(String name, String company, int platform, String date1, String date2);
    }

    public void sendBackResult(String name, String company, int platform, String date1, String date2) {
        // Notice the use of `getTargetFragment` which will be set when the dialog is displayed
        SearchDialogListener listener = (SearchDialogListener) getTargetFragment();
        listener.applyTexts(name,company,platform,date1,date2);
        dismiss();
    }

    private int platformToId(String platform){
        int idPlatform;
        switch (platform){
            case "PC":
                idPlatform=4;
                break;
            case "Play Station 5":
                idPlatform=187;
                break;
            case "Play Station 4":
                idPlatform=18;
                break;
            case "Play Station 3":
                idPlatform=16;
                break;
            case "Play Station 2":
                idPlatform=15;
                break;
            case "Xbox Series X":
                idPlatform=186;
                break;
            case "Xbox One":
                idPlatform=1;
                break;
            case "Xbox 360":
                idPlatform=14;
                break;
            case "Nintendo Switch":
                idPlatform=7;
                break;
            case "Nintendo 3DS":
                idPlatform=8;
                break;
            case "Nintendo DS":
                idPlatform=9;
                break;
            case "Nintendo Wii U":
                idPlatform=10;
                break;
            case "Nintendo Wii":
                idPlatform=11;
                break;
            case "Android":
                idPlatform=21;
                break;
            case "iOS":
                idPlatform=3;
                break;
            default:
                idPlatform=0;
        }

        return idPlatform;
    }

    private void setDefaultDateTime() {

        // Default is current time + 7 days
        mDate = new Date();
        mDate = new Date(mDate.getTime() + SEVEN_DAYS);

        Calendar c = Calendar.getInstance();
        c.setTime(mDate);

        setDateString(c.get(Calendar.YEAR), c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH));

        dateView1.setText(dateString);
        dateView2.setText(dateString);

        //setTimeString(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE),
        //        c.get(Calendar.MILLISECOND));

        //timeView.setText(timeString);
    }

    private static void setDateString(int year, int monthOfYear, int dayOfMonth) {

        // Increment monthOfYear for Calendar/Date -> Time Format setting
        monthOfYear++;
        String mon = "" + monthOfYear;
        String day = "" + dayOfMonth;

        if (monthOfYear < 10)
            mon = "0" + monthOfYear;
        if (dayOfMonth < 10)
            day = "0" + dayOfMonth;

        dateString = year + "-" + mon + "-" + day;
    }

    public static class DatePickerFragment1 extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the current date as the default date in the picker

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            setDateString(year, monthOfYear, dayOfMonth);

            dateView1.setText(dateString);
        }

    }
    public static class DatePickerFragment2 extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the current date as the default date in the picker

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            setDateString(year, monthOfYear, dayOfMonth);

            dateView2.setText(dateString);
        }

    }

}
