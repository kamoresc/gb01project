package es.unex.gpsasee.data;

import androidx.annotation.NonNull;
import androidx.room.Ignore;

import java.util.Objects;

public class SearchData {
    @NonNull
    private String name;
    @NonNull
    private int page;
    @NonNull
    private String platform;
    @NonNull
    private String company;
    @NonNull
    private String date;
    @NonNull
    private String ordering;

    @Ignore
    public SearchData(String name, int page, String platform, String company, String date, String ordering) {
        this.name = name;
        this.page = page;
        this.platform = platform;
        this.company = company;
        this.date = date;
        this.ordering = ordering;
    }

    public SearchData() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrdering() {
        return ordering;
    }

    public void setOrdering(String ordering) {
        this.ordering = ordering;
    }

    @Override
    public String toString() {
        return "SearchData{" +
                "name='" + name + '\'' +
                ", page=" + page +
                ", platform='" + platform + '\'' +
                ", company='" + company + '\'' +
                ", date='" + date + '\'' +
                ", ordering='" + ordering + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchData that = (SearchData) o;
        return page == that.page &&
                name.equals(that.name) &&
                platform.equals(that.platform) &&
                company.equals(that.company) &&
                date.equals(that.date) &&
                ordering.equals(that.ordering);
    }

//    @Override
//    public int hashCode() {
//        return Objects.hash(name, page, platform, company, date, ordering);
//    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((name == null||name=="") ? 0 : name.hashCode());
        result = prime * result + page;
        result = prime * result + ((platform == null||platform=="") ? 0 : platform.hashCode());
        result = prime * result + ((company == null||company=="") ? 0 : company.hashCode());
        result = prime * result + ((date == null||date=="") ? 0 : date.hashCode());
        result = prime * result + ((ordering == null||ordering=="") ? 0 : ordering.hashCode());

        return result;
    }

    public void toNull(){
        if (name=="") name=null;
        if (platform=="") platform=null;
        if (company=="") company=null;
        if (date=="") date=null;
        if (ordering=="") ordering=null;
    }

    public void toEmpty(){
        if (name==null) name="";
        if (platform==null) platform="";
        if (company==null) company="";
        if (date==null) date="";
        if (ordering==null) ordering="";
    }
}