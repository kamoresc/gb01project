package es.unex.gpsasee;

import java.io.IOException;

import es.unex.gpsasee.model.Rawg;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SuggestedNetworkLoaderRunnable implements Runnable{

    private final OnSuggestedLoadedListener mOnSuggestedLoadedListener;
    private int mId;

    public SuggestedNetworkLoaderRunnable(int id, OnSuggestedLoadedListener onSuggestedLoadedListener){
        mOnSuggestedLoadedListener = onSuggestedLoadedListener;
        this.mId=id;
    }

    @Override
    public void run() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.rawg.io/api/").addConverterFactory(GsonConverterFactory.create())
                .build();
        RawgService service = retrofit.create(RawgService.class);
        try {
            Rawg rawg = service.getSuggested(mId).execute().body();
            //System.out.println(game.getName() + " "+ game.getDescription());
            AppExecutors.getInstance().mainThread().execute(()->mOnSuggestedLoadedListener.onResultLoaded(rawg));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
