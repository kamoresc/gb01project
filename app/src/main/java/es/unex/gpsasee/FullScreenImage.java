package es.unex.gpsasee;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import es.unex.gpsasee.Adapters.GalleryAdapter;
import es.unex.gpsasee.roomdb.RAWGDatabase;

public class FullScreenImage extends AppCompatActivity {

    ImageView imageViewDetail;
    ImageView deleteImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);
        imageViewDetail = findViewById(R.id.detailImage);
        deleteImage = findViewById(R.id.delete_image);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Detalle Imagen");
        //Recibir ID de la imagen
        Intent intent = getIntent();
        //int posicion = intent.getExtras().getInt("imageId");
        String URL = intent.getExtras().getString("URLImage");
        long idJuego = intent.getLongExtra("IDGame", -1);

        Picasso.with(this)
                .load(URL)
                .error(R.drawable.error_image)
                .into(imageViewDetail);
        deleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        RAWGDatabase.getDatabase(getBaseContext()).imageDAO().deleteByID(idJuego);
                        Snackbar.make(v , "Foto eliminada", Snackbar.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
}