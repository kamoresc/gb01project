package es.unex.gpsasee.ui.logout;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.entity.User;

public class LogoutViewModel extends ViewModel {
    private UserRepository mRepository;

    public LogoutViewModel(UserRepository repository){
        mRepository = repository;
    }

    public LiveData<User> currentUser(){
        return mRepository.getCurrentUser();
    }

    public void logout(){
        mRepository.setCurrentUser(null);
    }
}