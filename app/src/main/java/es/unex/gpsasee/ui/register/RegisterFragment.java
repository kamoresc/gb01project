package es.unex.gpsasee.ui.register;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import es.unex.gpsasee.AppContainer;
import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.ListFragment;
import es.unex.gpsasee.MyApplication;
import es.unex.gpsasee.R;
import es.unex.gpsasee.SessionStore;
import es.unex.gpsasee.roomdb.entity.User;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment {

    private RegisterViewModel mViewModel;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public RegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegisterFragment newInstance(String param1, String param2) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        if(SessionStore.getInstance(getActivity()).getUser() != null){
            FragmentManager fm = getFragmentManager();
            if (fm.getBackStackEntryCount() > 0) {
                fm.popBackStack();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        mViewModel = ViewModelProviders.of(this, appContainer.registerViewModelFactory).get(RegisterViewModel.class);

        LiveData<User> liveData = mViewModel.currentUser();
        liveData.observe(getViewLifecycleOwner(), currentUser -> {
            if(currentUser != null) {
                FragmentManager fm = getFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack();
                }
            } else {

                Button button = view.findViewById(R.id.buttonRegister);
                button.setOnClickListener(v -> {
                    v = getView();
                    String username = ((EditText) v.findViewById(R.id.editTextUsername)).getText().toString();
                    String email = ((EditText) v.findViewById(R.id.editTextEmail)).getText().toString();
                    String emailRepeat = ((EditText) v.findViewById(R.id.editTextEmailRepeat)).getText().toString();
                    String password = ((EditText) v.findViewById(R.id.editTextPassword)).getText().toString();
                    String passwordRepeat = ((EditText) v.findViewById(R.id.editTextPasswordRepeat)).getText().toString();

                    if(username.equals("") || email.equals("") || emailRepeat.equals("")
                            || password.equals("") || passwordRepeat.equals("")) {
                        Snackbar.make(v, "Debes rellenar todos los campos", BaseTransientBottomBar.LENGTH_LONG).show();
                    } else if(!email.equals(emailRepeat)){
                        Snackbar.make(v, "Los correos electrónicos no coinciden", BaseTransientBottomBar.LENGTH_LONG).show();
                    } else if(!password.equals(passwordRepeat)){
                        Snackbar.make(v, "Debes rellenar todos los campos", BaseTransientBottomBar.LENGTH_LONG).show();
                    } else {
                        final User user = new User(null, email, username, password, "");

                        AppExecutors.getInstance().diskIO().execute(() -> {
                            LiveData<User> liveuser = mViewModel.getUserByName(user.getUsername());
                            AppExecutors.getInstance().mainThread().execute(() -> liveuser.observe(getViewLifecycleOwner(), checkuser -> {
                                if(checkuser != null) {
                                    getActivity().runOnUiThread(() -> Snackbar.make(getView(), "Ya existe un usuario con ese nombre", Snackbar.LENGTH_LONG).show());
                                } else {
                                    AppExecutors.getInstance().diskIO().execute(() -> {
                                        mViewModel.registerUser(user);

                                        Fragment fragment2 = new ListFragment();
                                        FragmentManager fragmentManager = getFragmentManager();
                                        FragmentTransaction fragmentTransaction =        fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.nav_host_fragment, fragment2);
                                        fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();
                                    });
                                }

                                liveuser.removeObservers(getViewLifecycleOwner());
                            }));

                        });
                    }
                });
            }

            liveData.removeObservers(getViewLifecycleOwner());
        });

        return view;
    }
}