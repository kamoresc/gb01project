package es.unex.gpsasee.ui.played_games;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.gpsasee.repositories.PlayedGamesListRepository;
import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.entity.PlayedGame;
import es.unex.gpsasee.roomdb.entity.User;

public class PlayedGamesListViewModel extends ViewModel {

    private final PlayedGamesListRepository mRepository;
    private final UserRepository mUserRepository;

    public PlayedGamesListViewModel(PlayedGamesListRepository repository, UserRepository userRepository) {
        mRepository = repository;
        mUserRepository = userRepository;
    }

    public LiveData<User> currentUser(){
        return mUserRepository.getCurrentUser();
    }

    public LiveData<List<PlayedGame>> getPlayedGames(long user) {
        return mRepository.getPlayedGames(user);
    }
}
