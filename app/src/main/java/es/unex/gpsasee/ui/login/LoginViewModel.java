package es.unex.gpsasee.ui.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.entity.User;

public class LoginViewModel extends ViewModel {
    private UserRepository mRepository;

    public LoginViewModel(UserRepository repository){
        mRepository = repository;
    }

    public LiveData<User> currentUser(){
        return mRepository.getCurrentUser();
    }

    public LiveData<User> checkLogin(String username, String password){
        return mRepository.checkLogin(username, password);
    }

    public void setCurrentUser(User user){
        mRepository.setCurrentUser(user);
    }
}
