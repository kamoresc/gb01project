package es.unex.gpsasee.ui.user;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.entity.User;

public class UserViewModel extends ViewModel {
    private UserRepository mRepository;

    public UserViewModel(UserRepository repository){
        mRepository = repository;
    }

    public LiveData<User> currentUser(){
        return mRepository.getCurrentUser();
    }

    public void editUser(User user){
        mRepository.update(user);
        mRepository.setCurrentUser(user);
    }

    public void deleteUser(User user){
        mRepository.delete(user);
        mRepository.setCurrentUser(null);
    }
}
