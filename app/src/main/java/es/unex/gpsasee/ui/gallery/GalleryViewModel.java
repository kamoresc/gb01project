package es.unex.gpsasee.ui.gallery;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.gpsasee.repositories.ImageRepository;
import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.entity.Image;
import es.unex.gpsasee.roomdb.entity.User;

public class GalleryViewModel extends ViewModel {
    private ImageRepository mImageRepository;
    private UserRepository mUserRepository;

    public GalleryViewModel(ImageRepository repository, UserRepository userRepository){
        mImageRepository = repository;
        mUserRepository = userRepository;
    }

    public LiveData<User> currentUser(){
        return mUserRepository.getCurrentUser();
    }

    public LiveData<List<Image>> getImagesByUser(long user){
        return mImageRepository.getImagesByUser(user);
    }

    public LiveData<List<Image>> getImagesByUserAndGame(long user, long game){
        return mImageRepository.getImagesByUserAndGame(user, game);
    }

    public LiveData<Image> getImage(long id){
        return mImageRepository.getImage(id);
    }
}
