package es.unex.gpsasee.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.gpsasee.repositories.CommentsListRepository;
import es.unex.gpsasee.roomdb.entity.Comment;

public class CommentsListViewModel extends ViewModel {

    private final CommentsListRepository mRepository;
    private final LiveData<List<Comment>> mComments;

    public CommentsListViewModel(CommentsListRepository repository) {
        mRepository = repository;
        mComments = mRepository.getCurrentComments();
    }

    public LiveData<List<Comment>> getComments() {
        return mComments;
    }


}
