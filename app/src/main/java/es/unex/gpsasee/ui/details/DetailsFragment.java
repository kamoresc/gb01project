package es.unex.gpsasee.ui.details;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import es.unex.gpsasee.AppContainer;
import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.DoCommentsFragment;
import es.unex.gpsasee.GameNetworkLoaderRunnable;
import es.unex.gpsasee.MyApplication;
import es.unex.gpsasee.OnGameLoadedListener;
import es.unex.gpsasee.R;
import es.unex.gpsasee.UploadImageActivity;
import es.unex.gpsasee.gameModel.Game;
import es.unex.gpsasee.roomdb.entity.Favourite;
import es.unex.gpsasee.roomdb.entity.PlayedGame;
import es.unex.gpsasee.roomdb.entity.User;
import es.unex.gpsasee.ui.SwipeViewFragment;
import es.unex.gpsasee.ui.gallery.Gallery;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsFragment extends Fragment {
    private DetailsViewModel mViewModel;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ARG_PARAM1 = "param1";
    public static final String ARG_PARAM2 = "param2";
    public static final String ARG_PARAM3 = "param3";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;

    private TextView titleText;
    private TextView platfomrText;
    private TextView companyText;
    private Button statusText;
    private ImageView imageView;
    //private LoadingDialog loadingDialog;
    private ImageButton bFav;
    private Button UploadImage;
    private boolean isRegister;
    private ImageView galeria;

    private Button comments;

    private Game game;

    public DetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailsFragment newInstance(String param1, String param2,String param3) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_details, container, false);

        int id = getArguments().getInt(ARG_PARAM1);

        System.out.println(id);

        //Referencia a appContainer
        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        mViewModel = ViewModelProviders.of(this, appContainer.detailsViewModelFactory).get(DetailsViewModel.class);

        mViewModel.currentUser().observe(getViewLifecycleOwner(), user -> {

            //loadingDialog = new LoadingDialog(getActivity());
            //loadingDialog.startLoadingDialog();
            game=new Game(id);
            titleText=v.findViewById(R.id.game_title);
            platfomrText=v.findViewById(R.id.game_platform);
            companyText=v.findViewById(R.id.game_company);
            statusText=v.findViewById(R.id.status);
            imageView=v.findViewById(R.id.game_image);
            //Buscamos referencia boton favoritos
            bFav = v.findViewById(R.id.favButton);

            galeria = v.findViewById(R.id.galeria_juego);
            UploadImage = v.findViewById(R.id.uploadImage);

            comments=v.findViewById(R.id.comment_button);
            comments.setOnClickListener(v1 -> {

                if(isRegister){

                    Intent intent = new Intent(getContext(), DoCommentsFragment.class);
                    intent.putExtra("gameID", (long) game.getId());
                    startActivity(intent);
                }
                else{
                    Snackbar.make(v1, "Error, debes iniciar sesion", Snackbar.LENGTH_SHORT).show();
                }

            });

            bFav.setOnClickListener(v1 -> {
                if(isRegister){
                    revertFavourite(user.getId(),game.getId());
                    refreshFavourite(user.getId(),game.getId());
                    Log.i("Usuario", "ID: " + user.getId());
                }
                else{Snackbar.make(v1, "Error, debes iniciar sesion", Snackbar.LENGTH_SHORT).show();}
            });

            UploadImage.setOnClickListener(v1 -> {
                if(isRegister){
                    Snackbar.make(v1, "Subiendo Imagen...", Snackbar.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity().getBaseContext(), UploadImageActivity.class);
                    //startActivityForResult(intent, ADD_TODO_UPLOADIMAGE_REQUEST);
                    Log.i("Id 1", "ID Juego Details: " + game.getId());
                    intent.putExtra("gameID", game.getId());
                    startActivity(intent);
                }
                else{Snackbar.make(v1, "Error, debes iniciar sesion", Snackbar.LENGTH_SHORT).show();}
            });

            statusText.setOnClickListener(v1 -> {
                if(isRegister){
                    revertPlayedGame(user.getId(), game.getId());
                    refreshPlayedGame(user.getId(),game.getId());
                }
                else{Snackbar.make(v1, "Error, debes iniciar sesion", Snackbar.LENGTH_SHORT).show();}
            });

            galeria.setOnClickListener(v1 -> {
                if (isRegister){
                    AppExecutors.getInstance().diskIO().execute(() -> {
                        //List<Image> listaImagenes = RAWGDatabase.getDatabase(getContext()).imageDAO().getImagesByUserAndGame(userSesionId, game.getId());
                        Intent intent = new Intent(getContext(), Gallery.class);
                        Log.e("hola1", "Llamada desde DetailFragment con id: " + game.getId());
                        intent.putExtra("gameid", (long) game.getId());
                        startActivity(intent);
                    });
                }
                else{Snackbar.make(v1, "Error, debes iniciar sesion", Snackbar.LENGTH_SHORT).show();}
            });

            AppExecutors.getInstance().networkIO().execute(new GameNetworkLoaderRunnable(id, new OnGameLoadedListener() {
                @Override
                public void onGameLoaded(Game lgame) {
                    game=lgame;
                    titleText.setText(game.getName());
                    platfomrText.setText(choosePlatforms());
                    companyText.setText(chooseCompanies());

                    //COMPROBAMOS SI USUARIO HA INICIADO SESION
                    isRegister = user != null;

                    //INICIO PARTE JUEGO JUGADO
                    if(isRegister){refreshPlayedGame(user.getId(), game.getId());}
                    //FIN PARTE JUEGO JUGADO

                    //INICIO PARTE DE FAVORITOS
                    if(isRegister){refreshFavourite(user.getId(), game.getId());}
                    //FIN PARTE FAVORITOS

                    Glide.with(v)  //2 Contexto
                            .load(game.getBackgroundImage()) //3 Foto buena
                            .centerCrop() //4
                            //.placeholder(R.drawable.ic_image_place_holder) //5
                            //.error(R.drawable.ic_broken_image) //6
                            //.fallback(R.drawable.ic_no_image) //7
                            .into(imageView); //8 Donde se pone
            /*TODO hacer la swipe view. Contiene:
            Descripcion: se saca de game.getDescription()
            Comentarios: se sacan de la base de datos (espero)
            Relacionados: esta es la dificil, hay que hacer una nueva entrada en la API con su correspondiente NETWORK LOADER y LOADED LISTENER
            para obtener los juegos relacionados se usa suggested
            por ejemplo para obtener los juegos relacionados con el GTAV la query seria la siguiente
            https://api.rawg.io/api/games/3498/suggested
            en principio deberia valer con el modelo de la carpeta modelo
            */

                    //loadingDialog.dismissDialog();

                    SwipeViewFragment fragment = new SwipeViewFragment();

                    Bundle bundle=new Bundle();
                    bundle.putInt(SwipeViewFragment.ARG_PARAM1, game.getId());
                    bundle.putString(SwipeViewFragment.ARG_PARAM2, game.getDescriptionRaw());
                    fragment.setArguments(bundle);

                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.swipeViewLayout, fragment).commit();
                }
            }));
        });


        return  v;
    }

    private String chooseCompanies() {
        String result="Sin desarrollador";
        if (game.getGDevelopers().size()!=0) {
            result="";
            for (int i = 0; i < game.getGDevelopers().size() && i < 2; i++) {
                result = result + game.getGDevelopers().get(i).getName() + " ";
            }
        }
        return result;
    }

    private String choosePlatforms() {
        String result="Sin plataforma";
        if (game.getPlatforms().size()!=0) {
            result="";
            for (int i = 0; i < game.getPlatforms().size() && i < 3; i++) {
                result = result + game.getPlatforms().get(i).getPlatform().getName() + " ";
            }
        }
        return result;
    }

    //actualiza la vista sobre el icono de favorito
    private void refreshFavourite(long userid, long gameid){
        LiveData<List<Favourite>> result = mViewModel.getFavouritesByUserAndGame(userid, gameid);
        result.observe(getViewLifecycleOwner(), favourites -> {
            if(favourites.isEmpty()){
                AppExecutors.getInstance().mainThread().execute(() -> bFav.setBackgroundColor(Color.RED));
            }
            else{
                AppExecutors.getInstance().mainThread().execute(() -> bFav.setBackgroundColor(Color.YELLOW));
            }
        });

    }

    //Cambia de Favoritto a no favorito y viceversa
    public void revertFavourite(long userid, long gameid) {
        LiveData<List<Favourite>> result = mViewModel.getFavouritesByUserAndGame(userid, gameid);
        result.observe(getViewLifecycleOwner(), favourites -> {
            if(favourites.isEmpty()){
                Favourite newFav = new Favourite(0, userid, gameid, game.getName(), game.getPlatforms().get(0).getPlatform().getName(), game.getBackgroundImage());
                AppExecutors.getInstance().diskIO().execute(() -> mViewModel.insertFavourite(newFav));
        /*Log.e("11", "ID: " + newFav.getId());
        Log.e("12", "User: " + newFav.getUserid());
        Log.e("13", "Game: " + newFav.getGameid());
        Log.e("14", "Title: " + newFav.getTitle());
        Log.e("15", "Platform: " + newFav.getPlatform());
        Log.e("16", "Image: " + newFav.getImage());*/
            }
            else{
                AppExecutors.getInstance().diskIO().execute(() -> mViewModel.deleteFavouriteByUserAndGame(userid,gameid));
            }

            result.removeObservers(getViewLifecycleOwner());
        });

    }

    //Actualiza la vista sobre el boton de juegos jugados
    private void refreshPlayedGame(long userid, long gameid){
        LiveData<List<PlayedGame>> result = mViewModel.getPlayedGamesByUserAndGame(userid,gameid);
        result.observe(getViewLifecycleOwner(), playedGames -> {
            if(playedGames.isEmpty()){
                AppExecutors.getInstance().mainThread().execute(() -> {
                    statusText.setText("NO JUGADO");
                    statusText.setBackgroundColor(Color.RED);
                });
            }
            else{
                AppExecutors.getInstance().mainThread().execute(() -> {
                    statusText.setText("JUGADO");
                    statusText.setBackgroundColor(Color.YELLOW);
                });
            }
        });

    }

    //Cambia de Jugados a No Jugados y viceversa
    public void revertPlayedGame(long userid, long gameid) {

        LiveData<List<PlayedGame>> result = mViewModel.getPlayedGamesByUserAndGame(userid,gameid);
        result.observe(getViewLifecycleOwner(), playedGames -> {
            if(playedGames.isEmpty()){
                PlayedGame newPG = new PlayedGame(0,userid, gameid, game.getName(), game.getPlatforms().get(0).getPlatform().getName(), game.getBackgroundImage());
                AppExecutors.getInstance().diskIO().execute(() -> mViewModel.insertPlayed(newPG));
        /*Log.e("Insert1", "Jugado userID: " + userid);
        Log.e("Insert2", "Jugado gameID: " + gameid);
        Log.e("Insert3", "Jugado name: " + game.getName());
        Log.e("Insert4", "Jugado Platform:  " + game.getPlatforms().get(0).getPlatform().getName());
        Log.e("Insert5", "Jugado Image: " + game.getBackgroundImage());*/
            }
            else{
                AppExecutors.getInstance().diskIO().execute(() -> mViewModel.deletePlayedGamesByUserAndGame(userid,gameid));
            }

            result.removeObservers(getViewLifecycleOwner());
        });


    }
}