package es.unex.gpsasee.ui;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.gpsasee.model.Rawg;
import es.unex.gpsasee.repositories.GameRepository;
import es.unex.gpsasee.roomdb.entity.GameParams;


public class ListViewModel extends ViewModel {

    private final GameRepository mRepository;
    private final LiveData<GameParams> mRepos;
    private String mName = null;
    private int mPage=1;
    private String mPlatform = null;
    private String mCompany =null;
    private String mDate=null;
    private String mOrdering=null;

    public ListViewModel(GameRepository repository) {
        mRepository = repository;
        mRepos = mRepository.getCurrentGames();
    }

    public void setParams(String name, String platform, String company, String date){
        restartValues();
        mName=name;
        mPlatform=platform;
        mCompany=company;
        mDate=date;
        updateRepository();
    }

    public void setName(String name){
        restartValues();
        mName=name;
        updateRepository();
    }

    public void setPage(int page){
        mPage=page;
        updateRepository();
    }

    public void decreasePage(){
        mPage--;
        updateRepository();
    }

    public void increasePage(){
        mPage++;
        updateRepository();
    }

    public void setOrdering(String ordering){
        mOrdering=ordering;
        updateRepository();
    }

    private void updateRepository(){
        mRepository.setParams(mName,mPage,mPlatform,mCompany,mDate,mOrdering);
    }

    public void onRefresh() {
        mRepository.doFetchGames(mName,mPage,mPlatform,mCompany,mDate,mOrdering);
    }

    public LiveData<GameParams> getRawg() {
        return mRepos;
    }

    public int getPage(){
        return mPage;
    }

    public void restartValues() {
        mName=null;
        mPage=1;
        mDate=null;
        mPlatform=null;
        mCompany=null;
    }
}
