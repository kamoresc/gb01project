package es.unex.gpsasee.ui.fav_games;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.gpsasee.repositories.FavGamesListRepository;
import es.unex.gpsasee.repositories.UserRepository;


/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link FavGamesListRepository}
 */
public class FavGamesListFactory extends ViewModelProvider.NewInstanceFactory{

    private final FavGamesListRepository mRepository;
    private final UserRepository mUserRepository;

    public FavGamesListFactory(FavGamesListRepository repository, UserRepository userRepository) {
        this.mRepository = repository;
        mUserRepository = userRepository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new FavGamesListViewModel(mRepository, mUserRepository);
    }

}
