package es.unex.gpsasee.ui.details;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import es.unex.gpsasee.R;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Detalle");

        int id = getIntent().getIntExtra("id", 0);

        System.out.println(id);

        DetailsFragment fragment = new DetailsFragment();

        Bundle bundle=new Bundle();
        bundle.putInt(DetailsFragment.ARG_PARAM1, id);

        fragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame, fragment)
                .commit();
    }
}