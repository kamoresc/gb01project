package es.unex.gpsasee.ui.fav_games;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import es.unex.gpsasee.Adapters.FavAdapter;
import es.unex.gpsasee.AppContainer;
import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.ui.details.DetailsActivity;
import es.unex.gpsasee.MyApplication;
import es.unex.gpsasee.R;

public class FavGamesListFragment extends Fragment {

    RecyclerView recycler;
    FavAdapter mAdapter;
    private boolean isRegister;

    FavGamesListViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View vDeVendetta = inflater.inflate(R.layout.fragment_fav_games_list, container, false);
        recycler = vDeVendetta.findViewById(R.id.RecyclerId);
        //Para cargar LinearLayout Vertical
        recycler.setLayoutManager(new LinearLayoutManager(getActivity().getBaseContext(), LinearLayoutManager.VERTICAL, false));

        //Referencia a appContainer
        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;

        mViewModel = new ViewModelProvider(this, appContainer.favGamesListFactory).get(FavGamesListViewModel.class);

        mViewModel.currentUser().observe(getViewLifecycleOwner(), user -> {
            if(user != null){
                mViewModel.getFavGames(user.getId()).observe(getViewLifecycleOwner(), favourites -> {
                    if(user != null){
                        mAdapter = new FavAdapter(favourites);
                        AppExecutors.getInstance().mainThread().execute(() -> recycler.setAdapter(mAdapter));
                    }

                    mAdapter.setOnClickListener(v -> {
                        Intent intent = new Intent(getActivity().getBaseContext(), DetailsActivity.class);
                        intent.putExtra("id", (int) favourites.get(recycler.getChildAdapterPosition(v)).getGameid());
                        startActivity(intent);
                    });

                });
            }
        });


        return vDeVendetta;
    }

}