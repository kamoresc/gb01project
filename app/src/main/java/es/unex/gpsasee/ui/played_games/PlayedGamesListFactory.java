package es.unex.gpsasee.ui.played_games;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.gpsasee.repositories.PlayedGamesListRepository;
import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.ui.played_games.PlayedGamesListViewModel;


/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link PlayedGamesListRepository}
 */
public class PlayedGamesListFactory extends ViewModelProvider.NewInstanceFactory{

    private final PlayedGamesListRepository mRepository;
    private final UserRepository mUserRepository;

    public PlayedGamesListFactory(PlayedGamesListRepository repository, UserRepository userRepository) {
        this.mRepository = repository;
        mUserRepository = userRepository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new PlayedGamesListViewModel(mRepository, mUserRepository);
    }

}