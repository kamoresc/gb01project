package es.unex.gpsasee.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.gpsasee.repositories.CommentsListRepository;


/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link CommentsListRepository}
 */
public class CommentsListFactory extends ViewModelProvider.NewInstanceFactory{

    private final CommentsListRepository mRepository;

    public CommentsListFactory(CommentsListRepository repository) {

        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new CommentsListViewModel(mRepository);
    }

}
