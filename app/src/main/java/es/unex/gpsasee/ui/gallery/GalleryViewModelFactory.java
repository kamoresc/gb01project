package es.unex.gpsasee.ui.gallery;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.gpsasee.repositories.ImageRepository;
import es.unex.gpsasee.repositories.UserRepository;

public class GalleryViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final ImageRepository mImageRepository;
    private final UserRepository mUserRepository;

    public GalleryViewModelFactory(ImageRepository repository, UserRepository userRepository){
        mImageRepository = repository;
        mUserRepository = userRepository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new GalleryViewModel(mImageRepository, mUserRepository);
    }
}
