package es.unex.gpsasee.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.entity.User;

public class NavigationDrawerViewModel extends ViewModel {
    private UserRepository mRepository;

    public NavigationDrawerViewModel(UserRepository repository){
        mRepository = repository;
    }

    public LiveData<User> currentUser(){
        return mRepository.getCurrentUser();
    }
}
