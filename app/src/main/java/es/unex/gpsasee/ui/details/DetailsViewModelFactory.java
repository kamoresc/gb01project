package es.unex.gpsasee.ui.details;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.gpsasee.repositories.FavGamesListRepository;
import es.unex.gpsasee.repositories.PlayedGamesListRepository;
import es.unex.gpsasee.repositories.UserRepository;

public class DetailsViewModelFactory extends ViewModelProvider.NewInstanceFactory{
    private final UserRepository mUserRepository;
    private final FavGamesListRepository mFavRepository;
    private final PlayedGamesListRepository mPlayedRepository;

    public DetailsViewModelFactory(UserRepository userRepository, FavGamesListRepository favGamesListRepository, PlayedGamesListRepository playedGamesListRepository) {
        this.mUserRepository = userRepository;
        mFavRepository = favGamesListRepository;
        mPlayedRepository = playedGamesListRepository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new DetailsViewModel(mUserRepository, mFavRepository, mPlayedRepository);
    }
}
