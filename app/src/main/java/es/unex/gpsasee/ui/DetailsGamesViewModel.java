package es.unex.gpsasee.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;


import es.unex.gpsasee.gameModel.Game;
import es.unex.gpsasee.repositories.DetailsRepository;

public class DetailsGamesViewModel extends ViewModel {


    private final DetailsRepository mRepository;
    private final LiveData<Game> mRepos;
    private int mId = -1;

    public DetailsGamesViewModel(DetailsRepository repository) {
        mRepository = repository;
        mRepos = mRepository.getCurrentGames();
    }

    public void setParams(int id){
        restartValues();
        mId=id;
        updateRepository();
    }

    private void updateRepository(){
        mRepository.setParams(mId);
    }

    public void onRefresh() {
        mRepository.doFetchGames(mId);
    }

    public LiveData<Game> getGame() {
        return mRepos;
    }

    public int getId(){
        return mId;
    }

    public void restartValues() {
        mId=-1;
    }
}
