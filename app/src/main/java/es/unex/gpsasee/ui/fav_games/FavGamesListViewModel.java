package es.unex.gpsasee.ui.fav_games;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.gpsasee.repositories.FavGamesListRepository;
import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.entity.Favourite;
import es.unex.gpsasee.roomdb.entity.User;

public class FavGamesListViewModel extends ViewModel {

    private final FavGamesListRepository mRepository;
    private final UserRepository mUserRepository;

    public FavGamesListViewModel(FavGamesListRepository repository, UserRepository userRepository) {
        mRepository = repository;
        mUserRepository = userRepository;
    }

    public LiveData<User> currentUser(){
        return mUserRepository.getCurrentUser();
    }

    public LiveData<List<Favourite>> getFavGames(long user) {
        return mRepository.getFavGames(user);
    }


}
