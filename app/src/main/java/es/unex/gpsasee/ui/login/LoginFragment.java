package es.unex.gpsasee.ui.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;

import es.unex.gpsasee.AppContainer;
import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.ListFragment;
import es.unex.gpsasee.MyApplication;
import es.unex.gpsasee.R;
import es.unex.gpsasee.roomdb.entity.User;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {

    private LoginViewModel mViewModel;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        mViewModel = ViewModelProviders.of(this, appContainer.loginViewModelFactory).get(LoginViewModel.class);

        LiveData<User> liveData = mViewModel.currentUser();
        liveData.observe(getViewLifecycleOwner(), currentUser -> {
            if (currentUser != null) {
                FragmentManager fm = getFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack();
                }
            } else {
                Button button = view.findViewById(R.id.buttonLogin);
                button.setOnClickListener(v -> {
                    final View view1 = getView();
                    final String username = ((EditText) view1.findViewById(R.id.editTextUsername)).getText().toString();
                    final String password = ((EditText) view1.findViewById(R.id.editTextPassword)).getText().toString();

                    if (username.equals("") || password.equals("")) {
                        Snackbar.make(view1, "Debes rellenar todos los campos", Snackbar.LENGTH_LONG).show();
                    } else {
                        AppExecutors.getInstance().diskIO().execute(() -> {
                            LiveData<User> liveuser = mViewModel.checkLogin(username, password);
                            AppExecutors.getInstance().mainThread().execute(() -> liveuser.observe(getViewLifecycleOwner(), user -> {
                                Snackbar.make(view1, "Debes rellenar todos los campos", Snackbar.LENGTH_LONG).show();
                                if (user == null) {
                                    LoginFragment.this.getActivity().runOnUiThread(() -> Snackbar.make(view1, "Nombre de usuario o contraseña incorrectos", Snackbar.LENGTH_LONG).show());

                                } else {

                                    mViewModel.setCurrentUser(user);

                                    Fragment fragment2 = new ListFragment();
                                    FragmentManager fragmentManager = LoginFragment.this.getFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.nav_host_fragment, fragment2);
                                    fragmentTransaction.addToBackStack(null);
                                    fragmentTransaction.commit();
                                }

                                liveuser.removeObservers(getViewLifecycleOwner());
                            }));

                        });

                    }
                });

                liveData.removeObservers(getViewLifecycleOwner());
            }
        });
        return view;
    }
}