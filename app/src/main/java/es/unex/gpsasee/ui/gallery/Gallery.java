package es.unex.gpsasee.ui.gallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import java.util.List;

import es.unex.gpsasee.Adapters.GalleryAdapter;
import es.unex.gpsasee.AppContainer;
import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.FullScreenImage;
import es.unex.gpsasee.MyApplication;
import es.unex.gpsasee.R;
import es.unex.gpsasee.SessionStore;
import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.entity.Image;
import es.unex.gpsasee.roomdb.entity.User;

public class Gallery extends AppCompatActivity {
    private GalleryViewModel mViewModel;

    GridView gridViewImagenes;
    private boolean esGaleria;
    private long GameID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        gridViewImagenes = findViewById(R.id.gridViewImagenes);

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        mViewModel = ViewModelProviders.of(this, appContainer.galleryViewModelFactory).get(GalleryViewModel.class);

        //COMPROBAMOS SI USUARIO HA INICIADO SESION
/*        try {
            userID = SessionStore.getInstance(getBaseContext()).getUser().getId();
            isRegister = true;
        }catch (Exception NullPointerException){
            isRegister = false;
        }*/

        GameID = getIntent().getLongExtra("gameid",-1);
        if(GameID!=-1){
            Log.e("esGaleria", "Llamada desde Juego Concreto: " + GameID);
            esGaleria = false;
        }
        else{
            esGaleria = true;
        }

        mViewModel.currentUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                if(user != null){
                    LiveData<List<Image>> liveData;
                    if(esGaleria){
                        liveData = mViewModel.getImagesByUser(user.getId());
                    }
                    else{
                        liveData = mViewModel.getImagesByUserAndGame(user.getId(), GameID);
                    }
                    liveData.observe(Gallery.this, new Observer<List<Image>>() {
                        @Override
                        public void onChanged(List<Image> images) {
                            gridViewImagenes.setAdapter(new GalleryAdapter(getBaseContext(), images));
                        }
                    });
                }
            }
        });

        gridViewImagenes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                //Pasar parametro intent
                //intent.putExtra("imageId",i);
                mViewModel.getImage(id).observe(Gallery.this, new Observer<Image>() {
                    @Override
                    public void onChanged(Image image) {
                        Intent intent = new Intent(getApplicationContext(),
                                FullScreenImage.class);
                        intent.putExtra("URLImage", image.getUri());
                        intent.putExtra("IDGame", id);
                        startActivity(intent);
                    }
                });

            }
        });

    }

}