package es.unex.gpsasee.ui.details;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.gpsasee.repositories.FavGamesListRepository;
import es.unex.gpsasee.repositories.PlayedGamesListRepository;
import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.entity.Favourite;
import es.unex.gpsasee.roomdb.entity.PlayedGame;
import es.unex.gpsasee.roomdb.entity.User;

public class DetailsViewModel extends ViewModel {
    private UserRepository mUserRepository;
    private FavGamesListRepository mFavRepository;
    private PlayedGamesListRepository mPlayedRepository;

    public DetailsViewModel(UserRepository userRepository, FavGamesListRepository favGamesListRepository, PlayedGamesListRepository playedGamesListRepository) {
        mUserRepository = userRepository;
        mFavRepository = favGamesListRepository;
        mPlayedRepository = playedGamesListRepository;
    }

    public LiveData<User> currentUser(){
        return mUserRepository.getCurrentUser();
    }

    public LiveData<List<Favourite>> getFavouritesByUserAndGame(long user, long game){
        return mFavRepository.getFavGamesByUserAndGame(user, game);
    }

    public LiveData<List<PlayedGame>> getPlayedGamesByUserAndGame(long userid, long gameid) {
        return mPlayedRepository.getPlayedGamesByUserAndGame(userid, gameid);
    }

    public void insertFavourite(Favourite newFav) {
        mFavRepository.insert(newFav);
    }

    public void deleteFavouriteByUserAndGame(long userid, long gameid) {
        mFavRepository.deleteFavByUserAndGame(userid, gameid);
    }

    public void insertPlayed(PlayedGame newPG) {
        mPlayedRepository.insertPlayedGame(newPG);
    }

    public void deletePlayedGamesByUserAndGame(long userid, long gameid) {
        mPlayedRepository.deletePlayedGamesByUserAndGame(userid, gameid);
    }
}
