package es.unex.gpsasee.ui;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import es.unex.gpsasee.Adapters.ViewPagerAdapter;
import es.unex.gpsasee.CommentsFragment;
import es.unex.gpsasee.DescriptionFragment;
import es.unex.gpsasee.R;
import es.unex.gpsasee.SuggestedFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SwipeViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SwipeViewFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ARG_PARAM1 = "param1";
    public static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private AppBarLayout appBar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int id;
    private String description;

    public SwipeViewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SwipeViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SwipeViewFragment newInstance(String param1, String param2) {
        SwipeViewFragment fragment = new SwipeViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_swipe_view, container, false);

        id = getArguments().getInt(ARG_PARAM1);
        description = getArguments().getString(ARG_PARAM2);

        appBar=v.findViewById(R.id.toolbar);
        tabLayout = new TabLayout(getContext());
        appBar.addView(tabLayout);

        viewPager=v.findViewById(R.id.pager);
        addViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }
        });
        tabLayout.setupWithViewPager(viewPager);


        return v;
    }

    private void addViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter=new ViewPagerAdapter(getFragmentManager(),0);
        SuggestedFragment suggestedFragment = new SuggestedFragment();
        Bundle bundle=new Bundle();
        bundle.putInt(SwipeViewFragment.ARG_PARAM1, id);
        suggestedFragment.setArguments(bundle);

        DescriptionFragment descriptionFragment = new DescriptionFragment();
        Bundle bundle2=new Bundle();
        bundle2.putString(DescriptionFragment.ARG_PARAM1,description);
        descriptionFragment.setArguments(bundle2);

        CommentsFragment commentsfragment = new CommentsFragment();
        Bundle bundle3=new Bundle();
        bundle3.putInt(SwipeViewFragment.ARG_PARAM1, id);
        commentsfragment.setArguments(bundle3);

        adapter.addFragment(descriptionFragment, "Descripción");
        adapter.addFragment(commentsfragment, "Comentarios");
        adapter.addFragment(suggestedFragment, "Relacionados");

        viewPager.setAdapter(adapter);
    }
}