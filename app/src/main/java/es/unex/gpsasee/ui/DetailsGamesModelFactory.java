package es.unex.gpsasee.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.gpsasee.repositories.DetailsRepository;


/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link DetailsRepository}
 */
public class DetailsGamesModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final DetailsRepository mRepository;

    public DetailsGamesModelFactory(DetailsRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new DetailsGamesViewModel(mRepository);
    }
}