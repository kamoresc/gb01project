package es.unex.gpsasee.ui.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;

import es.unex.gpsasee.AppContainer;
import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.ListFragment;
import es.unex.gpsasee.MyApplication;
import es.unex.gpsasee.R;
import es.unex.gpsasee.roomdb.entity.User;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFragment extends Fragment {
    private UserViewModel userViewModel;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public UserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserFragment newInstance(String param1, String param2) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user, container, false);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;

        userViewModel = ViewModelProviders.of(this, appContainer.userViewModelFactory).get(UserViewModel.class);
        userViewModel.currentUser().observe(getViewLifecycleOwner(), user -> {

            if(user == null){
                FragmentManager fm = getFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack();
                }
            } else {
                TextView tvUsername = view.findViewById(R.id.textViewUsername);
                tvUsername.setText(user.getUsername());
                TextView tvEmail = view.findViewById(R.id.textViewEmail);
                tvEmail.setText(user.getEmail());
                EditText etDescription = view.findViewById(R.id.editTextDescription);
                etDescription.setText(user.getDescription());

                Button buttonEdit = view.findViewById(R.id.buttonEdit);
                buttonEdit.setOnClickListener(v -> {
                    final View view12 = getView();

                    EditText etDescription1 = view12.findViewById(R.id.editTextDescription);
                    user.setDescription(etDescription1.getText().toString());

                    AppExecutors.getInstance().diskIO().execute(() -> {
                        userViewModel.editUser(user);

                        getActivity().runOnUiThread(() -> Snackbar.make(view12, "Actualizado con éxito", Snackbar.LENGTH_LONG).show());

                    });

                });

                Button buttonRemove = view.findViewById(R.id.buttonRemove);
                buttonRemove.setOnClickListener(v -> {
                    final View view1 = getView();

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setMessage(R.string.delete_user_message)
                            .setTitle(R.string.delete_user_title);

                    // Add the buttons
                    builder.setPositiveButton(R.string.ok, (dialog, id) -> {
                        // User clicked OK button

                        AppExecutors.getInstance().diskIO().execute(() -> {
                            //User user = SessionStore.getInstance(getActivity()).getUser();
                            userViewModel.deleteUser(user);

                            Fragment fragment2 = new ListFragment();
                            FragmentManager fragmentManager = getFragmentManager();
                            FragmentTransaction fragmentTransaction =        fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.nav_host_fragment, fragment2);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();
                        });
                    });
                    builder.setNegativeButton(R.string.cancel, (dialog, id) -> {
                        // User cancelled the dialog
                    });

                    // Create the AlertDialog
                    AlertDialog dialog = builder.create();

                    dialog.show();

                });
            }
        });



        return view;
    }
}