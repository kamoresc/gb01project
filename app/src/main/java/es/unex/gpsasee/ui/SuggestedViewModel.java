package es.unex.gpsasee.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;
import es.unex.gpsasee.repositories.SuggestedRepository;
import es.unex.gpsasee.roomdb.entity.SuggestedGames;


public class SuggestedViewModel extends ViewModel {

    private final SuggestedRepository mRepository;
    private final LiveData<SuggestedGames> mSuggested;
    private int mId = 0;

    public SuggestedViewModel(SuggestedRepository repository) {
        mRepository = repository;
        mSuggested = mRepository.getCurrentRepos();
    }

    public void setId(int id){
        mId = id;
        mRepository.setId(id);
    }

//    public void onRefresh() {
//        mRepository.doFetchRepos(mId);
//    }

    public LiveData<SuggestedGames> getSuggested() {
        return mSuggested;
    }
}
