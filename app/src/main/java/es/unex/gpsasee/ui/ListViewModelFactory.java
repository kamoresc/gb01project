package es.unex.gpsasee.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.gpsasee.repositories.GameRepository;


/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link GameRepository}
 */
public class ListViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final GameRepository mRepository;

    public ListViewModelFactory(GameRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new ListViewModel(mRepository);
    }
}