package es.unex.gpsasee.ui.played_games;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import es.unex.gpsasee.Adapters.PlayedAdapter;
import es.unex.gpsasee.AppContainer;
import es.unex.gpsasee.ui.details.DetailsActivity;
import es.unex.gpsasee.MyApplication;
import es.unex.gpsasee.R;

public class PlayedGamesListFragment extends Fragment {

    TextView mTitulo;
    RecyclerView recycler;
    PlayedAdapter mAdapter;
    PlayedGamesListViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View vDeVendetta = inflater.inflate(R.layout.fragment_fav_games_list, container, false);
        mTitulo = vDeVendetta.findViewById(R.id.textView2);
        mTitulo.setText("Jugados");
        recycler = vDeVendetta.findViewById(R.id.RecyclerId);
        //Para cargar LinearLayout Vertical
        recycler.setLayoutManager(new LinearLayoutManager(getActivity().getBaseContext(), LinearLayoutManager.VERTICAL, false));

        //Referencia a appContainer
        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;

        mViewModel = new ViewModelProvider(this, appContainer.playedGamesListFactory).get(PlayedGamesListViewModel.class);
        mViewModel.currentUser().observe(getViewLifecycleOwner(), user -> {
            if(user != null){
                mViewModel.getPlayedGames(user.getId()).observe(getViewLifecycleOwner(), playedGames -> {
                    if(user != null){
                        mAdapter = new PlayedAdapter(playedGames);
                        recycler.setAdapter(mAdapter);
                    }

                    mAdapter.setOnClickListener(v -> {
                        Intent intent = new Intent(getActivity().getBaseContext(), DetailsActivity.class);
                        intent.putExtra("id", (int) playedGames.get(recycler.getChildAdapterPosition(v)).getGameid());
                        startActivity(intent);
                    });

                });
            }
        });

        return vDeVendetta;
    }

}