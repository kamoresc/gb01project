package es.unex.gpsasee.ui.register;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.entity.User;

public class RegisterViewModel extends ViewModel {
    private UserRepository mRepository;

    public RegisterViewModel(UserRepository repository){
        mRepository = repository;
    }

    public LiveData<User> currentUser(){
        return mRepository.getCurrentUser();
    }

    public LiveData<User> getUserByName(String username){
        return mRepository.getUserByName(username);
    }

    public void registerUser(User user){
        Long id = mRepository.insert(user);
        user.setId(id);
        mRepository.setCurrentUser(user);
    }
}