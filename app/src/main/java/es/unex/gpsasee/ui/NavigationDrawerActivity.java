package es.unex.gpsasee.ui;

import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import es.unex.gpsasee.AppContainer;
import es.unex.gpsasee.MyApplication;
import es.unex.gpsasee.R;
import es.unex.gpsasee.SessionStore;
import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.entity.User;

public class NavigationDrawerActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    private NavigationDrawerViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_register, R.id.nav_login, R.id.nav_user,
                R.id.nav_logout, R.id.listFragment, R.id.gallery,R.id.import_export,R.id.settings)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        RAWGDatabase.getDatabase(this);
        SessionStore.getInstance(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        viewModel = ViewModelProviders.of(this, appContainer.navigationDrawerViewModelFactory).get(NavigationDrawerViewModel.class);

        viewModel.currentUser().observe(this, this::updateUser);

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void updateUser(User user) {
        TextView username = findViewById(R.id.navHeaderUsername);
        TextView email = findViewById(R.id.navHeaderEmail);

        username.setText(user == null ? "Invitado" : user.getUsername());
        email.setText(user == null ? "Inicia sesión o regístrate" : user.getEmail());
    }
}