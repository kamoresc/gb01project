package es.unex.gpsasee;

import java.io.IOException;

import es.unex.gpsasee.gameModel.Game;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GameNetworkLoaderRunnable implements Runnable{

    private final OnGameLoadedListener mOnGameLoadedListener;
    private int mId;

    public GameNetworkLoaderRunnable(int id, OnGameLoadedListener onGameLoadedListener){
        mOnGameLoadedListener = onGameLoadedListener;
        this.mId=id;
    }

    @Override
    public void run() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.rawg.io/api/").addConverterFactory(GsonConverterFactory.create())
                .build();
        RawgService service = retrofit.create(RawgService.class);
        try {
            //TODO introducir el page size mediante las preferencias
            Game game = service.getGame(mId).execute().body();
            //System.out.println(game.getName() + " "+ game.getDescription());
            AppExecutors.getInstance().mainThread().execute(()->mOnGameLoadedListener.onGameLoaded(game));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
