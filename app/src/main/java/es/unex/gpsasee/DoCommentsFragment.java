package es.unex.gpsasee;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.entity.Comment;
import es.unex.gpsasee.ui.NavigationDrawerActivity;

public class DoCommentsFragment extends AppCompatActivity {

    private Button enviar;
    private EditText edit;

    public long GameID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.do_fragment_comments);

        enviar=findViewById(R.id.button);
        edit=findViewById(R.id.editTextcoment);

        GameID = getIntent().getLongExtra("gameID",-1);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(edit.getText().toString()!=null){
                    if(GameID==-1){
                        Snackbar.make(v , "Error al pasar el juego, inténtelo de nuevo", Snackbar.LENGTH_SHORT).show();
                    }
                    else {
                        AppExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                Comment c = new Comment(0, SessionStore.getInstance(getApplicationContext()).getUser().getId(), GameID,
                                        "", edit.getText().toString(), SessionStore.getInstance(getApplicationContext()).getUser().getUsername());
                                RAWGDatabase.getDatabase(getApplicationContext()).commentDAO().insert(c);
                                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        //para que se espere al hilo principal
                                    }
                                });
                            }
                        });
                        Intent intent = new Intent(getBaseContext(), NavigationDrawerActivity.class);
                        startActivity(intent);
                    }

                }
                else{Snackbar.make(v , "Error, no puedes enviar un comentario vacío", Snackbar.LENGTH_SHORT).show();}
            }
        });
    }
}