package es.unex.gpsasee;

import android.util.Log;

import java.io.IOException;


import es.unex.gpsasee.model.Rawg;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResultNetworkLoaderRunnable implements Runnable{

    private final OnResultLoadedListener mOnResultLoadedListener;
    private String mName;
    private int mPage;
    private String mPlatforms;
    private String mDevelopers;
    private String mDates;
    private String mOrdering;


    public ResultNetworkLoaderRunnable(String name, int page, String platforms, String developers, String dates, String ordering, OnResultLoadedListener onResultLoadedListener){
        mOnResultLoadedListener = onResultLoadedListener;
        mName=name;
        mPage=page;
        mPlatforms=platforms;
        mDevelopers=developers;
        mDates=dates;
        mOrdering=ordering;
    }

    @Override
    public void run() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.rawg.io/api/").addConverterFactory(GsonConverterFactory.create())
                .build();
        RawgService service = retrofit.create(RawgService.class);
        try {
            //TODO introducir el page size mediante las preferencias
            System.out.println(mName+" "+mPage);
            Call<Rawg> call = service.getRawg(mName,20, mPage,mPlatforms,mDevelopers,mDates,mOrdering);
            Log.d("URL request: ", call.request().url().toString());
            Rawg rawg = call.execute().body();
            AppExecutors.getInstance().mainThread().execute(()-> mOnResultLoadedListener.onResultLoaded(rawg));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
