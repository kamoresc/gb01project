package es.unex.gpsasee;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.List;

import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.entity.Comment;
import es.unex.gpsasee.roomdb.entity.Favourite;
import es.unex.gpsasee.ui.CommentsListViewModel;

public class CommentsFragment extends Fragment {

    public static final String ARG_PARAM1 = "param1";
    private int mParam1;

    private List<Comment> stdList;

    CommentsListViewModel mViewModel;


    public CommentsFragment() {
        // Required empty public constructor
    }
    public static CommentsFragment newInstance(String param1) {
        CommentsFragment fragment = new CommentsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_comments, container, false);
        Log.d("Param1 vale: ",""+mParam1);
        final String[] res = new String[1];

        //Referencia a appContainer
        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;

        CommentsListViewModel mViewModel;

        mViewModel = new ViewModelProvider(this, appContainer.commentsListFactory).get(CommentsListViewModel.class);

        TextView descriptionTextView=v.findViewById(R.id.textViewComments);

        mViewModel.getComments().observe(getViewLifecycleOwner(), comments -> {
            res[0]="";
            if(comments != null){
                for(int i=0;i<comments.size();i++){
                    if(comments.get(i).getGameid()==mParam1) {
                        res[0] = res[0] + "Comentario hecho por: " + comments.get(i).getUsername() + "\n\n"
                                + comments.get(i).getTexto() + "\n--------------------------------------\n";
                    }
                }
            }
            descriptionTextView.setText(res[0]);
        });



        return v;
    }
}