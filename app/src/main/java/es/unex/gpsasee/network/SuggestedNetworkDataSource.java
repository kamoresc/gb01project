package es.unex.gpsasee.network;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.OnResultLoadedListener;
import es.unex.gpsasee.OnSuggestedLoadedListener;
import es.unex.gpsasee.ResultNetworkLoaderRunnable;
import es.unex.gpsasee.SuggestedNetworkLoaderRunnable;
import es.unex.gpsasee.data.SearchData;
import es.unex.gpsasee.model.Rawg;
import es.unex.gpsasee.roomdb.entity.GameParams;
import es.unex.gpsasee.roomdb.entity.SuggestedGames;

public class SuggestedNetworkDataSource {
    private static final String LOG_TAG = SuggestedNetworkDataSource.class.getSimpleName();
    private static SuggestedNetworkDataSource sInstance;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<SuggestedGames> mDownloadedRawg;

    private SuggestedNetworkDataSource() {
        mDownloadedRawg = new MutableLiveData<>();
    }

    public synchronized static SuggestedNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new SuggestedNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<SuggestedGames> getCurrentGames() {
        return mDownloadedRawg;
    }

    /**
     * Gets the newest rawg
     */

    public void fetchGames(int id){
        AppExecutors.getInstance().networkIO().execute(new SuggestedNetworkLoaderRunnable(id, new OnSuggestedLoadedListener() {
            @Override
            public void onResultLoaded(Rawg rawg) {
                mDownloadedRawg.postValue(new SuggestedGames(id,rawg));
            }
        }));
    }

}
