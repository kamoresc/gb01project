package es.unex.gpsasee.network;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.GameNetworkLoaderRunnable;
import es.unex.gpsasee.OnGameLoadedListener;
import es.unex.gpsasee.gameModel.Game;

public class DetailsNetworkDataSource {
    private static final String LOG_TAG = DetailsNetworkDataSource.class.getSimpleName();
    private static DetailsNetworkDataSource sInstance;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<Game> mDownloadedGame;

    private DetailsNetworkDataSource() {
        mDownloadedGame = new MutableLiveData<>();
    }

    public synchronized static DetailsNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new DetailsNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source for details");
        }
        return sInstance;
    }

    public LiveData<Game> getCurrentDetails() {
        return mDownloadedGame;
    }

    /**
     * Gets the newest game
     */

    public void fetchGames(int id){
        AppExecutors.getInstance().networkIO().execute(new GameNetworkLoaderRunnable(id,
                new OnGameLoadedListener() {
                    @Override
                    public void onGameLoaded(Game game) {
                        mDownloadedGame.postValue(game);
                    }
                }));
    }

}
