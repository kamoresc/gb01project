package es.unex.gpsasee.network;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import es.unex.gpsasee.AppExecutors;
import es.unex.gpsasee.OnResultLoadedListener;
import es.unex.gpsasee.ResultNetworkLoaderRunnable;
import es.unex.gpsasee.data.SearchData;
import es.unex.gpsasee.model.Rawg;
import es.unex.gpsasee.roomdb.entity.GameParams;

public class GameNetworkDataSource {
    private static final String LOG_TAG = GameNetworkDataSource.class.getSimpleName();
    private static GameNetworkDataSource sInstance;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<GameParams> mDownloadedRawg;

    private GameNetworkDataSource() {
        mDownloadedRawg = new MutableLiveData<>();
    }

    public synchronized static GameNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new GameNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<GameParams> getCurrentGames() {
        return mDownloadedRawg;
    }

    /**
     * Gets the newest rawg
     */

    public void fetchGames(String name, int page, String platform, String company, String date, String ordering){
        AppExecutors.getInstance().networkIO().execute(new ResultNetworkLoaderRunnable(name,
                page,
                platform,
                company,
                date,
                ordering,
                new OnResultLoadedListener() {
                    @Override
                    public void onResultLoaded(Rawg rawg) {
                        mDownloadedRawg.postValue(new GameParams(new SearchData(name,page,platform,company,date,ordering),rawg));
                    }
                }));
    }

}
