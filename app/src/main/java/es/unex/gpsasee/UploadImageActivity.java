package es.unex.gpsasee;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;

import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.entity.Image;

public class UploadImageActivity extends AppCompatActivity {

    private EditText uploadText;
    private Button bAccept;
    private Button bCancel;
    private long gameId;
    private long userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image);
        //ActioBar de la Actividad
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Subir Imagen");

        Intent intent = getIntent();
        gameId = intent.getExtras().getInt("gameID");
        userId = SessionStore.getInstance(this).getUser().getId();
        //Log.i("ID 2", "ID Juego Upload: " + gameId);
        //Log.e("ID Usuario", "ID Usuario: " + SessionStore.getInstance(this).getUser().getId());

        //Obtencion de referencias
        uploadText = findViewById(R.id.UploadText);
        bAccept = findViewById(R.id.accept);
        bCancel = findViewById(R.id.cancel);

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        bAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(uploadText.getText().toString().length()==0){
                    Snackbar.make(v , "Error, se debe rellenar el campo", Snackbar.LENGTH_SHORT).show();
                }
                else{
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            Image image = new Image(0,gameId, userId, uploadText.getText().toString());
                            RAWGDatabase.getDatabase(getBaseContext()).imageDAO().insert(image);
                            //RAWGDatabase.getDatabase(getBaseContext()).imageDAO().deleteAll();
                            //Log.e("Bo", "Imagenes Borradas");
                        }
                    });
                    finish();
                }
            }
        });

    }
}