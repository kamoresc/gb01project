
package es.unex.gpsasee.gameModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GParentPlatform {

    @SerializedName("platform")
    @Expose
    private GPlatform_ platform;

    public GPlatform_ getPlatform() {
        return platform;
    }

    public void setPlatform(GPlatform_ platform) {
        this.platform = platform;
    }

}
