
package es.unex.gpsasee.gameModel;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import es.unex.gpsasee.roomdb.converter.ResultConverter;

@Entity(tableName = "game")
public class Game {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("name_original")
    @Expose
    private String nameOriginal;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("metacritic")
    @Expose
    @Ignore
    private Integer metacritic;
    @SerializedName("metacritic_platforms")
    @Expose
    @Ignore
    private List<GMetacriticPlatform> GMetacriticPlatforms = null;
    @SerializedName("released")
    @Expose
    @Ignore
    private String released;
    @SerializedName("tba")
    @Expose
    @Ignore
    private Boolean tba;
    @SerializedName("updated")
    @Expose
    @Ignore
    private String updated;
    @SerializedName("background_image")
    @Expose
    private String backgroundImage;
    @SerializedName("background_image_additional")
    @Expose
    private String backgroundImageAdditional;
    @SerializedName("website")
    @Expose
    @Ignore
    private String website;
    @SerializedName("rating")
    @Expose
    @Ignore
    private Double rating;
    @SerializedName("rating_top")
    @Expose
    @Ignore
    private Integer ratingTop;
    @SerializedName("ratings")
    @Expose
    @Ignore
    private List<GRating> GRatings = null;
    @SerializedName("reactions")
    @Expose
    @Ignore
    private GReactions GReactions;
    @SerializedName("added")
    @Expose
    @Ignore
    private Integer added;
    @SerializedName("added_by_status")
    @Expose
    @Ignore
    private GAddedByStatus GAddedByStatus;
    @SerializedName("playtime")
    @Expose
    @Ignore
    private Integer playtime;
    @SerializedName("screenshots_count")
    @Expose
    @Ignore
    private Integer screenshotsCount;
    @SerializedName("movies_count")
    @Expose
    @Ignore
    private Integer moviesCount;
    @SerializedName("creators_count")
    @Expose
    @Ignore
    private Integer creatorsCount;
    @SerializedName("achievements_count")
    @Expose
    @Ignore
    private Integer achievementsCount;
    @SerializedName("parent_achievements_count")
    @Expose
    @Ignore
    private Integer parentAchievementsCount;
    @SerializedName("reddit_url")
    @Expose
    @Ignore
    private String redditUrl;
    @SerializedName("reddit_name")
    @Expose
    @Ignore
    private String redditName;
    @SerializedName("reddit_description")
    @Expose
    @Ignore
    private String redditDescription;
    @SerializedName("reddit_logo")
    @Expose
    @Ignore
    private String redditLogo;
    @SerializedName("reddit_count")
    @Expose
    @Ignore
    private Integer redditCount;
    @SerializedName("twitch_count")
    @Expose
    @Ignore
    private Integer twitchCount;
    @SerializedName("youtube_count")
    @Expose
    @Ignore
    private Integer youtubeCount;
    @SerializedName("reviews_text_count")
    @Expose
    @Ignore
    private Integer reviewsTextCount;
    @SerializedName("ratings_count")
    @Expose
    @Ignore
    private Integer ratingsCount;
    @SerializedName("suggestions_count")
    @Expose
    @Ignore
    private Integer suggestionsCount;
    @SerializedName("alternative_names")
    @Expose
    @Ignore
    private List<String> alternativeNames = null;
    @SerializedName("metacritic_url")
    @Expose
    @Ignore
    private String metacriticUrl;
    @SerializedName("parents_count")
    @Expose
    @Ignore
    private Integer parentsCount;
    @SerializedName("additions_count")
    @Expose
    @Ignore
    private Integer additionsCount;
    @SerializedName("game_series_count")
    @Expose
    @Ignore
    private Integer gameSeriesCount;
    @SerializedName("user_game")
    @Expose
    @Ignore
    private Object userGame;
    @SerializedName("reviews_count")
    @Expose
    @Ignore
    private Integer reviewsCount;
    @SerializedName("saturated_color")
    @Expose
    @Ignore
    private String saturatedColor;
    @SerializedName("dominant_color")
    @Expose
    @Ignore
    private String dominantColor;
    @SerializedName("parent_platforms")
    @Expose
    @Ignore
    private List<GParentPlatform> GParentPlatforms = null;
    @SerializedName("platforms")
    @Expose
    @TypeConverters(ResultConverter.class)
    private List<GPlatform__> platforms = null;
    @SerializedName("stores")
    @Expose
    @Ignore
    private List<GStore> GStores = null;
    @SerializedName("developers")
    @Expose
    @TypeConverters(ResultConverter.class)
    private List<GDeveloper> GDevelopers = null;
    @SerializedName("genres")
    @Expose
    @Ignore
    private List<GGenre> GGenres = null;
    @SerializedName("tags")
    @Expose
    @Ignore
    private List<GTag> GTags = null;
    @SerializedName("publishers")
    @Expose
    @Ignore
    private List<GPublisher> GPublishers = null;
    @SerializedName("esrb_rating")
    @Expose
    @Ignore
    private GEsrbRating GEsrbRating;
    @SerializedName("clip")
    @Expose
    @Ignore
    private GClip GClip;
    @SerializedName("description_raw")
    @Expose
    private String descriptionRaw;

    public Game(int id){this.id=id;}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameOriginal() {
        return nameOriginal;
    }

    public void setNameOriginal(String nameOriginal) {
        this.nameOriginal = nameOriginal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMetacritic() {
        return metacritic;
    }

    public void setMetacritic(Integer metacritic) {
        this.metacritic = metacritic;
    }

    public List<GMetacriticPlatform> getGMetacriticPlatforms() {
        return GMetacriticPlatforms;
    }

    public void setGMetacriticPlatforms(List<GMetacriticPlatform> GMetacriticPlatforms) {
        this.GMetacriticPlatforms = GMetacriticPlatforms;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public Boolean getTba() {
        return tba;
    }

    public void setTba(Boolean tba) {
        this.tba = tba;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getBackgroundImageAdditional() {
        return backgroundImageAdditional;
    }

    public void setBackgroundImageAdditional(String backgroundImageAdditional) {
        this.backgroundImageAdditional = backgroundImageAdditional;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getRatingTop() {
        return ratingTop;
    }

    public void setRatingTop(Integer ratingTop) {
        this.ratingTop = ratingTop;
    }

    public List<GRating> getGRatings() {
        return GRatings;
    }

    public void setGRatings(List<GRating> GRatings) {
        this.GRatings = GRatings;
    }

    public GReactions getGReactions() {
        return GReactions;
    }

    public void setGReactions(GReactions GReactions) {
        this.GReactions = GReactions;
    }

    public Integer getAdded() {
        return added;
    }

    public void setAdded(Integer added) {
        this.added = added;
    }

    public GAddedByStatus getGAddedByStatus() {
        return GAddedByStatus;
    }

    public void setGAddedByStatus(GAddedByStatus GAddedByStatus) {
        this.GAddedByStatus = GAddedByStatus;
    }

    public Integer getPlaytime() {
        return playtime;
    }

    public void setPlaytime(Integer playtime) {
        this.playtime = playtime;
    }

    public Integer getScreenshotsCount() {
        return screenshotsCount;
    }

    public void setScreenshotsCount(Integer screenshotsCount) {
        this.screenshotsCount = screenshotsCount;
    }

    public Integer getMoviesCount() {
        return moviesCount;
    }

    public void setMoviesCount(Integer moviesCount) {
        this.moviesCount = moviesCount;
    }

    public Integer getCreatorsCount() {
        return creatorsCount;
    }

    public void setCreatorsCount(Integer creatorsCount) {
        this.creatorsCount = creatorsCount;
    }

    public Integer getAchievementsCount() {
        return achievementsCount;
    }

    public void setAchievementsCount(Integer achievementsCount) {
        this.achievementsCount = achievementsCount;
    }

    public Integer getParentAchievementsCount() {
        return parentAchievementsCount;
    }

    public void setParentAchievementsCount(Integer parentAchievementsCount) {
        this.parentAchievementsCount = parentAchievementsCount;
    }

    public String getRedditUrl() {
        return redditUrl;
    }

    public void setRedditUrl(String redditUrl) {
        this.redditUrl = redditUrl;
    }

    public String getRedditName() {
        return redditName;
    }

    public void setRedditName(String redditName) {
        this.redditName = redditName;
    }

    public String getRedditDescription() {
        return redditDescription;
    }

    public void setRedditDescription(String redditDescription) {
        this.redditDescription = redditDescription;
    }

    public String getRedditLogo() {
        return redditLogo;
    }

    public void setRedditLogo(String redditLogo) {
        this.redditLogo = redditLogo;
    }

    public Integer getRedditCount() {
        return redditCount;
    }

    public void setRedditCount(Integer redditCount) {
        this.redditCount = redditCount;
    }

    public Integer getTwitchCount() {
        return twitchCount;
    }

    public void setTwitchCount(Integer twitchCount) {
        this.twitchCount = twitchCount;
    }

    public Integer getYoutubeCount() {
        return youtubeCount;
    }

    public void setYoutubeCount(Integer youtubeCount) {
        this.youtubeCount = youtubeCount;
    }

    public Integer getReviewsTextCount() {
        return reviewsTextCount;
    }

    public void setReviewsTextCount(Integer reviewsTextCount) {
        this.reviewsTextCount = reviewsTextCount;
    }

    public Integer getRatingsCount() {
        return ratingsCount;
    }

    public void setRatingsCount(Integer ratingsCount) {
        this.ratingsCount = ratingsCount;
    }

    public Integer getSuggestionsCount() {
        return suggestionsCount;
    }

    public void setSuggestionsCount(Integer suggestionsCount) {
        this.suggestionsCount = suggestionsCount;
    }

    public List<String> getAlternativeNames() {
        return alternativeNames;
    }

    public void setAlternativeNames(List<String> alternativeNames) {
        this.alternativeNames = alternativeNames;
    }

    public String getMetacriticUrl() {
        return metacriticUrl;
    }

    public void setMetacriticUrl(String metacriticUrl) {
        this.metacriticUrl = metacriticUrl;
    }

    public Integer getParentsCount() {
        return parentsCount;
    }

    public void setParentsCount(Integer parentsCount) {
        this.parentsCount = parentsCount;
    }

    public Integer getAdditionsCount() {
        return additionsCount;
    }

    public void setAdditionsCount(Integer additionsCount) {
        this.additionsCount = additionsCount;
    }

    public Integer getGameSeriesCount() {
        return gameSeriesCount;
    }

    public void setGameSeriesCount(Integer gameSeriesCount) {
        this.gameSeriesCount = gameSeriesCount;
    }

    public Object getUserGame() {
        return userGame;
    }

    public void setUserGame(Object userGame) {
        this.userGame = userGame;
    }

    public Integer getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(Integer reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

    public String getSaturatedColor() {
        return saturatedColor;
    }

    public void setSaturatedColor(String saturatedColor) {
        this.saturatedColor = saturatedColor;
    }

    public String getDominantColor() {
        return dominantColor;
    }

    public void setDominantColor(String dominantColor) {
        this.dominantColor = dominantColor;
    }

    public List<GParentPlatform> getGParentPlatforms() {
        return GParentPlatforms;
    }

    public void setGParentPlatforms(List<GParentPlatform> GParentPlatforms) {
        this.GParentPlatforms = GParentPlatforms;
    }

    public List<GPlatform__> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<GPlatform__> platforms) {
        this.platforms = platforms;
    }

    public List<GStore> getGStores() {
        return GStores;
    }

    public void setGStores(List<GStore> GStores) {
        this.GStores = GStores;
    }

    public List<GDeveloper> getGDevelopers() {
        return GDevelopers;
    }

    public void setGDevelopers(List<GDeveloper> GDevelopers) {
        this.GDevelopers = GDevelopers;
    }

    public List<GGenre> getGGenres() {
        return GGenres;
    }

    public void setGGenres(List<GGenre> GGenres) {
        this.GGenres = GGenres;
    }

    public List<GTag> getGTags() {
        return GTags;
    }

    public void setGTags(List<GTag> GTags) {
        this.GTags = GTags;
    }

    public List<GPublisher> getGPublishers() {
        return GPublishers;
    }

    public void setGPublishers(List<GPublisher> GPublishers) {
        this.GPublishers = GPublishers;
    }

    public GEsrbRating getGEsrbRating() {
        return GEsrbRating;
    }

    public void setGEsrbRating(GEsrbRating GEsrbRating) {
        this.GEsrbRating = GEsrbRating;
    }

    public GClip getGClip() {
        return GClip;
    }

    public void setGClip(GClip GClip) {
        this.GClip = GClip;
    }

    public String getDescriptionRaw() {
        return descriptionRaw;
    }

    public void setDescriptionRaw(String descriptionRaw) {
        this.descriptionRaw = descriptionRaw;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", slug='" + slug + '\'' +
                ", name='" + name + '\'' +
                ", nameOriginal='" + nameOriginal + '\'' +
                ", description='" + description + '\'' +
                ", metacritic=" + metacritic +
                ", GMetacriticPlatforms=" + GMetacriticPlatforms +
                ", released='" + released + '\'' +
                ", tba=" + tba +
                ", updated='" + updated + '\'' +
                ", backgroundImage='" + backgroundImage + '\'' +
                ", backgroundImageAdditional='" + backgroundImageAdditional + '\'' +
                ", website='" + website + '\'' +
                ", rating=" + rating +
                ", ratingTop=" + ratingTop +
                ", GRatings=" + GRatings +
                ", GReactions=" + GReactions +
                ", added=" + added +
                ", GAddedByStatus=" + GAddedByStatus +
                ", playtime=" + playtime +
                ", screenshotsCount=" + screenshotsCount +
                ", moviesCount=" + moviesCount +
                ", creatorsCount=" + creatorsCount +
                ", achievementsCount=" + achievementsCount +
                ", parentAchievementsCount=" + parentAchievementsCount +
                ", redditUrl='" + redditUrl + '\'' +
                ", redditName='" + redditName + '\'' +
                ", redditDescription='" + redditDescription + '\'' +
                ", redditLogo='" + redditLogo + '\'' +
                ", redditCount=" + redditCount +
                ", twitchCount=" + twitchCount +
                ", youtubeCount=" + youtubeCount +
                ", reviewsTextCount=" + reviewsTextCount +
                ", ratingsCount=" + ratingsCount +
                ", suggestionsCount=" + suggestionsCount +
                ", alternativeNames=" + alternativeNames +
                ", metacriticUrl='" + metacriticUrl + '\'' +
                ", parentsCount=" + parentsCount +
                ", additionsCount=" + additionsCount +
                ", gameSeriesCount=" + gameSeriesCount +
                ", userGame=" + userGame +
                ", reviewsCount=" + reviewsCount +
                ", saturatedColor='" + saturatedColor + '\'' +
                ", dominantColor='" + dominantColor + '\'' +
                ", GParentPlatforms=" + GParentPlatforms +
                ", platforms=" + platforms +
                ", GStores=" + GStores +
                ", GDevelopers=" + GDevelopers +
                ", GGenres=" + GGenres +
                ", GTags=" + GTags +
                ", GPublishers=" + GPublishers +
                ", GEsrbRating=" + GEsrbRating +
                ", GClip=" + GClip +
                ", descriptionRaw='" + descriptionRaw + '\'' +
                '}';
    }
}
