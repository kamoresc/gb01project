
package es.unex.gpsasee.gameModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GStore {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("store")
    @Expose
    private GStore_ store;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public GStore_ getStore() {
        return store;
    }

    public void setStore(GStore_ store) {
        this.store = store;
    }

}
