
package es.unex.gpsasee.gameModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GPlatform__ {

    @SerializedName("platform")
    @Expose
    private GPlatform___ platform;
    @SerializedName("released_at")
    @Expose
    private String releasedAt;
    @SerializedName("requirements")
    @Expose
    private Object requirements;

    public GPlatform___ getPlatform() {
        return platform;
    }

    public void setPlatform(GPlatform___ platform) {
        this.platform = platform;
    }

    public String getReleasedAt() {
        return releasedAt;
    }

    public void setReleasedAt(String releasedAt) {
        this.releasedAt = releasedAt;
    }

    public Object getRequirements() {
        return requirements;
    }

    public void setRequirements(Object requirements) {
        this.requirements = requirements;
    }

}
