
package es.unex.gpsasee.gameModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GClip {

    @SerializedName("clip")
    @Expose
    private String clip;
    @SerializedName("clips")
    @Expose
    private GClips GClips;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("preview")
    @Expose
    private String preview;

    public String getClip() {
        return clip;
    }

    public void setClip(String clip) {
        this.clip = clip;
    }

    public GClips getGClips() {
        return GClips;
    }

    public void setGClips(GClips GClips) {
        this.GClips = GClips;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

}
