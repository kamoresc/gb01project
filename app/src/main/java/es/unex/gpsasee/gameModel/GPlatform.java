
package es.unex.gpsasee.gameModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GPlatform {

    @SerializedName("platform")
    @Expose
    private Integer platform;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

}
