
package es.unex.gpsasee.gameModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GMetacriticPlatform {

    @SerializedName("metascore")
    @Expose
    private Integer metascore;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("platform")
    @Expose
    private GPlatform GPlatform;

    public Integer getMetascore() {
        return metascore;
    }

    public void setMetascore(Integer metascore) {
        this.metascore = metascore;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public GPlatform getGPlatform() {
        return GPlatform;
    }

    public void setGPlatform(GPlatform GPlatform) {
        this.GPlatform = GPlatform;
    }

}
