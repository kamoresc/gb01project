package es.unex.gpsasee;

import es.unex.gpsasee.model.Rawg;


public interface OnResultLoadedListener {

    public void onResultLoaded(Rawg rawg);

}
