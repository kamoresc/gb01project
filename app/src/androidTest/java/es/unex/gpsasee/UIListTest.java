package es.unex.gpsasee;

import android.widget.DatePicker;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.contrib.PickerActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.gpsasee.ui.NavigationDrawerActivity;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class UIListTest {

    @Rule
    public ActivityTestRule<NavigationDrawerActivity> mActivityRule =
            new ActivityTestRule<>(NavigationDrawerActivity.class);

    @Before
    public void init(){
        mActivityRule.getActivity().getSupportFragmentManager().beginTransaction();
    }


    @Test
    public void testBasicSearch() throws InterruptedException {
        // Perform a click() action on R.id.fab
        Thread.sleep(2000);
        onView(withId(R.id.next_button)).perform(click());
        Thread.sleep(500);
        onView(withId(R.id.previous_button)).perform(click());
        Thread.sleep(500);
        onView(withId(R.id.editTextSearch)).perform(typeText("fortnite"), closeSoftKeyboard());
        Thread.sleep(500);
        onView(withId(R.id.search_button)).perform(click());
        Thread.sleep(500);
        onView(withId(R.id.next_button)).perform(click());
        Thread.sleep(500);
        onView(withId(R.id.next_button)).perform(click());
        Thread.sleep(500);
        onView(withId(R.id.next_button)).perform(click());
        Thread.sleep(500);
        onView(withId(R.id.next_button)).check(matches(not(isDisplayed())));
        Thread.sleep(500);
        onView(withId(R.id.editTextSearch)).perform(clearText(),typeText("doom"), closeSoftKeyboard());
        Thread.sleep(500);
        onView(withId(R.id.search_button)).perform(click());
        Thread.sleep(500);
        onView(withId(R.id.editTextSearch)).perform(clearText(),typeText("asdfawfoiengaow;eifgnvoei"), closeSoftKeyboard());
        Thread.sleep(500);
        onView(withId(R.id.search_button)).perform(click());
    }

    @Test
    public void testAdvancedSearch() throws InterruptedException {
        Thread.sleep(2000);
        onView(withId(R.id.more_button)).perform(click());
        onView(withId(R.id.game_company)).perform(typeText("nintendo"),closeSoftKeyboard());
        onView(withText("Aceptar")).perform(click());
        Thread.sleep(500);

        onView(withId(R.id.more_button)).perform(click());
        onView(withId(R.id.game_date_button1)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2020, 1, 1));
        onView(withText("OK")).perform(click());
        onView(withId(R.id.game_date_button2)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2020, 12, 31));
        onView(withText("OK")).perform(click());
        onView(withText("Aceptar")).perform(click());
        Thread.sleep(500);

        onView(withId(R.id.more_button)).perform(click());
        onView(withId(R.id.game_name)).perform(typeText("zelda"),closeSoftKeyboard());
        onView(withId(R.id.game_company)).perform(typeText("nintendo"),closeSoftKeyboard());
        onView(withText("Aceptar")).perform(click());
        Thread.sleep(500);
    }

    @Test
    public void testOrdering() throws InterruptedException {
        Thread.sleep(2000);
        onView(withId(R.id.spinner)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("Nombre A-Z"))).perform(click());
        Thread.sleep(500);
        onView(withId(R.id.spinner)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("Nombre Z-A"))).perform(click());
        Thread.sleep(500);
        onView(withId(R.id.spinner)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("Más recientes"))).perform(click());
        Thread.sleep(500);
        onView(withId(R.id.spinner)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("Más antiguos"))).perform(click());
        Thread.sleep(500);
        onView(withId(R.id.spinner)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("Mejor valorados"))).perform(click());
        Thread.sleep(500);
        onView(withId(R.id.spinner)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("Peor valorados"))).perform(click());
        Thread.sleep(500);
    }

}
