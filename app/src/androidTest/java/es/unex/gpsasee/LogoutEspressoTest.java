package es.unex.gpsasee;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.gpsasee.roomdb.entity.User;
import es.unex.gpsasee.ui.NavigationDrawerActivity;
import es.unex.gpsasee.ui.logout.LogoutFragment;
import es.unex.gpsasee.ui.register.RegisterFragment;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class LogoutEspressoTest {
    @Rule
    public ActivityTestRule<NavigationDrawerActivity> activityRule =
            new ActivityTestRule<>(NavigationDrawerActivity.class);

    @Before
    public void init(){
        activityRule.getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.nav_host_fragment, new LogoutFragment())
                .addToBackStack(null)
                .commit();

    }

    @Test
    public void logout() {

    }
}
