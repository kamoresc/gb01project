package es.unex.gpsasee;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Method;

import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.dao.FavouriteDAO;
import es.unex.gpsasee.roomdb.entity.Favourite;

import static org.junit.Assert.assertEquals;

public class getJsonTest {


    /* ---------------------------------------------------------------------------------------------*/
    /*                       INICIO PARTE DE PRUEBAS DE LOS MÉTODOS DEL JSON                        */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos la insercion de un juego a favoritos por Json
    public void ProbarObtencionJson() throws Exception {

        JsonMethods jsonMethods = new JsonMethods();
        Context c= ApplicationProvider.getApplicationContext();
        //importo un Json para que se quede en la BD
        jsonMethods.importarJson(this.getTestContext(),"{\"Favourites\": [{\"gameid\":3498,\"id\":1,\"image\":\"https://media.rawg.io/media/games/84d/84da2ac3fdfc6507807a1808595afb12.jpg\"," +
                "\"platform\":\"PC\",\"title\":\"Grand Theft Auto V\",\"userid\":1}]}" );
        //Leo el Json que acabo de importar
        assertEquals(jsonMethods.devolverTablaFavs(c), "[{\"gameid\":3498,\"id\":1,\"image\":\"https://media.rawg.io/media/games/84d/84da2ac3fdfc6507807a1808595afb12.jpg\",\"platform\":\"PC\",\"title\":\"Grand Theft Auto V\",\"userid\":1}]");
    }


    /* ---------------------------------------------------------------------------------------------*/
    /*                        FIN PARTE DE PRUEBAS DE LOS MÉTODOS DEL JSON                          */
    /* ---------------------------------------------------------------------------------------------*/


    private Context getTestContext()
    {
        try
        {
            Method getTestContext = searchUnitTest.class.getMethod("getTestContext");
            return (Context) getTestContext.invoke(this);
        }
        catch (final Exception exception)
        {
            exception.printStackTrace();
            return null;
        }
    }

}
