package es.unex.gpsasee;

import android.view.Gravity;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.gpsasee.ui.NavigationDrawerActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class commentsExpressoTest {
    @Rule
    public ActivityTestRule<NavigationDrawerActivity> mActivityRule =
            new ActivityTestRule<>(NavigationDrawerActivity.class);

    @Before
    public void init(){
        mActivityRule.getActivity().getSupportFragmentManager().beginTransaction();
    }

    /* ---------------------------------------------------------------------------------------------*/
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Crea un comentario. El usuario ha de estar logueado.
    public void favourite() throws InterruptedException {
        Thread.sleep(2000);
        onView(withId(R.id.resultlist)).perform(RecyclerViewActions.actionOnItemAtPosition(1, click()));
        Thread.sleep(2000);
        onView(withId(R.id.comment_button)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.editTextcoment)).perform(typeText("Muy bueno!"));
        Thread.sleep(1000);
        Espresso.pressBack();
        Thread.sleep(1000);
        onView(withId(R.id.button)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.resultlist)).perform(RecyclerViewActions.actionOnItemAtPosition(1, click()));
        Thread.sleep(1000);
        onView(withId(R.id.constraintLayout3)).perform(swipeUp());
        Thread.sleep(1000);
        onView(withId(R.id.swipeViewLayout)).perform(swipeLeft());
        Thread.sleep(1000);
        onView(withId(R.id.textViewComments)).check(matches(withText("Comentario hecho por: sergio\n\nMuy bueno!\n--------------------------------------\n")));
    }
}