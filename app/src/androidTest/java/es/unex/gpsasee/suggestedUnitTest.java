package es.unex.gpsasee;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Method;

import es.unex.gpsasee.TestUtils.LiveDataTestUtils;
import es.unex.gpsasee.data.SearchData;
import es.unex.gpsasee.model.Rawg;
import es.unex.gpsasee.network.SuggestedNetworkDataSource;
import es.unex.gpsasee.repositories.GameRepository;
import es.unex.gpsasee.repositories.SuggestedRepository;
import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.dao.GameDao;
import es.unex.gpsasee.roomdb.dao.SuggestedGameDao;
import es.unex.gpsasee.roomdb.entity.GameParams;
import es.unex.gpsasee.roomdb.entity.SuggestedGames;
import es.unex.gpsasee.ui.ListViewModel;
import es.unex.gpsasee.ui.SuggestedViewModel;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class suggestedUnitTest {

    @Mock
    SuggestedNetworkDataSource suggestedNetworkDataSource;


    // necessary to test the LiveData
    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setup () {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testSuggestedNetworkDataSource(){
        SuggestedGames suggestedGames = new SuggestedGames();

        MutableLiveData<SuggestedGames> liveGameParams = new MutableLiveData<>();
        liveGameParams.postValue(suggestedGames);

        when(suggestedNetworkDataSource.getCurrentGames()).thenReturn(liveGameParams);

        assertEquals(suggestedNetworkDataSource.getCurrentGames(),liveGameParams);
    }

    @Test
    public void testGameDao() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        SuggestedGameDao suggestedGameDao = database.suggestedGameDao();

        SuggestedGames suggestedGames=buildSuggestedGame();
        suggestedGameDao.bulkInsert(suggestedGames);

        assertEquals(suggestedGames.getId(), LiveDataTestUtils.getValue(suggestedGameDao.getGamesById(1)).getId());
        assertEquals(suggestedGames.getRagw().getCount(), LiveDataTestUtils.getValue(suggestedGameDao.getGamesById(1)).getRagw().getCount());
    }

    @Test
    public void testGameRepository() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        SuggestedGameDao suggestedGameDao = database.suggestedGameDao();
        SuggestedGames suggestedGames=buildSuggestedGame();
        MutableLiveData<SuggestedGames> liveGameParams=new MutableLiveData<>();
        liveGameParams.postValue(suggestedGames);
        when(suggestedNetworkDataSource.getCurrentGames()).thenReturn(liveGameParams);
        SuggestedRepository suggestedRepository=SuggestedRepository.getInstance(suggestedGameDao, suggestedNetworkDataSource);
        suggestedRepository.setId(1);

        assertEquals(suggestedGames.getId(),LiveDataTestUtils.getValue(suggestedRepository.getCurrentRepos()).getId());
        assertEquals(suggestedGames.getRagw().getCount(),LiveDataTestUtils.getValue(suggestedRepository.getCurrentRepos()).getRagw().getCount());
    }

    @Test
    public void testGameViewModel() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        SuggestedGameDao suggestedGameDao = database.suggestedGameDao();
        SuggestedGames suggestedGames=buildSuggestedGame();
        MutableLiveData<SuggestedGames> liveGameParams=new MutableLiveData<>();
        liveGameParams.postValue(suggestedGames);
        when(suggestedNetworkDataSource.getCurrentGames()).thenReturn(liveGameParams);
        SuggestedRepository suggestedRepository=SuggestedRepository.getInstance(suggestedGameDao, suggestedNetworkDataSource);
        SuggestedViewModel suggestedViewModel = new SuggestedViewModel(suggestedRepository);
        suggestedViewModel.setId(1);

        assertEquals(suggestedGames.getId(),LiveDataTestUtils.getValue(suggestedViewModel.getSuggested()).getId());
        assertEquals(suggestedGames.getRagw().getCount(),LiveDataTestUtils.getValue(suggestedViewModel.getSuggested()).getRagw().getCount());
    }

    public SuggestedGames buildSuggestedGame(){
        SuggestedGames sg=new SuggestedGames();
        sg.setId(1);
        Rawg rawg = new Rawg();
        rawg.setCount(20);
        sg.setRagw(rawg);

        return sg;
    }

    private Context getTestContext()
    {
        try
        {
            Method getTestContext = suggestedUnitTest.class.getMethod("getTestContext");
            return (Context) getTestContext.invoke(this);
        }
        catch (final Exception exception)
        {
            exception.printStackTrace();
            return null;
        }
    }
}