package es.unex.gpsasee;

import android.content.Context;
import android.util.Log;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import es.unex.gpsasee.TestUtils.LiveDataTestUtils;
import es.unex.gpsasee.data.SearchData;
import es.unex.gpsasee.model.Rawg;
import es.unex.gpsasee.network.GameNetworkDataSource;
import es.unex.gpsasee.network.SuggestedNetworkDataSource;
import es.unex.gpsasee.repositories.GameRepository;
import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.dao.GameDao;
import es.unex.gpsasee.roomdb.entity.GameParams;
import es.unex.gpsasee.roomdb.entity.SuggestedGames;
import es.unex.gpsasee.ui.ListViewModel;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class searchUnitTest {

    @Mock
    GameNetworkDataSource gameNetworkDataSource;


    // necessary to test the LiveData
    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setup () {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testNetworkDataSource(){
        GameParams gameParams = new GameParams();

        MutableLiveData<GameParams> liveGameParams = new MutableLiveData<>();
        liveGameParams.postValue(gameParams);

        when(gameNetworkDataSource.getCurrentGames()).thenReturn(liveGameParams);

        assertEquals(gameNetworkDataSource.getCurrentGames(),liveGameParams);
    }

    @Test
    public void testGameDao() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        GameDao gameDao = database.gameDao();

        GameParams gameParamsNull=buildGameParamsNull();
        gameDao.bulkInsert(gameParamsNull);
        GameParams gameParams=buildGameParams();
        gameDao.bulkInsert(gameParams);
        GameParams gameParamsOrder=buildGameParamsOrder();
        gameDao.bulkInsert(gameParamsOrder);

        assertEquals(gameParamsNull.getSearchData(), LiveDataTestUtils.getValue(gameDao.getGamesBySearch("Fortnite",1,"","","","")).getSearchData());
        assertEquals(gameParamsNull.getRagw().getCount(), LiveDataTestUtils.getValue(gameDao.getGamesBySearch("Fortnite",1,"","","","")).getRagw().getCount());
        assertEquals(gameParams.getSearchData(), LiveDataTestUtils.getValue(gameDao.getGamesBySearch("Fortnite",1,"4","","","")).getSearchData());
        assertEquals(gameParams.getRagw().getCount(), LiveDataTestUtils.getValue(gameDao.getGamesBySearch("Fortnite",1,"4","","","")).getRagw().getCount());
        assertEquals(gameParamsOrder.getSearchData(), LiveDataTestUtils.getValue(gameDao.getGamesBySearch("Fortnite",1,"4","","","-name")).getSearchData());
        assertEquals(gameParamsOrder.getRagw().getCount(), LiveDataTestUtils.getValue(gameDao.getGamesBySearch("Fortnite",1,"4","","","-name")).getRagw().getCount());

    }

    @Test
    public void testGameRepository() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        GameDao gameDao = database.gameDao();
        GameParams gameParams=buildGameParams();
        MutableLiveData<GameParams> liveGameParams=new MutableLiveData<>();
        liveGameParams.postValue(gameParams);
        when(gameNetworkDataSource.getCurrentGames()).thenReturn(liveGameParams);
        GameRepository gameRepository=GameRepository.getInstance(gameDao,gameNetworkDataSource);
        gameRepository.setParams("Fortnite",1,"4",null,null,null);

        assertEquals(gameParams.getSearchData(),LiveDataTestUtils.getValue(gameRepository.getCurrentGames()).getSearchData());
        assertEquals(gameParams.getRagw().getCount(),LiveDataTestUtils.getValue(gameRepository.getCurrentGames()).getRagw().getCount());
    }

    @Test
    public void testGameViewModelWithNull() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        GameDao gameDao = database.gameDao();
        GameParams gameParams=buildGameParamsNull();
        MutableLiveData<GameParams> liveGameParams=new MutableLiveData<>();
        liveGameParams.postValue(gameParams);
        when(gameNetworkDataSource.getCurrentGames()).thenReturn(liveGameParams);
        GameRepository gameRepository=GameRepository.getInstance(gameDao,gameNetworkDataSource);
        ListViewModel listViewModel = new ListViewModel(gameRepository);
        listViewModel.setName("Fortnite");

        assertEquals(gameParams.getSearchData(),LiveDataTestUtils.getValue(listViewModel.getRawg()).getSearchData());
        assertEquals(gameParams.getRagw().getCount(),LiveDataTestUtils.getValue(listViewModel.getRawg()).getRagw().getCount());
    }

    @Test
    public void testGameViewModel() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        GameDao gameDao = database.gameDao();
        GameParams gameParams=buildGameParams();
        MutableLiveData<GameParams> liveGameParams=new MutableLiveData<>();
        liveGameParams.postValue(gameParams);
        when(gameNetworkDataSource.getCurrentGames()).thenReturn(liveGameParams);
        GameRepository gameRepository=GameRepository.getInstance(gameDao,gameNetworkDataSource);
        ListViewModel listViewModel = new ListViewModel(gameRepository);
        listViewModel.setParams("Fortnite","4",null,null);

        assertEquals(gameParams.getSearchData(),LiveDataTestUtils.getValue(listViewModel.getRawg()).getSearchData());
        assertEquals(gameParams.getRagw().getCount(),LiveDataTestUtils.getValue(listViewModel.getRawg()).getRagw().getCount());
    }

    @Test
    public void testGameViewModelWithOrder() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        GameDao gameDao = database.gameDao();
        GameParams gameParams=buildGameParamsOrder();
        MutableLiveData<GameParams> liveGameParams=new MutableLiveData<>();
        liveGameParams.postValue(gameParams);
        when(gameNetworkDataSource.getCurrentGames()).thenReturn(liveGameParams);
        GameRepository gameRepository=GameRepository.getInstance(gameDao,gameNetworkDataSource);
        ListViewModel listViewModel = new ListViewModel(gameRepository);
        listViewModel.setParams("Fortnite","4",null,null);
        listViewModel.setOrdering("-name");

        assertEquals(gameParams.getSearchData(),LiveDataTestUtils.getValue(listViewModel.getRawg()).getSearchData());
        assertEquals(gameParams.getRagw().getCount(),LiveDataTestUtils.getValue(listViewModel.getRawg()).getRagw().getCount());
    }

    public GameParams buildGameParamsNull(){
        GameParams gp=new GameParams();
        gp.setId(1);
        Rawg rawg = new Rawg();
        rawg.setCount(300);
        gp.setRagw(rawg);
        SearchData sd=new SearchData("Fortnite",1,null,null,null,null);
        sd.toEmpty();
        gp.setSearchData(sd);

        return gp;
    }

    public GameParams buildGameParams(){
        GameParams gp=new GameParams();
        gp.setId(2);
        Rawg rawg = new Rawg();
        rawg.setCount(500);
        gp.setRagw(rawg);
        SearchData sd=new SearchData("Fortnite",1,"4",null,null,null);
        sd.toEmpty();
        gp.setSearchData(sd);

        return gp;
    }

    public GameParams buildGameParamsOrder(){
        GameParams gp=new GameParams();
        gp.setId(3);
        Rawg rawg = new Rawg();
        rawg.setCount(500);
        gp.setRagw(rawg);
        SearchData sd=new SearchData("Fortnite",1,"4",null,null,"-name");
        sd.toEmpty();
        gp.setSearchData(sd);

        return gp;
    }

    private Context getTestContext()
    {
        try
        {
            Method getTestContext = searchUnitTest.class.getMethod("getTestContext");
            return (Context) getTestContext.invoke(this);
        }
        catch (final Exception exception)
        {
            exception.printStackTrace();
            return null;
        }
    }
}