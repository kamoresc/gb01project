package es.unex.gpsasee;

import android.view.Gravity;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import es.unex.gpsasee.ui.NavigationDrawerActivity;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.anything;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class galleryExpressoTest {
    @Rule
    public ActivityTestRule<NavigationDrawerActivity> mActivityRule =
            new ActivityTestRule<>(NavigationDrawerActivity.class);

    @Before
    public void init(){
        mActivityRule.getActivity().getSupportFragmentManager().beginTransaction();
    }

    @Test
    //Añade una imagen en un juego determinado y despues comprueba que existe tanto en la galeria concreta del juego como en la general
    //Como precondicion no debe haber ninguna imagen añadida en la BD previamente
    //Tambien es necesario iniciar las pruebas estando registrado
    public void galleryTest() throws InterruptedException {
        Thread.sleep(2000);
        onView(withId(R.id.resultlist)).perform(RecyclerViewActions.actionOnItemAtPosition(1, click()));
        Thread.sleep(1000);
        onView(withId(R.id.uploadImage)).perform(click());

        Thread.sleep(1000);
        onView(withId(R.id.accept)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.galeria_juego)).perform(click());
        Thread.sleep(4000);

        onData(anything()).inAdapterView(withId(R.id.gridViewImagenes)).atPosition(0).perform(click());

        Thread.sleep(3000);
        Espresso.pressBack();
        Thread.sleep(1000);
        Espresso.pressBack();
        Thread.sleep(1000);
        Espresso.pressBack();
        Thread.sleep(1000);

        onView(withId(R.id.drawer_layout))
                .check(ViewAssertions.matches(isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open());

        Thread.sleep(1000);
        onView(withId(R.id.gallery)).perform(click());
        Thread.sleep(1000);
        onData(anything()).inAdapterView(withId(R.id.gridViewImagenes)).atPosition(0).perform(click());
        Thread.sleep(1000);
    }
}
