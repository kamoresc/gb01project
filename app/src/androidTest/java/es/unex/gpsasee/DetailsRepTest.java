package es.unex.gpsasee;

import android.content.Context;
import android.util.Log;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import es.unex.gpsasee.TestUtils.LiveDataTestUtils;
import es.unex.gpsasee.gameModel.Game;
import es.unex.gpsasee.model.Rawg;
import es.unex.gpsasee.network.DetailsNetworkDataSource;
import es.unex.gpsasee.network.SuggestedNetworkDataSource;
import es.unex.gpsasee.repositories.DetailsRepository;
import es.unex.gpsasee.repositories.FavGamesListRepository;
import es.unex.gpsasee.repositories.PlayedGamesListRepository;
import es.unex.gpsasee.repositories.SuggestedRepository;
import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.dao.DetailsGameDAO;
import es.unex.gpsasee.roomdb.dao.FavouriteDAO;
import es.unex.gpsasee.roomdb.dao.PlayedGameDAO;
import es.unex.gpsasee.roomdb.dao.SuggestedGameDao;
import es.unex.gpsasee.roomdb.dao.UserDAO;
import es.unex.gpsasee.roomdb.entity.Favourite;
import es.unex.gpsasee.roomdb.entity.SuggestedGames;
import es.unex.gpsasee.roomdb.entity.User;
import es.unex.gpsasee.ui.SuggestedViewModel;
import es.unex.gpsasee.ui.details.DetailsViewModel;
import es.unex.gpsasee.ui.fav_games.FavGamesListViewModel;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class DetailsRepTest {
    @Mock
    DetailsNetworkDataSource detailsNetworkDataSource;


    // necessary to test the LiveData
    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setup () {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testDetailsNetworkDataSource(){
        Game game = new Game(3498);

        MutableLiveData<Game> liveGameParams = new MutableLiveData<>();
        liveGameParams.postValue(game);

        when(detailsNetworkDataSource.getCurrentDetails()).thenReturn(liveGameParams);

        assertEquals(detailsNetworkDataSource.getCurrentDetails(),liveGameParams);
    }

    @Test
    public void testDetailsGameDao() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        DetailsGameDAO detailsGameDAO = database.detailsGameDAO();

        //Pongo por ejemplo id y descripción
        Game game=new Game(10);
        game.setDescription("Una increible aventura");
        detailsGameDAO.bulkInsert(game);

        assertEquals(game.getId(), LiveDataTestUtils.getValue(detailsGameDAO.getGamesBySearch(10)).getId());
        assertEquals(game.getDescription() , LiveDataTestUtils.getValue(detailsGameDAO.getGamesBySearch(10)).getDescription());
    }

    @Test
    public void testDetailsRepository() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        DetailsGameDAO detailsGameDAO = database.detailsGameDAO();

        //Pongo por ejemplo id y descripción
        Game game=new Game(10);
        game.setDescription("Una increible aventura");

        MutableLiveData<Game> liveGameParams=new MutableLiveData<>();
        liveGameParams.postValue(game);
        when(detailsNetworkDataSource.getCurrentDetails()).thenReturn(liveGameParams);
        DetailsRepository detailsRepository=DetailsRepository.getInstance(detailsGameDAO,detailsNetworkDataSource);
        detailsRepository.setParams(10);

        assertEquals(game.getId(),LiveDataTestUtils.getValue(detailsRepository.getCurrentGames()).getId());
        assertEquals(game.getDescription(),LiveDataTestUtils.getValue(detailsRepository.getCurrentGames()).getDescription());
    }


    private Context getTestContext()
    {
        try
        {
            Method getTestContext = searchUnitTest.class.getMethod("getTestContext");
            return (Context) getTestContext.invoke(this);
        }
        catch (final Exception exception)
        {
            exception.printStackTrace();
            return null;
        }
    }

}
