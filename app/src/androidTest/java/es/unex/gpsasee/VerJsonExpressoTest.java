package es.unex.gpsasee;

import android.view.Gravity;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.gpsasee.ui.NavigationDrawerActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import static org.hamcrest.core.StringContains.containsString;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class VerJsonExpressoTest {
    @Rule
    public ActivityTestRule<NavigationDrawerActivity> mActivityRule =
            new ActivityTestRule<>(NavigationDrawerActivity.class);

    @Before
    public void init(){
        mActivityRule.getActivity().getSupportFragmentManager().beginTransaction();
    }

    /* ---------------------------------------------------------------------------------------------*/
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Añade a favoritos mediante el método de insertar Json y ve si se ha hecho correctamente. Previamente
    // La lista de favoritos debe estar vacía
    public void favourite() throws InterruptedException {
        Thread.sleep(2000);
        onView(withId(R.id.drawer_layout))
                .check(ViewAssertions.matches(isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open());
        Thread.sleep(1000);
        onView(withId(R.id.import_export)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.editTextImport)).perform(typeText("{\"Favourites\": [{\"gameid\":3498,\"id\":1,\"image\":\"https://media.rawg.io/media/games/84d/84da2ac3fdfc6507807a1808595afb12.jpg\"," +
                "\"platform\":\"PC\",\"title\":\"Grand Theft Auto V\",\"userid\":1}]}"));
        Thread.sleep(1000);
        Espresso.pressBack();
        Thread.sleep(1000);
        onView(withId(R.id.importar)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.exportar)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.textView3)).check(matches(withText("[{\"gameid\":3498,\"id\":1,\"image\":\"https://media.rawg.io/media/games/84d/84da2ac3fdfc6507807a1808595afb12.jpg\",\"platform\":\"PC\",\"title\":\"Grand Theft Auto V\",\"userid\":1}]")));
        Thread.sleep(1000);
    }
}