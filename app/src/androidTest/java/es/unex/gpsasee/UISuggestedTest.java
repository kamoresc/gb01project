package es.unex.gpsasee;

import android.widget.DatePicker;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.contrib.PickerActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.gpsasee.ui.NavigationDrawerActivity;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class UISuggestedTest {

    @Rule
    public ActivityTestRule<NavigationDrawerActivity> mActivityRule =
            new ActivityTestRule<>(NavigationDrawerActivity.class);

    @Before
    public void init(){
        mActivityRule.getActivity().getSupportFragmentManager().beginTransaction();
    }


    @Test
    public void testSuggested() throws InterruptedException {
        Thread.sleep(2000);
        onView(withId(R.id.resultlist)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.constraintLayout3)).perform(swipeUp());
        onView(withId(R.id.swipeViewLayout)).perform(swipeLeft());
        onView(withId(R.id.swipeViewLayout)).perform(swipeLeft());
        Espresso.pressBack();
        Thread.sleep(1000);

        onView(withId(R.id.resultlist)).perform(RecyclerViewActions.actionOnItemAtPosition(5, click()));
        onView(withId(R.id.constraintLayout3)).perform(swipeUp());
        onView(withId(R.id.swipeViewLayout)).perform(swipeLeft());
        onView(withId(R.id.swipeViewLayout)).perform(swipeLeft());
        Thread.sleep(1000);
    }

}
