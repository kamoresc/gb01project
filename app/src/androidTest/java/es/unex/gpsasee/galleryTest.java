package es.unex.gpsasee;

import android.content.Context;
import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import es.unex.gpsasee.TestUtils.LiveDataTestUtils;
import es.unex.gpsasee.repositories.ImageRepository;
import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.dao.ImageDAO;
import es.unex.gpsasee.roomdb.dao.UserDAO;
import es.unex.gpsasee.roomdb.entity.Image;
import es.unex.gpsasee.ui.gallery.GalleryViewModel;

import static org.junit.Assert.assertEquals;

public class galleryTest {
    private ImageDAO ImaDao;
    private RAWGDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, RAWGDatabase.class).build();
        ImaDao = db.imageDAO();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                           INICIO PARTE DE PRUEBAS DEL DAO                                    */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Pruebas sobre Inser y Delete
    public void shouldAddDeleteImage() throws Exception {
        Image ima = new Image(1, 2, 100, "Imagen1");
        ImaDao.insert(ima);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).size(),1);
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).get(0).getId(),1);
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).get(0).getGameid(),2);
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).get(0).getUserid(),100);
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).get(0).getUri(),"Imagen1");
                }
                catch (Exception e){
                    Log.e("ErrorIma", "Salta una excepcion");
                }

            }
        });
        ImaDao.delete(ima);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).size(),0);
                }
                catch (Exception e){
                    Log.e("ErrorIma", "Salta una excepcion");
                }

            }
        });
    }

    @Test
    //Pruebas sobre Update
    public void shouldUpdateImage() throws Exception {
        Image ima = new Image(1, 2, 100, "Imagen1");
        ImaDao.insert(ima);
        ima.setUri("ImagenCambiada");
        ImaDao.update(ima);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).size(),1);
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).get(0).getId(),1);
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).get(0).getGameid(),2);
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).get(0).getUserid(),100);
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).get(0).getUri(),"ImagenCambiada");
                }
                catch (Exception e){
                    Log.e("ErrorIma", "Salta una excepcion");
                }

            }
        });
    }

    @Test
    //Pruebas sobre deleteAllByUser
    public void shouldDeleteAllByUserIma() throws Exception {
        Image ima = new Image(1, 2, 100, "Imagen1");
        Image ima2 = new Image(2, 100, 100, "Imagen2");
        Image ima3 = new Image(3, 100, 99, "Imagen3");
        ImaDao.insert(ima);
        ImaDao.insert(ima2);
        ImaDao.insert(ima3);
        ImaDao.deleteAllByUser(100);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).size(),1);
                }
                catch (Exception e){
                    Log.e("ErrorIma", "Salta una excepcion");
                }

            }
        });
    }

    @Test
    //Pruebas sobre deleteAllByGame
    public void shouldDeleteAllByGameIma() throws Exception {
        Image ima = new Image(1, 2, 100, "Imagen1");
        Image ima2 = new Image(2, 100, 100, "Imagen2");
        Image ima3 = new Image(3, 100, 99, "Imagen3");
        ImaDao.insert(ima);
        ImaDao.insert(ima2);
        ImaDao.insert(ima3);
        ImaDao.deleteAllByGame(2);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).size(),2);
                }
                catch (Exception e){
                    Log.e("ErrorIma", "Salta una excepcion");
                }

            }
        });
    }

    @Test
    //Pruebas sobre deleteAllByGame
    public void shouldDeleteByIDIma() throws Exception {
        Image ima = new Image(1, 2, 100, "Imagen1");
        Image ima2 = new Image(2, 100, 100, "Imagen2");
        Image ima3 = new Image(3, 100, 99, "Imagen3");
        ImaDao.insert(ima);
        ImaDao.insert(ima2);
        ImaDao.insert(ima3);
        ImaDao.deleteAllByGame(2);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).size(),2);
                }
                catch (Exception e){
                    Log.e("ErrorIma", "Salta una excepcion");
                }

            }
        });
    }

    @Test
    //Pruebas sobre deleteAllByGame
    public void shouldDeleteAllIma() throws Exception {
        Image ima = new Image(1, 2, 100, "Imagen1");
        Image ima2 = new Image(2, 100, 100, "Imagen2");
        Image ima3 = new Image(3, 100, 99, "Imagen3");
        ImaDao.insert(ima);
        ImaDao.insert(ima2);
        ImaDao.insert(ima3);
        ImaDao.deleteAll();
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(ImaDao.getAllImages()).size(),0);
                }
                catch (Exception e){
                    Log.e("ErrorIma", "Salta una excepcion");
                }

            }
        });
    }


    /* ---------------------------------------------------------------------------------------------*/
    /*                           FIN PARTE DE PRUEBAS DEL DAO                                       */
    /* ---------------------------------------------------------------------------------------------*/

    /* ---------------------------------------------------------------------------------------------*/
    /*                      INICIO PARTE DE PRUEBAS DEL REPOSITORIO                                 */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos LiveData de Imagenes por Usuario
    public void shouldGetImagesByUser() throws Exception {
        LiveData<List<Image>> aux;
        ImageRepository repo = ImageRepository.getInstance(ImaDao);
        Image ima = new Image(1, 2, 100, "Imagen1");
        ImaDao.insert(ima);
        aux = repo.getImagesByUser(100);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getUri(),"Imagen1");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
        ima.setUri("ImagenCambiada");
        ImaDao.update(ima);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getUri(),"ImagenCambiada");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
    }


    @Test
    //Comprobamos LiveData de Imagenes por Usuario y Juego
    public void shouldGetImagesByUserAndGame() throws Exception {
        LiveData<List<Image>> aux;
        ImageRepository repo = ImageRepository.getInstance(ImaDao);
        Image ima = new Image(1, 2, 100, "Imagen1");
        ImaDao.insert(ima);
        aux = repo.getImagesByUserAndGame(100,2);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getUri(),"Imagen1");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
        ima.setUri("ImagenCambiada");
        ImaDao.update(ima);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getUri(),"ImagenCambiada");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
    }


    @Test
    //Comprobamos LiveData de Imagenes obteniendo una imagen por su ID
    public void shouldGetImage() throws Exception {
        LiveData<Image> aux;
        ImageRepository repo = ImageRepository.getInstance(ImaDao);
        Image ima = new Image(1, 2, 100, "Imagen1");
        ImaDao.insert(ima);
        aux = repo.getImage(ima.getId());
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).getUri(),"Imagen1");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
        ima.setUri("ImagenCambiada");
        ImaDao.update(ima);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).getUri(),"ImagenCambiada");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                         FIN PARTE DE PRUEBAS DEL REPOSITORIO                                 */
    /* ---------------------------------------------------------------------------------------------*/


    /* ---------------------------------------------------------------------------------------------*/
    /*                         INICIO PARTE DE PRUEBAS DEL VIEWMODEL                                */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos la obtencion de elementos desde el ViewModel por usuario
    public void shouldGetImagesByUserVM() throws Exception {
        ImageRepository repo = ImageRepository.getInstance(ImaDao);
        UserDAO userDAO = db.userDao();
        UserRepository urepo = UserRepository.getInstance(userDAO, SessionStore.getInstance(getTestContext()));
        GalleryViewModel VM = new GalleryViewModel(repo, urepo);
        LiveData<List<Image>> aux;
        Image ima = new Image(1, 10, 25, "Imagen1");
        ImaDao.insert(ima);
        aux = VM.getImagesByUser(25);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getUri(),"Imagen1");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
        ima.setUri("ImagenCambiada");
        ImaDao.update(ima);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getUri(),"ImagenCambiada");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
    }

    @Test
    //Comprobamos la obtencion de elementos desde el ViewModel por usuario y juego
    public void shouldGetImagesByUserAndGameVM() throws Exception {
        ImageRepository repo = ImageRepository.getInstance(ImaDao);
        UserDAO userDAO = db.userDao();
        UserRepository urepo = UserRepository.getInstance(userDAO, SessionStore.getInstance(getTestContext()));
        GalleryViewModel VM = new GalleryViewModel(repo, urepo);
        LiveData<List<Image>> aux;
        Image ima = new Image(1, 10, 25, "Imagen1");
        ImaDao.insert(ima);
        aux = VM.getImagesByUserAndGame(25,10);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getUri(),"Imagen1");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
        ima.setUri("ImagenCambiada");
        ImaDao.update(ima);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getUri(),"ImagenCambiada");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
    }

    @Test
    //Comprobamos la obtencion de elementos desde el ViewModel, obtenemos Imagen por ID
    public void shouldGetImageVM() throws Exception {
        ImageRepository repo = ImageRepository.getInstance(ImaDao);
        UserDAO userDAO = db.userDao();
        UserRepository urepo = UserRepository.getInstance(userDAO, SessionStore.getInstance(getTestContext()));
        GalleryViewModel VM = new GalleryViewModel(repo, urepo);
        LiveData<Image> aux;
        Image ima = new Image(1, 10, 25, "Imagen1");
        ImaDao.insert(ima);
        aux = VM.getImage(1);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).getUri(),"Imagen1");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
        ima.setUri("ImagenCambiada");
        ImaDao.update(ima);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).getUri(),"ImagenCambiada");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                           FIN PARTE DE PRUEBAS DEL VIEWMODEL                                 */
    /* ---------------------------------------------------------------------------------------------*/

    private Context getTestContext()
    {
        try
        {
            Method getTestContext = searchUnitTest.class.getMethod("getTestContext");
            return (Context) getTestContext.invoke(this);
        }
        catch (final Exception exception)
        {
            exception.printStackTrace();
            return null;
        }
    }

}
