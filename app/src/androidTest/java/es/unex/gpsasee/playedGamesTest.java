package es.unex.gpsasee;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import es.unex.gpsasee.TestUtils.LiveDataTestUtils;
import es.unex.gpsasee.repositories.PlayedGamesListRepository;
import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.dao.PlayedGameDAO;
import es.unex.gpsasee.roomdb.dao.UserDAO;
import es.unex.gpsasee.roomdb.entity.PlayedGame;
import es.unex.gpsasee.ui.played_games.PlayedGamesListViewModel;

import static org.junit.Assert.assertEquals;

public class playedGamesTest {


    private PlayedGameDAO PlayedDao;
    private RAWGDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, RAWGDatabase.class).build();
        PlayedDao = db.playedGameDAO();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                           INICIO PARTE DE PRUEBAS DEL DAO                                    */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos la insercion y eliminacion de un juego a jugado
    public void shouldAddDeleteplayedoriteToDB() throws Exception {
        PlayedGame played = new PlayedGame(1,1,100, "Titulo1", "Plataforma1","ImagenURL1");
        PlayedDao.insert(played);
        List<PlayedGame> listaplayeds = PlayedDao.getAllPlayedGames();
        assertEquals(listaplayeds.size(),1);
        assertEquals(listaplayeds.get(0).getId(), 1);
        assertEquals(listaplayeds.get(0).getGameid(), 100);
        assertEquals(listaplayeds.get(0).getTitle(), "Titulo1");
        assertEquals(listaplayeds.get(0).getPlatform(), "Plataforma1");
        assertEquals(listaplayeds.get(0).getImage(), "ImagenURL1");
        PlayedDao.delete(played);
        listaplayeds = PlayedDao.getAllPlayedGames();
        assertEquals(listaplayeds.size(),0);
    }

    @Test
    //Comprobamos la actualizacion de un objeto
    public void shouldUpdateplayedoriteToDB() throws Exception {
        PlayedGame played = new PlayedGame(1,1,100, "Titulo2", "Plataforma2","ImagenURL2");
        PlayedDao.insert(played);
        List<PlayedGame> listaplayeds = PlayedDao.getAllPlayedGames();
        assertEquals(listaplayeds.get(0).getTitle(), "Titulo2");
        played.setTitle("NuevoTitulo");
        PlayedDao.update(played);
        listaplayeds = PlayedDao.getAllPlayedGames();
        assertEquals(listaplayeds.get(0).getTitle(), "NuevoTitulo");
        PlayedDao.delete(played);
    }

    @Test
    //Comprobamos eliminacion por ID y GAMEID
    public void shouldDeleteIdGameIdplayedoriteToDB() throws Exception {
        PlayedGame played = new PlayedGame(1,12,24, "Titulo2", "Plataforma2","ImagenURL2");
        PlayedGame played2 = new PlayedGame(2,13,26, "Titulo3", "Plataforma3","ImagenURL3");
        PlayedDao.insert(played);
        PlayedDao.insert(played2);
        //inicialmente solo hay 2
        List<PlayedGame> listaplayeds = PlayedDao.getAllPlayedGames();
        assertEquals(listaplayeds.size(),2);
        //Borramos uno de los elementos
        PlayedDao.deletePlayedGamesByUserAndGame(12,24);
        listaplayeds = PlayedDao.getAllPlayedGames();
        assertEquals(listaplayeds.size(),1);
        //Borramos un elemento que no existe por lo que no se elimina ningun elemento
        PlayedDao.deletePlayedGamesByUserAndGame(1,1);
        listaplayeds = PlayedDao.getAllPlayedGames();
        assertEquals(listaplayeds.size(),1);
        //Borramos el ultimo elemento, dejando la BD sin elementos
        PlayedDao.deletePlayedGamesByUserAndGame(13,26);
        listaplayeds = PlayedDao.getAllPlayedGames();
        assertEquals(listaplayeds.size(),0);
    }

    @Test
    //Comprobamos si fuciona el eliminar por usuario
    public void shouldDeleteByUserplayedoriteToDB() throws Exception {
        PlayedGame played = new PlayedGame(1,200,24, "Titulo2", "Plataforma2","ImagenURL2");
        PlayedGame played2 = new PlayedGame(2,200,26, "Titulo3", "Plataforma3","ImagenURL3");
        PlayedGame played3 = new PlayedGame(3,199,28, "Titulo4", "Plataforma4","ImagenURL4");
        PlayedDao.insert(played);
        PlayedDao.insert(played2);
        PlayedDao.insert(played3);
        //Tenemos 3 elementos en la BD
        List<PlayedGame> listaplayeds = PlayedDao.getAllPlayedGames();
        assertEquals(listaplayeds.size(),3);
        //Borramos los 2 elementos vinculados al usuario 200 (dejando 1 en la BD)
        PlayedDao.deleteAllPlayedGamesByUser(200);
        listaplayeds = PlayedDao.getAllPlayedGames();
        assertEquals(listaplayeds.size(),1);
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                           FIN PARTE DE PRUEBAS DEL DAO                                       */
    /* ---------------------------------------------------------------------------------------------*/

    /* ---------------------------------------------------------------------------------------------*/
    /*                      INICIO PARTE DE PRUEBAS DEL REPOSITORIO                                 */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos LiveData de Juegos por Usuario
    public void shouldGetplayedGamesByUser() throws Exception {
        LiveData<List<PlayedGame>> aux;
        PlayedGamesListRepository repo = PlayedGamesListRepository.getInstance(PlayedDao);
        PlayedGame played = new PlayedGame(99,12,24, "Titulo2", "Plataforma2","ImagenURL2");
        PlayedDao.insert(played);
        aux = repo.getPlayedGames(12);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"Titulo2");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
        played.setTitle("TituloCambiado");
        PlayedDao.update(played);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"TituloCambiado");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
    }

    @Test
    //Comprobamos si fuciona el LiveData de Juego por Usuario y Juego
    public void shouldGetplayedGamesByUserAndGame() throws Exception {
        LiveData<List<PlayedGame>> aux;
        PlayedGamesListRepository repo = PlayedGamesListRepository.getInstance(PlayedDao);
        PlayedGame played = new PlayedGame(99,12,24, "Titulo2", "Plataforma2","ImagenURL2");
        PlayedDao.insert(played);
        aux = repo.getPlayedGamesByUserAndGame(12,24);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"Titulo2");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
        played.setTitle("TituloCambiado");
        PlayedDao.update(played);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"TituloCambiado");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                         FIN PARTE DE PRUEBAS DEL REPOSITORIO                                 */
    /* ---------------------------------------------------------------------------------------------*/


    /* ---------------------------------------------------------------------------------------------*/
    /*                         INICIO PARTE DE PRUEBAS DEL VIEWMODEL                                */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos la obtencion de elementos desde el ViewModel
    public void shouldGetplayedGamesVM() throws Exception {
        PlayedGamesListRepository repo = PlayedGamesListRepository.getInstance(PlayedDao);
        UserDAO userDAO = db.userDao();
        UserRepository urepo = UserRepository.getInstance(userDAO, SessionStore.getInstance(getTestContext()));
        PlayedGamesListViewModel VM = new PlayedGamesListViewModel(repo, urepo);
        LiveData<List<PlayedGame>> aux;
        PlayedGame played = new PlayedGame(99,12,24, "Titulo2", "Plataforma2","ImagenURL2");
        PlayedDao.insert(played);
        aux = VM.getPlayedGames(99);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"Titulo2");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
        played.setTitle("TituloCambiado");
        PlayedDao.update(played);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"TituloCambiado");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                           FIN PARTE DE PRUEBAS DEL VIEWMODEL                                 */
    /* ---------------------------------------------------------------------------------------------*/

    private Context getTestContext()
    {
        try
        {
            Method getTestContext = searchUnitTest.class.getMethod("getTestContext");
            return (Context) getTestContext.invoke(this);
        }
        catch (final Exception exception)
        {
            exception.printStackTrace();
            return null;
        }
    }

}
