package es.unex.gpsasee;

import android.content.Context;
import android.util.Log;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.dao.FavouriteDAO;
import es.unex.gpsasee.roomdb.entity.Favourite;

import static org.junit.Assert.assertEquals;

public class setJsonTest {

    private FavouriteDAO FavDao;
    private RAWGDatabase db;

    @Before
    public void createDb() {
        //prerequisito: bd favs vacía
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, RAWGDatabase.class).build();
        FavDao = db.favouriteDAO();
        FavDao.deleteAllFavourites();
        db.close();
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                       INICIO PARTE DE PRUEBAS DE LOS MÉTODOS DEL JSON                        */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos la insercion y eliminacion de un juego a favoritos. IMPORTANTE PROBAR ESTO CUANDO LA LISTA DE JUGADOS ESTÉ VACÍA
    public void ProbarAddJson() throws Exception {
        //Inserto favoritos
        JsonMethods jsonMethods = new JsonMethods();
        String entrada="{\"Favourites\": [{\"gameid\":12,\"id\":12,\"image\":\"4\"," +
                "\"platform\":\"PC\",\"title\":\"Grand Theft Auto V\",\"userid\":12}]}";
        if(jsonMethods.importarJson(this.getTestContext(),entrada)){

            //voy a crear la salida que deberia dar si se encuentra en la bd (los parámetros son los mismos que los que se intsertan, cambia el formato)
            String salida="[{\"gameid\":12,\"id\":12,\"image\":\"4\"," +
                    "\"platform\":\"PC\",\"title\":\"Grand Theft Auto V\",\"userid\":12}]";
            //Como el otro método se ha probado que funciona independientemente; se puede usar para ver si este lo hace y de paso no hacer consultas
            //en la bd. Se saca los favoritos; y tiene que estar sólo el que acabamos de insertar. Para esto el requisito es que la bd de favs esté vacía
            assertEquals(jsonMethods.devolverTablaFavs(this.getTestContext()),salida);
        }
        else{
            //fuerzo el error
            assertEquals(2+2,5);
            Log.e("Error SetJson", "Booleano da falso");
        }

    }


    /* ---------------------------------------------------------------------------------------------*/
    /*                        FIN PARTE DE PRUEBAS DE LOS MÉTODOS DEL JSON                          */
    /* ---------------------------------------------------------------------------------------------*/


    private Context getTestContext()
    {
        try
        {
            Method getTestContext = searchUnitTest.class.getMethod("getTestContext");
            return (Context) getTestContext.invoke(this);
        }
        catch (final Exception exception)
        {
            exception.printStackTrace();
            return null;
        }
    }

}
