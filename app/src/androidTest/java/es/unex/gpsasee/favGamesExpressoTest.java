package es.unex.gpsasee;

import android.util.Log;
import android.view.Gravity;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.gpsasee.ui.NavigationDrawerActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static androidx.test.espresso.matcher.ViewMatchers.hasChildCount;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static java.util.regex.Pattern.matches;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class favGamesExpressoTest {
    @Rule
    public ActivityTestRule<NavigationDrawerActivity> mActivityRule =
            new ActivityTestRule<>(NavigationDrawerActivity.class);

    @Before
    public void init(){
        mActivityRule.getActivity().getSupportFragmentManager().beginTransaction();
    }

    /* ---------------------------------------------------------------------------------------------*/
    /* NOTA: LAS PRUEBAS PARA AÑADIR FAVORITOS Y MOSTRAR FAVORITOS SON LAS MISMAS POR RAZONES OBVIAS*/
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Añade a favorito un elemento y despues comprueba que dicho elemento existe en favoritos
    //Como precondicion el juego Portal 2 debe aparecer en 2º lugar y no estar añadido a favoritos previamente
    //Tambien es necesario iniciar las pruebas estando registrado
    public void favourite() throws InterruptedException {
        Thread.sleep(2000);
        onView(withId(R.id.resultlist)).perform(RecyclerViewActions.actionOnItemAtPosition(1, click()));
        Thread.sleep(1000);
        onView(withId(R.id.favButton)).perform(click());
        Thread.sleep(1000);
        Espresso.pressBack();
        Thread.sleep(1000);

        onView(withId(R.id.drawer_layout))
                .check(ViewAssertions.matches(isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open());

        Thread.sleep(1000);
        onView(withId(R.id.favGamesListFragment)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.RecyclerId)).check(ViewAssertions.matches(hasDescendant(withText("Portal 2"))));
        Thread.sleep(1000);
    }
}