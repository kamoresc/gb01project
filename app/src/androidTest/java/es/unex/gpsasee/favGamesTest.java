package es.unex.gpsasee;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.unex.gpsasee.TestUtils.LiveDataTestUtils;
import es.unex.gpsasee.repositories.FavGamesListRepository;
import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.dao.FavouriteDAO;
import es.unex.gpsasee.roomdb.dao.UserDAO;
import es.unex.gpsasee.roomdb.entity.Favourite;
import es.unex.gpsasee.roomdb.entity.GameParams;
import es.unex.gpsasee.ui.fav_games.FavGamesListViewModel;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class favGamesTest {

    private FavouriteDAO FavDao;
    private RAWGDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, RAWGDatabase.class).build();
        FavDao = db.favouriteDAO();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    /* ---------------------------------------------------------------------------------------------*/
    /* NOTA: LAS PRUEBAS PARA AÑADIR FAVORITOS Y MOSTRAR FAVORITOS SON LAS MISMAS POR RAZONES OBVIAS*/
    /* ---------------------------------------------------------------------------------------------*/

    /* ---------------------------------------------------------------------------------------------*/
    /*                           INICIO PARTE DE PRUEBAS DEL DAO                                    */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos la insercion y eliminacion de un juego a favoritos
    public void shouldAddDeleteFavoriteToDB() throws Exception {
        Favourite fav = new Favourite(1,1,100, "Titulo1", "Plataforma1","ImagenURL1");
        FavDao.insert(fav);
        List<Favourite> listaFavs = FavDao.getAllFavourites();
        assertEquals(listaFavs.size(),1);
        assertEquals(listaFavs.get(0).getId(), 1);
        assertEquals(listaFavs.get(0).getGameid(), 100);
        assertEquals(listaFavs.get(0).getTitle(), "Titulo1");
        assertEquals(listaFavs.get(0).getPlatform(), "Plataforma1");
        assertEquals(listaFavs.get(0).getImage(), "ImagenURL1");
        FavDao.delete(fav);
        listaFavs = FavDao.getAllFavourites();
        assertEquals(listaFavs.size(),0);
    }

    @Test
    //Comprobamos la actualizacion de un objeto
    public void shouldUpdateFavoriteToDB() throws Exception {
        Favourite fav = new Favourite(1,1,100, "Titulo2", "Plataforma2","ImagenURL2");
        FavDao.insert(fav);
        List<Favourite> listaFavs = FavDao.getAllFavourites();
        assertEquals(listaFavs.get(0).getTitle(), "Titulo2");
        fav.setTitle("NuevoTitulo");
        FavDao.update(fav);
        listaFavs = FavDao.getAllFavourites();
        assertEquals(listaFavs.get(0).getTitle(), "NuevoTitulo");
        FavDao.delete(fav);
    }

    @Test
    //Comprobamos eliminacion por ID y GAMEID
    public void shouldDeleteIdGameIdFavoriteToDB() throws Exception {
        Favourite fav = new Favourite(1,12,24, "Titulo2", "Plataforma2","ImagenURL2");
        Favourite fav2 = new Favourite(2,13,26, "Titulo3", "Plataforma3","ImagenURL3");
        FavDao.insert(fav);
        FavDao.insert(fav2);
        //inicialmente solo hay 2
        List<Favourite> listaFavs = FavDao.getAllFavourites();
        assertEquals(listaFavs.size(),2);
        //Borramos uno de los elementos
        FavDao.deleteFavouriteByUserAndGame(12,24);
        listaFavs = FavDao.getAllFavourites();
        assertEquals(listaFavs.size(),1);
        //Borramos un elemento que no existe por lo que no se elimina ningun elemento
        FavDao.deleteFavouriteByUserAndGame(1,1);
        listaFavs = FavDao.getAllFavourites();
        assertEquals(listaFavs.size(),1);
        //Borramos el ultimo elemento, dejando la BD sin elementos
        FavDao.deleteFavouriteByUserAndGame(13,26);
        listaFavs = FavDao.getAllFavourites();
        assertEquals(listaFavs.size(),0);
    }

    @Test
    //Comprobamos si fuciona el eliminar por usuario
    public void shouldDeleteByUserFavoriteToDB() throws Exception {
        Favourite fav = new Favourite(1,200,24, "Titulo2", "Plataforma2","ImagenURL2");
        Favourite fav2 = new Favourite(2,200,26, "Titulo3", "Plataforma3","ImagenURL3");
        Favourite fav3 = new Favourite(3,199,28, "Titulo4", "Plataforma4","ImagenURL4");
        FavDao.insert(fav);
        FavDao.insert(fav2);
        FavDao.insert(fav3);
        //Tenemos 3 elementos en la BD
        List<Favourite> listaFavs = FavDao.getAllFavourites();
        assertEquals(listaFavs.size(),3);
        //Borramos los 2 elementos vinculados al usuario 200 (dejando 1 en la BD)
        FavDao.deleteAllFavouritesByUser(200);
        listaFavs = FavDao.getAllFavourites();
        assertEquals(listaFavs.size(),1);
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                           FIN PARTE DE PRUEBAS DEL DAO                                       */
    /* ---------------------------------------------------------------------------------------------*/

    /* ---------------------------------------------------------------------------------------------*/
    /*                      INICIO PARTE DE PRUEBAS DEL REPOSITORIO                                 */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos LiveData de Juegos por Usuario
    public void shouldGetFavGamesByUser() throws Exception {
        LiveData<List<Favourite>> aux;
        FavGamesListRepository repo = FavGamesListRepository.getInstance(FavDao);
        Favourite fav = new Favourite(99,12,24, "Titulo2", "Plataforma2","ImagenURL2");
        FavDao.insert(fav);
        aux = repo.getFavGames(12);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"Titulo2");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
        fav.setTitle("TituloCambiado");
        FavDao.update(fav);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"TituloCambiado");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
    }

    @Test
    //Comprobamos si fuciona el LiveData de Juego por Usuario y Juego
    public void shouldGetFavGamesByUserAndGame() throws Exception {
        LiveData<List<Favourite>> aux;
        FavGamesListRepository repo = FavGamesListRepository.getInstance(FavDao);
        Favourite fav = new Favourite(99,12,24, "Titulo2", "Plataforma2","ImagenURL2");
        FavDao.insert(fav);
        aux = repo.getFavGamesByUserAndGame(12,24);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"Titulo2");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
        fav.setTitle("TituloCambiado");
        FavDao.update(fav);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"TituloCambiado");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                         FIN PARTE DE PRUEBAS DEL REPOSITORIO                                 */
    /* ---------------------------------------------------------------------------------------------*/


    /* ---------------------------------------------------------------------------------------------*/
    /*                         INICIO PARTE DE PRUEBAS DEL VIEWMODEL                                */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos la obtencion de elementos desde el ViewModel
    public void shouldGetFavGamesVM() throws Exception {
        FavGamesListRepository repo = FavGamesListRepository.getInstance(FavDao);
        UserDAO userDAO = db.userDao();
        UserRepository urepo = UserRepository.getInstance(userDAO, SessionStore.getInstance(getTestContext()));
        FavGamesListViewModel VM = new FavGamesListViewModel(repo, urepo);
        LiveData<List<Favourite>> aux;
        Favourite fav = new Favourite(99,12,24, "Titulo2", "Plataforma2","ImagenURL2");
        FavDao.insert(fav);
        aux = VM.getFavGames(99);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"Titulo2");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
        fav.setTitle("TituloCambiado");
        FavDao.update(fav);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"TituloCambiado");
                }
                catch (Exception e){
                    Log.e("Error LiveData", "Salta una excepcion");
                }
            }
        });
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                           FIN PARTE DE PRUEBAS DEL VIEWMODEL                                 */
    /* ---------------------------------------------------------------------------------------------*/

    private Context getTestContext()
    {
        try
        {
            Method getTestContext = searchUnitTest.class.getMethod("getTestContext");
            return (Context) getTestContext.invoke(this);
        }
        catch (final Exception exception)
        {
            exception.printStackTrace();
            return null;
        }
    }

}
