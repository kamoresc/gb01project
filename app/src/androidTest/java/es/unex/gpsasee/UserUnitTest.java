package es.unex.gpsasee;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Method;

import es.unex.gpsasee.TestUtils.LiveDataTestUtils;
import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.dao.UserDAO;
import es.unex.gpsasee.roomdb.entity.User;
import es.unex.gpsasee.ui.NavigationDrawerViewModel;
import es.unex.gpsasee.ui.login.LoginViewModel;
import es.unex.gpsasee.ui.logout.LogoutViewModel;
import es.unex.gpsasee.ui.register.RegisterViewModel;
import es.unex.gpsasee.ui.user.UserViewModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class UserUnitTest {

    // necessary to test the LiveData
    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setup () {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testUserDao() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        UserDAO userDAO = database.userDao();

        User user = getTestUser();

        user.setId(userDAO.insert(user));

        assertEquals(user, LiveDataTestUtils.getValue(userDAO.getUser(user.getId())));
        assertEquals(user, LiveDataTestUtils.getValue(userDAO.getUserByName(user.getUsername())));
        assertEquals(user, LiveDataTestUtils.getValue(userDAO.login(user.getUsername(), user.getPassword())));
        assertEquals(user.getDescription(), LiveDataTestUtils.getValue(userDAO.getUser(user.getId())).getDescription());

        user.setDescription(getTestComment());
        userDAO.update(user);

        assertEquals(user.getDescription(), LiveDataTestUtils.getValue(userDAO.getUser(user.getId())).getDescription());

        userDAO.delete(user);

        assertNull(LiveDataTestUtils.getValue(userDAO.getUser(user.getId())));
        assertNull(LiveDataTestUtils.getValue(userDAO.getUserByName(user.getUsername())));
        assertNull(LiveDataTestUtils.getValue(userDAO.login(user.getUsername(), user.getPassword())));
    }

    @Test
    public void testUserSessionStore() throws InterruptedException {
        SessionStore store = SessionStore.getInstance(getTestContext());

        User user = getTestUser();

        store.setUser(user);

        assertEquals(user, LiveDataTestUtils.getValue(store.currentUser));
        assertEquals(user.getDescription(), LiveDataTestUtils.getValue(store.currentUser).getDescription());

        store.setUser(null);

        assertNull(LiveDataTestUtils.getValue(store.currentUser));
    }

    @Test
    public void testUserRepository() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        UserDAO userDAO = database.userDao();
        UserRepository userRepository = UserRepository.getInstance(userDAO, SessionStore.getInstance(getTestContext()));

        User user = getTestUser();

        user.setId(userRepository.insert(user));

        assertEquals(user, LiveDataTestUtils.getValue(userRepository.getUserById(user.getId())));
        assertEquals(user, LiveDataTestUtils.getValue(userRepository.getUserByName(user.getUsername())));
        assertEquals(user, LiveDataTestUtils.getValue(userRepository.checkLogin(user.getUsername(), user.getPassword())));
        assertEquals(user.getDescription(), LiveDataTestUtils.getValue(userRepository.getUserById(user.getId())).getDescription());

        userRepository.delete(user);

        assertNull(LiveDataTestUtils.getValue(userRepository.getUserById(user.getId())));
        assertNull(LiveDataTestUtils.getValue(userRepository.getUserByName(user.getUsername())));
        assertNull(LiveDataTestUtils.getValue(userRepository.checkLogin(user.getUsername(), user.getPassword())));
    }

    @Test
    public void testRegisterViewModel() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        UserDAO userDAO = database.userDao();
        SessionStore store = SessionStore.getInstance(getTestContext());
        UserRepository userRepository = UserRepository.getInstance(userDAO, store);
        RegisterViewModel viewModel = new RegisterViewModel(userRepository);

        User user = getTestUser();

        viewModel.registerUser(user);

        assertEquals(user, LiveDataTestUtils.getValue(viewModel.currentUser()));
        assertEquals(user, LiveDataTestUtils.getValue(viewModel.getUserByName(user.getUsername())));
        assertEquals(user.getDescription(), LiveDataTestUtils.getValue(viewModel.currentUser()).getDescription());

        userRepository.delete(user);
        store.setUser(null);
    }

    @Test
    public void testLoginViewModel() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        UserDAO userDAO = database.userDao();
        SessionStore store = SessionStore.getInstance(getTestContext());
        UserRepository userRepository = UserRepository.getInstance(userDAO, store);
        LoginViewModel viewModel = new LoginViewModel(userRepository);

        User user = getTestUser();
        user.setId(userRepository.insert(user));

        assertEquals(user, LiveDataTestUtils.getValue(viewModel.checkLogin(user.getUsername(), user.getPassword())));
        assertNull(LiveDataTestUtils.getValue(viewModel.currentUser()));

        viewModel.setCurrentUser(user);

        assertEquals(user, LiveDataTestUtils.getValue(viewModel.currentUser()));

        userRepository.delete(user);
        store.setUser(null);
    }

    @Test
    public void testUserViewModel() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        UserDAO userDAO = database.userDao();
        SessionStore store = SessionStore.getInstance(getTestContext());
        UserRepository userRepository = UserRepository.getInstance(userDAO, store);
        UserViewModel viewModel = new UserViewModel(userRepository);

        User user = getTestUser();
        user.setId(userRepository.insert(user));
        store.setUser(user);

        assertEquals(user, LiveDataTestUtils.getValue(viewModel.currentUser()));
        assertEquals(user.getDescription(), LiveDataTestUtils.getValue(viewModel.currentUser()).getDescription());

        user.setDescription(getTestComment());
        viewModel.editUser(user);

        assertEquals(user.getDescription(), LiveDataTestUtils.getValue(viewModel.currentUser()).getDescription());
        assertEquals(user.getDescription(), LiveDataTestUtils.getValue(userDAO.getUser(user.getId())).getDescription());

        viewModel.deleteUser(user);

        assertNull(LiveDataTestUtils.getValue(viewModel.currentUser()));
        assertNull(LiveDataTestUtils.getValue(userRepository.getUserById(user.getId())));
        assertNull(LiveDataTestUtils.getValue(userRepository.getUserByName(user.getUsername())));
        assertNull(LiveDataTestUtils.getValue(userRepository.checkLogin(user.getUsername(), user.getPassword())));
    }

    @Test
    public void testLogoutViewModel() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        UserDAO userDAO = database.userDao();
        SessionStore store = SessionStore.getInstance(getTestContext());
        UserRepository userRepository = UserRepository.getInstance(userDAO, store);
        LogoutViewModel viewModel = new LogoutViewModel(userRepository);

        User user = getTestUser();
        //user.setId(userRepository.insert(user));
        store.setUser(user);

        assertEquals(user, LiveDataTestUtils.getValue(viewModel.currentUser()));

        viewModel.logout();

        assertNull(LiveDataTestUtils.getValue(viewModel.currentUser()));
    }

    @Test
    public void testNavigationDrawerViewModel() throws InterruptedException {
        RAWGDatabase database = RAWGDatabase.getDatabase(getTestContext());
        UserDAO userDAO = database.userDao();
        SessionStore store = SessionStore.getInstance(getTestContext());
        UserRepository userRepository = UserRepository.getInstance(userDAO, store);
        NavigationDrawerViewModel viewModel = new NavigationDrawerViewModel(userRepository);

        assertNull(LiveDataTestUtils.getValue(viewModel.currentUser()));

        User user = getTestUser();
        //user.setId(userRepository.insert(user));
        store.setUser(user);

        assertEquals(user, LiveDataTestUtils.getValue(viewModel.currentUser()));
    }

    private User getTestUser(){
        return new User(0L, "agua@hagrid.com", "Hagrid33", "chorrodebits", "");
    }

    private String getTestComment(){
        return "Chorro de bits";
    }

    private Context getTestContext()
    {
        try
        {
            Method getTestContext = UserUnitTest.class.getMethod("getTestContext");
            return (Context) getTestContext.invoke(this);
        }
        catch (final Exception exception)
        {
            exception.printStackTrace();
            return null;
        }
    }
}