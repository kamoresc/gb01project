package es.unex.gpsasee;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import es.unex.gpsasee.TestUtils.LiveDataTestUtils;
import es.unex.gpsasee.repositories.CommentsListRepository;
import es.unex.gpsasee.repositories.FavGamesListRepository;
import es.unex.gpsasee.repositories.UserRepository;
import es.unex.gpsasee.roomdb.RAWGDatabase;
import es.unex.gpsasee.roomdb.dao.CommentDAO;
import es.unex.gpsasee.roomdb.dao.FavouriteDAO;
import es.unex.gpsasee.roomdb.dao.UserDAO;
import es.unex.gpsasee.roomdb.entity.Comment;
import es.unex.gpsasee.roomdb.entity.Favourite;
import es.unex.gpsasee.ui.CommentsListViewModel;
import es.unex.gpsasee.ui.fav_games.FavGamesListViewModel;

import static org.junit.Assert.assertEquals;

public class CommentsTest {

    private CommentDAO ComDao;
    private RAWGDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, RAWGDatabase.class).build();
        ComDao = db.commentDAO();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                           INICIO PARTE DE PRUEBAS DEL DAO                                    */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos la insercion de un comentario
    public void shouldAddDeleteCommentsToDB() throws Exception {
        Comment com = new Comment(1,1,100, "Increible", "Está bastante chulo, la verdad","sergio");
        ComDao.insert(com);
        List<Comment> listaComs = ComDao.getCommentsByUser(1);
        assertEquals(listaComs.size(),(int) 1);
        assertEquals(listaComs.get(0).getId(), 1);
        assertEquals(listaComs.get(0).getUserid(), 1);
        assertEquals(listaComs.get(0).getGameid(), 100);
        assertEquals(listaComs.get(0).getTitle(), "Increible");
        assertEquals(listaComs.get(0).getTexto(), "Está bastante chulo, la verdad");
        assertEquals(listaComs.get(0).getUsername(), "sergio");
        ComDao.delete(com);
        listaComs = ComDao.getCommentsByUser(1);
        assertEquals(listaComs.size(),0);
    }

    @Test
    //Comprobamos las otras funciones que no se usan pero están disponibles para un futuro
    public void shouldUpdateCommentsToDB() throws Exception {
        Comment com = new Comment(3,1,100, "Increible", "Está bastante chulo, la verdad","sergio");
        ComDao.insert(com);
        //Obtención de un comentario por ID
        Comment c=ComDao.getComment((long)3);
        //Obtención de los datos asociados al comentario obtenido
        assertEquals(c.getId(), 3);
        assertEquals(c.getUserid(), 1);
        assertEquals(c.getGameid(), 100);
        assertEquals(c.getTitle(), "Increible");
        assertEquals(c.getTexto(), "Está bastante chulo, la verdad");
        assertEquals(c.getUsername(), "sergio");
        ComDao.delete(com);
        c = ComDao.getComment((long)3);
        assertEquals(c, null);
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                           FIN PARTE DE PRUEBAS DEL DAO                                       */
    /* ---------------------------------------------------------------------------------------------*/

    /* ---------------------------------------------------------------------------------------------*/
    /*                      INICIO PARTE DE PRUEBAS DEL REPOSITORIO                                 */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos LiveData de Comentarios
    public void shouldGetCommentsRepLD() throws Exception {
        LiveData<List<Comment>> aux;
        CommentsListRepository repo = CommentsListRepository.getInstance(ComDao);
        Comment com = new Comment(1,1,100, "Bien", "Decente","sergio");
        ComDao.insert(com);
        aux = repo.getCurrentComments();
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    //assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"Bien");
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getId(),1);
                }
                catch (Exception e){
                    Log.e("Error LiveData comentarios", "Salta una excepcion");
                }
            }
        });
        com.setTexto("Ha mejorado");
        ComDao.update(com);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    //assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"Ha mejorado");
                }
                catch (Exception e){
                    Log.e("Error LiveData comentarios", "Salta una excepcion");
                }
            }
        });
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                         FIN PARTE DE PRUEBAS DEL REPOSITORIO                                 */
    /* ---------------------------------------------------------------------------------------------*/


    /* ---------------------------------------------------------------------------------------------*/
    /*                         INICIO PARTE DE PRUEBAS DEL VIEWMODEL                                */
    /* ---------------------------------------------------------------------------------------------*/

    @Test
    //Comprobamos la obtencion de elementos desde el ViewModel
    public void shouldGetCommentsVM() throws Exception {
        CommentsListRepository repo = CommentsListRepository.getInstance(ComDao);
        CommentsListViewModel VM = new CommentsListViewModel(repo);
        LiveData<List<Comment>> aux;
        Comment com = new Comment(1,1,100, "Guay", "Chachi piruli","sergio");
        ComDao.insert(com);
        aux = VM.getComments();
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"Guay");
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTexto(),"Chachi piruli");
                }
                catch (Exception e){
                    Log.e("Error LiveData Comments", "Salta una excepcion");
                }
            }
        });
        com.setTitle("Malo");
        ComDao.update(com);
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                try{
                    assertEquals(LiveDataTestUtils.getValue(aux).get(0).getTitle(),"Malo");
                }
                catch (Exception e){
                    Log.e("Error LiveData Comments", "Salta una excepcion");
                }
            }
        });
    }

    /* ---------------------------------------------------------------------------------------------*/
    /*                           FIN PARTE DE PRUEBAS DEL VIEWMODEL                                 */
    /* ---------------------------------------------------------------------------------------------*/

    private Context getTestContext()
    {
        try
        {
            Method getTestContext = searchUnitTest.class.getMethod("getTestContext");
            return (Context) getTestContext.invoke(this);
        }
        catch (final Exception exception)
        {
            exception.printStackTrace();
            return null;
        }
    }

}
